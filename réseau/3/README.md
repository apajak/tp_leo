# TP3 : On va router des trucs

Au menu de ce TP, on va revoir un peu ARP et IP histoire de **se mettre en jambes dans un environnement avec des VMs**.

Puis on mettra en place **un routage simple, pour permettre à deux LANs de communiquer**.

![Reboot the router](./pics/reboot.jpeg)

## Sommaire

- [TP3 : On va router des trucs](#tp3--on-va-router-des-trucs)
  - [Sommaire](#sommaire)
  - [0. Prérequis](#0-prérequis)
  - [I. ARP](#i-arp)
    - [1. Echange ARP](#1-echange-arp)
    - [2. Analyse de trames](#2-analyse-de-trames)
  - [II. Routage](#ii-routage)
    - [1. Mise en place du routage](#1-mise-en-place-du-routage)
    - [2. Analyse de trames](#2-analyse-de-trames-1)
    - [3. Accès internet](#3-accès-internet)
  - [III. DHCP](#iii-dhcp)
    - [1. Mise en place du serveur DHCP](#1-mise-en-place-du-serveur-dhcp)
    - [2. Analyse de trames](#2-analyse-de-trames-2)

## 0. Prérequis

➜ Pour ce TP, on va se servir de VMs Rocky Linux. 1Go RAM c'est large large. Vous pouvez redescendre la mémoire vidéo aussi.  

➜ Vous aurez besoin de deux réseaux host-only dans VirtualBox :

- un premier réseau `10.3.1.0/24`
- le second `10.3.2.0/24`
- **vous devrez désactiver le DHCP de votre hyperviseur (VirtualBox) et définir les IPs de vos VMs de façon statique**

➜ Quelques paquets seront souvent nécessaires dans les TPs, il peut être bon de les installer dans la VM que vous clonez :

- de quoi avoir les commandes :
  - `dig`
  - `tcpdump`
  - `nmap`
  - `nc`
  - `python3`
  - `vim` peut être une bonne idée

➜ Les firewalls de vos VMs doivent **toujours** être actifs (et donc correctement configurés).

➜ **Si vous voyez le p'tit pote 🦈 c'est qu'il y a un PCAP à produire et à mettre dans votre dépôt git de rendu.**

## I. ARP

Première partie simple, on va avoir besoin de 2 VMs.

| Machine  | `10.3.1.0/24` |
|----------|---------------|
| `john`   | `10.3.1.11`   |
| `marcel` | `10.3.1.12`   |

```schema
   john               marcel
  ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     │
  └─────┘    └───┘    └─────┘
```

> Référez-vous au [mémo Réseau Rocky](../../cours/memo/rocky_network.md) pour connaître les commandes nécessaire à la réalisation de cette partie.

### 1. Echange ARP

🌞**Générer des requêtes ARP**

- effectuer un `ping` d'une machine à l'autre
   ```bash
   [paja444@john ~]$ ip a | grep enp0s8
   2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
      inet 10.3.1.11/24 brd 10.3.1.255 scope global noprefixroute enp0s8
   [paja444@john ~]$ ping 10.3.1.12
   PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
   64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=1.20 ms
   64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=0.823 ms
   64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=1.12 ms
   ^C
   --- 10.3.1.12 ping statistics ---
   3 packets transmitted, 3 received, 0% packet loss, time 2003ms
   rtt min/avg/max/mdev = 0.823/1.047/1.199/0.161 ms
   ```

- observer les tables ARP des deux machines
   ```bash
   [paja444@john ~]$ ip n s
   10.3.1.12 dev enp0s8 lladdr 08:00:27:bd:df:93 STALE
   10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:00 DELAY
   ```

   ```bash
   [paja444@marcel ~]$ ip n s
   10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:00 DELAY
   10.3.1.11 dev enp0s8 lladdr 08:00:27:fa:9f:42 STALE
   ```

- repérer l'adresse MAC de `john` dans la table ARP de `marcel` et vice-versa
        john-MAC: 08:00:27:fa:9f:42
        marcel-MAC: 08:00:27:bd:df:93

- prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)
  - une commande pour voir la MAC de `marcel` dans la table ARP de `john`
   ```bash
   [paja444@john ~]$ ip n s 10.3.1.12
   10.3.1.12 dev enp0s8 lladdr 08:00:27:bd:df:93 STALE
   ```
  - et une commande pour afficher la MAC de `marcel`, depuis `marcel`
   ```bash
   paja444@marcel ~]$ ip n s 10.3.1.11
   10.3.1.11 dev enp0s8 lladdr 08:00:27:fa:9f:42 STALE
   ```

### 2. Analyse de trames

🌞**Analyse de trames**

- utilisez la commande `tcpdump` pour réaliser une capture de trame
- videz vos tables ARP, sur les deux machines, puis effectuez un `ping`
   ```bash
   [paja444@john ~]$ sudo ip n f all
   ```
   ```bash
   [paja444@marcel ~]$ ping 10.3.1.11
   PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
   64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=2.30 ms
   64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.765 ms
   64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=0.672 ms
   64 bytes from 10.3.1.11: icmp_seq=4 ttl=64 time=0.835 ms
   ^C
   --- 10.3.1.11 ping statistics ---
   4 packets transmitted, 4 received, 0% packet loss, time 3038ms
   rtt min/avg/max/mdev = 0.672/1.141/2.295/0.668 ms
   ```
   ```bash
   [paja444@john ~]$ sudo tcpdump -i enp0s8 -n arp
   dropped privs to tcpdump
   tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
   listening on enp0s8, link-type EN10MB (Ethernet), snapshot length 262144 bytes
   17:44:31.644840 ARP, Request who-has 10.3.1.11 tell 10.3.1.12, length 46
   17:44:31.644951 ARP, Reply 10.3.1.11 is-at 08:00:27:fa:9f:42, length 28
   17:44:31.644974 ARP, Request who-has 10.3.1.11 tell 10.3.1.12, length 46
   17:44:31.644977 ARP, Reply 10.3.1.11 is-at 08:00:27:fa:9f:42, length 28
   17:44:36.993283 ARP, Request who-has 10.3.1.1 tell 10.3.1.11, length 28
   17:44:36.993321 ARP, Request who-has 10.3.1.12 tell 10.3.1.11, length 28
   17:44:36.993670 ARP, Reply 10.3.1.1 is-at 0a:00:27:00:00:00, length 46
   17:44:36.994010 ARP, Reply 10.3.1.12 is-at 08:00:27:bd:df:93, length 46
   ^C
   8 packets captured
   12 packets received by filter
   0 packets dropped by kernel
   ```

🦈 **Capture réseau `tp2_arp.pcapng`** qui contient un ARP request et un ARP reply

   ```bash
   [paja444@john ~]$ sudo tcpdump -n arp -w ping.pcap -i enp0s8
   dropped privs to tcpdump
   tcpdump: listening on enp0s8, link-type EN10MB (Ethernet), snapshot length 262144 bytes
   ^C0 packets captured
   0 packets received by filter
   0 packets dropped by kernel
   [paja444@john ~]$ ls
   ping.pcap
   ```

   ```bash
   (base) paja@paja-mac ~ % scp paja444@10.3.1.11:/home/paja444/ping.pcap ./Downloads/
   paja444@10.3.1.11's password: 
   ping.pcap                                  100%   24    15.4KB/s   00:00  
   ```
[ping.pcap](./PCAP/ping.pcap)

> **Si vous ne savez pas comment récupérer votre fichier `.pcapng`** sur votre hôte afin de l'ouvrir dans Wireshark, et me le livrer en rendu, demandez-moi.

## II. Routage

Vous aurez besoin de 3 VMs pour cette partie. **Réutilisez les deux VMs précédentes.**

| Machine  | `10.3.1.0/24` | `10.3.2.0/24` |
|----------|---------------|---------------|
| `router` | `10.3.1.254`  | `10.3.2.254`  |
| `john`   | `10.3.1.11`   | no            |
| `marcel` | no            | `10.3.2.12`   |

> Je les appelés `marcel` et `john` PASKON EN A MAR des noms nuls en réseau 🌻

```schema
   john                router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └───┘    └─────┘    └───┘    └─────┘
```
```bash
[paja444@john ~]$ ip a | grep enp0s3
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.1.11/24 brd 10.3.1.255 scope global noprefixroute enp0s3
```
```bash
[paja444@marcel ~]$ ip a | grep enp0s3
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.2.12/24 brd 10.3.2.255 scope global noprefixroute enp0s3
```
```bash
[paja444@router ~]$ ip a | grep enp0s3
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.1.254/24 brd 10.3.1.255 scope global noprefixroute enp0s3
[paja444@router ~]$ ip a | grep enp0s8
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.2.254/24 brd 10.3.2.255 scope global noprefixroute enp0s8
```

### 1. Mise en place du routage

🌞**Activer le routage sur le noeud `router`**

```bash
[paja444@router ~]$ sudo firewall-cmd --list-all
[sudo] password for paja444: 
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
[paja444@router ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s3 enp0s8
[paja444@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[paja444@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

> Cette étape est nécessaire car Rocky Linux c'est pas un OS dédié au routage par défaut. Ce n'est bien évidemment une opération qui n'est pas nécessaire sur un équipement routeur dédié comme du matériel Cisco.

🌞**Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`**

*** John: ***
```bash
[paja444@john ~]$ sudo nano /etc/sysconfig/network-scripts/route-enp0s3
```
```bash
10.3.2.12/24 via 10.3.1.254 dev enp0s3
```
```bash
[paja444@john ~]$ sudo systemctl restart NetworkManager
[paja444@john ~]$ ip r s
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 100 
10.3.2.0/24 via 10.3.1.254 dev enp0s3 proto static metric 100 
```
```bash
[paja444@john ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=2.06 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.16 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=1.40 ms
```

*** Marcel ***
```bash
[paja444@marcel ~]$ sudo nano /etc/sysconfig/network-scripts/route-enp0s3
```
```bash
10.3.1.11/24 via 10.3.2.254 dev enp0s3
```
```bash
[paja444@marcel ~]$ sudo systemctl restart NetworkManager
[paja444@marcel ~]$ ip r s
10.3.1.0/24 via 10.3.2.254 dev enp0s3 proto static metric 100 
10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.12 metric 100 
```
```bash
[paja444@marcel ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=63 time=2.54 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=63 time=0.835 ms
^C
--- 10.3.1.11 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1003ms
rtt min/avg/max/mdev = 0.835/1.688/2.542/0.853 ms
```

![THE SIZE](./pics/thesize.png)

### 2. Analyse de trames

🌞**Analyse des échanges ARP**

- videz les tables ARP des trois noeuds
   ```bash
   [paja444@john ~]$ sudo ip neigh flush all
   [sudo] password for paja444: 
   ```
   ```bash
   [paja444@marcel network-scripts]$ sudo ip neigh flush all
   [sudo] password for paja444: 
   ```
   ```bash
   [paja444@router ~]$ sudo ip neigh flush all
   [sudo] password for paja444: 
   ```

- effectuez un `ping` de `john` vers `marcel`
   ```bash
[paja444@john ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.91 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=2.73 ms
^C
--- 10.3.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1001ms
rtt min/avg/max/mdev = 1.910/2.321/2.733/0.411 ms
   ```
- regardez les tables ARP des trois noeuds
   ```bash
   [paja444@john ~]$ ip n s dev enp0s3
   10.3.1.254 lladdr 08:00:27:a6:bd:69 STALE
   10.3.1.1 lladdr 0a:00:27:00:00:00 DELAY
   ```
   ```bash
   [paja444@marcel ~]$ ip n s dev enp0s3
   10.3.2.254 lladdr 08:00:27:0c:a3:b6 STALE
   10.3.2.1 lladdr 0a:00:27:00:00:01 REACHABLE
   ```
   ```bash
   [paja444@router ~]$ ip n s 
   10.3.2.12 dev enp0s8 lladdr 08:00:27:b3:da:bd STALE
   10.3.1.11 dev enp0s3 lladdr 08:00:27:12:7b:b7 STALE
   10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:00 REACHABLE
   ```
- essayez de déduire un peu les échanges ARP qui ont eu lieu
- répétez l'opération précédente (vider les tables, puis `ping`), en lançant `tcpdump` sur `marcel`
- **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames** utiles pour l'échange

Par exemple (copiez-collez ce tableau ce sera le plus simple) :

```bash
[paja444@marcel ~]$ sudo tcpdump -w ping_johnToMarcel_marcel.pcap -i enp0s3
```
```bash
[paja444@john ~]$ sudo tcpdump -w ping_johnToMarcel_john.pcap -i enp0s3
```
```bash
(base) paja@paja-mac ~ % scp paja444@10.3.1.11:/home/paja444/ping_johnToMarcel_john.pcap ./Downloads/
paja444@10.3.1.11's password: 
ping_johnToMarcel_john.pcap                                       100% 6672     3.6MB/s   00:00    
(base) paja@paja-mac ~ % scp paja444@10.3.2.12:/home/paja444/ping_johnToMarcel_marcel.pcap ./Downloads/
paja444@10.3.2.12's password: 
ping_johnToMarcel_marcel.pcap                                     100% 2228     2.3MB/s   00:00  
```

router_MAC(enp0s8): `08:00:27:0c:a3:b`
router_IP(enp0s8): `10.3.2.254`

router_MAC(enp0s3): `08:00:27:a6:bd:69`
router_IP(enp0s3): `10.3.1.254`

john_MAC: `08:00:27:12:7b:b7`
john_IP: `10.3.1.11`

marcel_MAC: `08:00:27:b3:da:bd`
marcel_IP: `10.3.2.12`

*** marcel: ***

| ordre | type trame  | IP source  | MAC source                   | IP destination | MAC destination              |
|-------|-------------|------------|------------------------------|----------------|------------------------------|
| 1     | Requête ARP | x          | `router` `08:00:27:0c:a3:b6` | x              | Broadcast `FF:FF:FF:FF:FF`   |
| 2     | Réponse ARP | x          | `marcel` `08:00:27:b3:da:bd` | x              | `router` `08:00:27:0c:a3:b6` |
| ...   | ...         | ...        | ...                          |                |                              |
| 3     | Ping        |`10.3.2.254`| `router` `08:00:27:0c:a3:b6` | `10.3.2.12`    | `marcel` `08:00:27:b3:da:bd` |
| 4     | Pong        |`10.3.2.12` | `marcel` `08:00:27:b3:da:bd`  | `10.3.2.254`  | `router` `08:00:27:0c:a3:b6` |


*** john: ***
| ordre | type trame  | IP source  | MAC source                   | IP destination | MAC destination              |
|-------|-------------|------------|------------------------------|----------------|------------------------------|
| 1     | Requête ARP | x          | `john` `08:00:27:12:7b:b7`.  | x              | Broadcast `FF:FF:FF:FF:FF`   |
| 2     | Réponse ARP | x          | `router` `08:00:27:a6:bd:69` | x              | `john` `08:00:27:12:7b:b7`   |
| ...   | ...         | ...        | ...                          |                |                              |
| 3     | Ping        |`10.3.1.11` | `john` `08:00:27:12:7b:b7`   | `10.3.2.12`    | `marcel` `08:00:27:b3:da:bd` |
| 4     | Pong        |`10.3.2.12` | `marcel` `08:00:27:b3:da:bd` | `10.3.1.11`    | `john` `08:00:27:12:7b:b7`   |

> Vous pourriez, par curiosité, lancer la capture sur `john` aussi, pour voir l'échange qu'il a effectué de son côté.

🦈 **Capture réseau `tp2_routage_marcel.pcapng`**
[ping_johnToMarcel_marcel.pcap](./PCAP/ping_johnToMarcel_marcel.pcap )
[ping_johnToMarcel_john.pcap](./PCAP/ping_johnToMarcel_john.pcap )

### 3. Accès internet

🌞**Donnez un accès internet à vos machines**

- ajoutez une carte NAT en 3ème inteface sur le `router` pour qu'il ait un accès internet
   ```bash
   [paja444@router ~]$ ip a | grep enp0s9
   4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
      inet 10.0.4.15/24 brd 10.0.4.255 scope global dynamic noprefixroute enp0s9
   ```
- ajoutez une route par défaut à `john` et `marcel`

   *** john: ***
   ```bash
   [paja444@john ~]$ sudo nano /etc/sysconfig/network
   ```
   ```bash
   # Created by anaconda
   GATEWAY=10.3.1.254
   ```
   ```bash
   [paja444@john ~]$ sudo systemctl restart NetworkManager
   [paja444@john ~]$ sudo ip r s
   default via 10.3.1.254 dev enp0s3 proto static metric 100 
   10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 100 
   10.3.2.0/24 via 10.3.1.254 dev enp0s3 proto static metric 100 
   [paja444@john ~]$ ping 54.37.153.179
   PING 54.37.153.179 (54.37.153.179) 56(84) bytes of data.
   64 bytes from 54.37.153.179: icmp_seq=1 ttl=61 time=24.8 ms
   64 bytes from 54.37.153.179: icmp_seq=2 ttl=61 time=55.5 ms
   ^C
   --- 54.37.153.179 ping statistics ---
   2 packets transmitted, 2 received, 0% packet loss, time 1001ms
   rtt min/avg/max/mdev = 24.824/40.159/55.495/15.335 ms
   ```

   *** marcel: ***
   ```bash
   [paja444@marcel ~]$ sudo nano /etc/sysconfig/network
   ```
   ```bash
   # Created by anaconda
   GATEWAY=10.3.2.254
   ```
   ```bash
   [paja444@marcel ~]$ sudo systemctl restart NetworkManager
   [paja444@marcel ~]$ sudo ip r s
   default via 10.3.2.254 dev enp0s3 proto static metric 100 
   10.3.1.0/24 via 10.3.2.254 dev enp0s3 proto static metric 100 
   10.3.2.0/24 dev enp0s3 proto kernel scope link src 10.3.2.12 metric 100 
   [paja444@marcel ~]$ ping 54.37.153.179
   PING 54.37.153.179 (54.37.153.179) 56(84) bytes of data.
   64 bytes from 54.37.153.179: icmp_seq=1 ttl=61 time=24.9 ms
   64 bytes from 54.37.153.179: icmp_seq=2 ttl=61 time=26.5 ms
   ^C
   --- 54.37.153.179 ping statistics ---
   2 packets transmitted, 2 received, 0% packet loss, time 1001ms
   rtt min/avg/max/mdev = 24.912/25.703/26.494/0.791 ms
   ```

- donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser
   *** marcel: ***
   ```bash
   [paja444@john ~]$ sudo nano /etc/resolv.conf 
   [sudo] password for paja444: 
   [paja444@john ~]$ cat /etc/resolv.conf 
   nameserver 8.8.8.8
   nameserver 8.8.4.4
   nameserver 1.1.1.1
   ```
   *** marcel: ***
   ```bash
   [paja444@marcel ~]$ sudo nano /etc/resolv.conf 
   [sudo] password for paja444: 
   [paja444@marcel ~]$ cat /etc/resolv.conf 
   nameserver 8.8.8.8
   nameserver 8.8.4.4
   nameserver 1.1.1.1
   ```

  - vérifiez que vous avez une résolution de noms qui fonctionne avec `dig`
   *** john ***
   ```bash
   [paja444@john ~]$ dig gitlab.com

   ; <<>> DiG 9.16.23-RH <<>> gitlab.com
   ;; global options: +cmd
   ;; Got answer:
   ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 10473
   ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

   ;; OPT PSEUDOSECTION:
   ; EDNS: version: 0, flags:; udp: 512
   ;; QUESTION SECTION:
   ;gitlab.com.			IN	A

   ;; ANSWER SECTION:
   gitlab.com.		300	IN	A	172.65.251.78

   ;; Query time: 32 msec
   ;; SERVER: 8.8.8.8#53(8.8.8.8)
   ;; WHEN: Mon Oct 03 14:16:03 CEST 2022
   ;; MSG SIZE  rcvd: 55
   ```
   *** marcel ***
   ```bash
   [paja444@marcel ~]$ dig gitlab.com

   ; <<>> DiG 9.16.23-RH <<>> gitlab.com
   ;; global options: +cmd
   ;; Got answer:
   ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 35316
   ;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

   ;; OPT PSEUDOSECTION:
   ; EDNS: version: 0, flags:; udp: 512
   ;; QUESTION SECTION:
   ;gitlab.com.			IN	A

   ;; ANSWER SECTION:
   gitlab.com.		300	IN	A	172.65.251.78

   ;; Query time: 33 msec
   ;; SERVER: 8.8.8.8#53(8.8.8.8)
   ;; WHEN: Mon Oct 03 14:16:01 CEST 2022
   ;; MSG SIZE  rcvd: 55
   ```
  - puis avec un `ping` vers un nom de domaine

   ```bash
   [paja444@john ~]$ ping gitlab.com
   PING gitlab.com (172.65.251.78) 56(84) bytes of data.
   64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=1 ttl=61 time=101 ms
   64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=2 ttl=61 time=37.4 ms
   ^C
   --- gitlab.com ping statistics ---
   2 packets transmitted, 2 received, 0% packet loss, time 1001ms
   rtt min/avg/max/mdev = 37.371/69.290/101.209/31.919 ms
   ```
   ```bash
   [paja444@marcel ~]$ ping gitlab.com
   PING gitlab.com (172.65.251.78) 56(84) bytes of data.
   64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=1 ttl=61 time=20.4 ms
   64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=2 ttl=61 time=29.7 ms
   ^C
   --- gitlab.com ping statistics ---
   2 packets transmitted, 2 received, 0% packet loss, time 1002ms
   rtt min/avg/max/mdev = 20.360/25.024/29.688/4.664 ms
   ```

🌞**Analyse de trames**

- effectuez un `ping 8.8.8.8` depuis `john`
   ```bash
   [paja444@john ~]$ ping 8.8.8.8
   PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
   64 bytes from 8.8.8.8: icmp_seq=1 ttl=61 time=19.4 ms
   64 bytes from 8.8.8.8: icmp_seq=2 ttl=61 time=20.6 ms
   ^C
   --- 8.8.8.8 ping statistics ---
   2 packets transmitted, 2 received, 0% packet loss, time 1003ms
   rtt min/avg/max/mdev = 19.370/19.991/20.613/0.621 ms
   ```
- capturez le ping depuis `john` avec `tcpdump`
   ```bash
   [paja444@john ~]$ sudo tcpdump icmp -w ping_john_IP.pcap -i enp0s3
   dropped privs to tcpdump
   tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
   ^C4 packets captured
   4 packets received by filter
   0 packets dropped by kernel
   ```
- analysez un ping aller et le retour qui correspond et mettez dans un tableau :
   ```bash
   (base) paja@paja-mac ~ % scp paja444@10.3.1.11:/home/paja444/ping_john_IP.pcap ./Downloads/
   paja444@10.3.1.11's password: 
   ping_john_IP.pcap                                                 100%  480   225.1KB/s   00:00
   ```
   
john_MAC(enp0s3): `08:00:27:12:7b:b7`
router_MAC(enp0s3): `08:00:27:a6:bd:69`
| ordre | type trame | IP source          | MAC source                 | IP destination   | MAC destination            |
|-------|------------|--------------------|----------------------------|------------------|----------------------------|
| 1     | ping       | `john` `10.3.1.11` |`john` `08:00:27:12:7b:b7`  |      `8.8.8.8`   |`router` `08:00:27:a6:bd:69`|
| 2     | pong       | `8.8.8.8`          |`router` `08:00:27:a6:bd:69`|`john` `10.3.1.11`| `john` `08:00:27:12:7b:b7` |

🦈 **Capture réseau `tp2_routage_internet.pcapng`**
[ping_john_IP.pcap](./PCAP/ping_john_IP.pcap)

## III. DHCP

On reprend la config précédente, et on ajoutera à la fin de cette partie une 4ème machine pour effectuer des tests.

| Machine  | `10.3.1.0/24`              | `10.3.2.0/24` |
|----------|----------------------------|---------------|
| `router` | `10.3.1.254`               | `10.3.2.254`  |
| `john`   | `10.3.1.11`                | no            |
| `bob`    | oui mais pas d'IP statique | no            |
| `marcel` | no                         | `10.3.2.12`   |

```schema
   john               router              marcel
  ┌─────┐             ┌─────┐             ┌─────┐
  │     │    ┌───┐    │     │    ┌───┐    │     │
  │     ├────┤ho1├────┤     ├────┤ho2├────┤     │
  └─────┘    └─┬─┘    └─────┘    └───┘    └─────┘
   john        │
  ┌─────┐      │
  │     │      │
  │     ├──────┘
  └─────┘
```

### 1. Mise en place du serveur DHCP

🌞**Sur la machine `john`, vous installerez et configurerez un serveur DHCP** (go Google "rocky linux dhcp server").

- installation du serveur sur `john`
   ```bash
   [paja444@john ~]$ sudo dnf install dhcp-server
   [sudo] password for paja444: 
   Rocky Linux 9 - BaseOS                          6.0 kB/s | 3.6 kB     00:00    
   Rocky Linux 9 - AppStream                       8.2 kB/s | 3.6 kB     00:00    
   Rocky Linux 9 - Extras                          3.2 kB/s | 2.9 kB     00:00    
   Dependencies resolved.
   ================================================================================
   Package            Architecture  Version                   Repository     Size
   ================================================================================
   Installing:
   dhcp-server        x86_64        12:4.4.2-15.b1.el9        baseos        1.2 M
   Installing dependencies:
   dhcp-common        noarch        12:4.4.2-15.b1.el9        baseos        128 k

   Transaction Summary
   ================================================================================
   Install  2 Packages

   Total download size: 1.3 M
   Installed size: 4.2 M
   Is this ok [y/N]: y
   Downloading Packages:
   (1/2): dhcp-common-4.4.2-15.b1.el9.noarch.rpm   274 kB/s | 128 kB     00:00    
   (2/2): dhcp-server-4.4.2-15.b1.el9.x86_64.rpm   1.8 MB/s | 1.2 MB     00:00    
   --------------------------------------------------------------------------------
   Total                                           1.2 MB/s | 1.3 MB     00:01     
   Running transaction check
   Transaction check succeeded.
   Running transaction test
   Transaction test succeeded.
   Running transaction
   Preparing        :                                                        1/1 
   Installing       : dhcp-common-12:4.4.2-15.b1.el9.noarch                  1/2 
   Running scriptlet: dhcp-server-12:4.4.2-15.b1.el9.x86_64                  2/2 
   useradd warning: dhcpd's uid 177 outside of the SYS_UID_MIN 201 and SYS_UID_MAX 999 range.

   Installing       : dhcp-server-12:4.4.2-15.b1.el9.x86_64                  2/2 
   Running scriptlet: dhcp-server-12:4.4.2-15.b1.el9.x86_64                  2/2 
   Verifying        : dhcp-server-12:4.4.2-15.b1.el9.x86_64                  1/2 
   Verifying        : dhcp-common-12:4.4.2-15.b1.el9.noarch                  2/2 

   Installed:
   dhcp-common-12:4.4.2-15.b1.el9.noarch  dhcp-server-12:4.4.2-15.b1.el9.x86_64 

   Complete!
   ```
   ```bash
   [paja444@john ~]$ sudo cp -r /etc/dhcp /etc/dhcp_backup
   ```
   ```bash
   [paja444@john ~]$ sudo nano /etc/dhcp/dhcpd.conf
   ```
   ```bash
   sudo cat /etc/dhcp/dhcpd.conf
   [sudo] password for paja444: 
   #
   # DHCP Server Configuration file.
   #   see /usr/share/doc/dhcp-server/dhcpd.conf.example
   #   see dhcpd.conf(5) man page
   #
   default-lease-time 900;
   max-lease-time 10800;
   ddns-update-style none;
   authoritative;
   subnet 10.3.1.0 netmask 255.255.255.0 {
   range 10.3.1.12 10.3.1.253;
   }
   ```
   ```bash
   [paja444@john ~]$ sudo firewall-cmd --permanent --add-port=67/udp
   success
   ```

   ```bash
   [paja444@john ~]$ sudo systemctl enable dhcpd
   Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
   [paja444@john ~]$ sudo systemctl start dhcpd
   [paja444@john ~]$ sudo systemctl status dhcpd
   ● dhcpd.service - DHCPv4 Server Daemon
      Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; vendor preset: disabled)
      Active: active (running) since Mon 2022-10-03 14:36:11 CEST; 5s ago
         Docs: man:dhcpd(8)
               man:dhcpd.conf(5)
      Main PID: 1584 (dhcpd)
      Status: "Dispatching packets..."
         Tasks: 1 (limit: 5907)
      Memory: 4.6M
         CPU: 14ms
      CGroup: /system.slice/dhcpd.service
               └─1584 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid

   Oct 03 14:36:11 john dhcpd[1584]: Config file: /etc/dhcp/dhcpd.conf
   Oct 03 14:36:11 john dhcpd[1584]: Database file: /var/lib/dhcpd/dhcpd.leases
   Oct 03 14:36:11 john dhcpd[1584]: PID file: /var/run/dhcpd.pid
   Oct 03 14:36:11 john dhcpd[1584]: Source compiled to use binary-leases
   Oct 03 14:36:11 john dhcpd[1584]: Wrote 0 leases to leases file.
   Oct 03 14:36:11 john dhcpd[1584]: Listening on LPF/enp0s3/08:00:27:12:7b:b7/10.3.1.0/24
   Oct 03 14:36:11 john dhcpd[1584]: Sending on   LPF/enp0s3/08:00:27:12:7b:b7/10.3.1.0/24
   Oct 03 14:36:11 john dhcpd[1584]: Sending on   Socket/fallback/fallback-net
   Oct 03 14:36:11 john dhcpd[1584]: Server starting service.
   Oct 03 14:36:11 john systemd[1]: Started DHCPv4 Server Daemon.
   ```

- créer une machine `bob`
   ```bash
   [paja444@bob ~]$ ip a
   1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
      link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
      inet 127.0.0.1/8 scope host lo
         valid_lft forever preferred_lft forever
      inet6 ::1/128 scope host 
         valid_lft forever preferred_lft forever
   2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
      link/ether 08:00:27:7b:d9:88 brd ff:ff:ff:ff:ff:ff
      inet 10.3.1.12/24 brd 10.3.1.255 scope global dynamic noprefixroute enp0s3
         valid_lft 791sec preferred_lft 791sec
      inet6 fe80::a00:27ff:fe7b:d988/64 scope link noprefixroute 
         valid_lft forever preferred_lft forever
   ```
- faites lui récupérer une IP en DHCP à l'aide de votre serveur
   ```bash
   [paja444@bob ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-en0s3
   ```
   ```bash
   NAME=enp0s3
   DEVICE=enp0s3

   BOOTPROTO=dhcp
   ONBOOT=yes
   ```
   ```bash
   sudo systemctl restart NetworkManager
   ```
   ```bash
   [paja444@bob ~]$ sudo dnf install dhclient
   Rocky Linux 9 - BaseOS                                              2.0 kB/s | 3.6 kB     00:01    
   Rocky Linux 9 - AppStream                                           8.1 kB/s | 3.6 kB     00:00    
   Rocky Linux 9 - Extras                                              5.3 kB/s | 2.9 kB     00:00    
   Dependencies resolved.
   ====================================================================================================
   Package                    Architecture     Version                      Repository           Size
   ====================================================================================================
   Installing:
   dhcp-client                x86_64           12:4.4.2-15.b1.el9           baseos              787 k
   Installing dependencies:
   dhcp-common                noarch           12:4.4.2-15.b1.el9           baseos              128 k
   ipcalc                     x86_64           1.0.0-5.el9                  baseos               41 k
   Installing weak dependencies:
   geolite2-city              noarch           20191217-6.el9               appstream            23 M
   geolite2-country           noarch           20191217-6.el9               appstream           1.6 M

   Transaction Summary
   ====================================================================================================
   Install  5 Packages

   Total download size: 25 M
   Installed size: 66 M
   Is this ok [y/N]: y
   Downloading Packages:
   (1/5): ipcalc-1.0.0-5.el9.x86_64.rpm                                195 kB/s |  41 kB     00:00    
   (2/5): dhcp-common-4.4.2-15.b1.el9.noarch.rpm                       416 kB/s | 128 kB     00:00    
   (3/5): dhcp-client-4.4.2-15.b1.el9.x86_64.rpm                       929 kB/s | 787 kB     00:00    
   (4/5): geolite2-country-20191217-6.el9.noarch.rpm                   1.3 MB/s | 1.6 MB     00:01    
   (5/5): geolite2-city-20191217-6.el9.noarch.rpm                      3.2 MB/s |  23 MB     00:07    
   ----------------------------------------------------------------------------------------------------
   Total                                                               3.1 MB/s |  25 MB     00:08     
   Running transaction check
   Transaction check succeeded.
   Running transaction test
   Transaction test succeeded.
   Running transaction
   Preparing        :                                                                            1/1 
   Installing       : geolite2-city-20191217-6.el9.noarch                                        1/5 
   Installing       : geolite2-country-20191217-6.el9.noarch                                     2/5 
   Installing       : ipcalc-1.0.0-5.el9.x86_64                                                  3/5 
   Installing       : dhcp-common-12:4.4.2-15.b1.el9.noarch                                      4/5 
   Installing       : dhcp-client-12:4.4.2-15.b1.el9.x86_64                                      5/5 
   Running scriptlet: dhcp-client-12:4.4.2-15.b1.el9.x86_64                                      5/5 
   Verifying        : ipcalc-1.0.0-5.el9.x86_64                                                  1/5 
   Verifying        : dhcp-client-12:4.4.2-15.b1.el9.x86_64                                      2/5 
   Verifying        : dhcp-common-12:4.4.2-15.b1.el9.noarch                                      3/5 
   Verifying        : geolite2-country-20191217-6.el9.noarch                                     4/5 
   Verifying        : geolite2-city-20191217-6.el9.noarch                                        5/5 

   Installed:
   dhcp-client-12:4.4.2-15.b1.el9.x86_64            dhcp-common-12:4.4.2-15.b1.el9.noarch            
   geolite2-city-20191217-6.el9.noarch              geolite2-country-20191217-6.el9.noarch           
   ipcalc-1.0.0-5.el9.x86_64                       

   Complete!
   ```
   ```bash
   [paja444@bob ~]$ sudo dhclient -r -v enp0s3
   Internet Systems Consortium DHCP Client 4.4.2b1
   Copyright 2004-2019 Internet Systems Consortium.
   All rights reserved.
   For info, please visit https://www.isc.org/software/dhcp/

   Listening on LPF/enp0s3/08:00:27:7b:d9:88
   Sending on   LPF/enp0s3/08:00:27:7b:d9:88
   Sending on   Socket/fallback
   ```
   ```bash
   [paja444@bob ~]$ sudo dhclient -v
   Internet Systems Consortium DHCP Client 4.4.2b1
   Copyright 2004-2019 Internet Systems Consortium.
   All rights reserved.
   For info, please visit https://www.isc.org/software/dhcp/

   Listening on LPF/enp0s3/08:00:27:7b:d9:88
   Sending on   LPF/enp0s3/08:00:27:7b:d9:88
   Sending on   Socket/fallback
   DHCPDISCOVER on enp0s3 to 255.255.255.255 port 67 interval 8 (xid=0xa042d309)
   DHCPOFFER of 10.3.1.13 from 10.3.1.11
   DHCPREQUEST for 10.3.1.13 on enp0s3 to 255.255.255.255 port 67 (xid=0xa042d309)
   DHCPACK of 10.3.1.13 from 10.3.1.11 (xid=0xa042d309)
   bound to 10.3.1.13 -- renewal in 443 seconds.
   ```
> Il est possible d'utilise la commande `dhclient` pour forcer à la main, depuis la ligne de commande, la demande d'une IP en DHCP, ou renouveler complètement l'échange DHCP (voir `dhclient -h` puis call me et/ou Google si besoin d'aide).

🌞**Améliorer la configuration du DHCP**

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :
  - une route par défaut
  - un serveur DNS à utiliser
   ```bash
   [paja444@john ~]$ sudo nano /etc/dhcp/dhcpd.conf
   ```
   ```bash
   sudo cat /etc/dhcp/dhcpd.conf
   [sudo] password for paja444: 
   #
   # DHCP Server Configuration file.
   #   see /usr/share/doc/dhcp-server/dhcpd.conf.example
   #   see dhcpd.conf(5) man page
   #
   default-lease-time 900;
   max-lease-time 10800;
   ddns-update-style none;
   authoritative;
   subnet 10.3.1.0 netmask 255.255.255.0 {
   range 10.3.1.12 10.3.1.253;
   option routers 10.3.1.254;
   option subnet-mask 255.255.255.0;
   option domain-name-servers 8.8.8.8;
   }
   ```
- récupérez de nouveau une IP en DHCP sur `marcel` pour tester :
  - `marcel` doit avoir une IP
    - vérifier avec une commande qu'il a récupéré son IP

      ```bash
      [paja444@bob ~]$ sudo dhclient -r -v enp0s3
      [sudo] password for paja444: 
      Killed old client process
      Internet Systems Consortium DHCP Client 4.4.2b1
      Copyright 2004-2019 Internet Systems Consortium.
      All rights reserved.
      For info, please visit https://www.isc.org/software/dhcp/

      Listening on LPF/enp0s3/08:00:27:7b:d9:88
      Sending on   LPF/enp0s3/08:00:27:7b:d9:88
      Sending on   Socket/fallback
      DHCPRELEASE of 10.3.1.13 on enp0s3 to 10.3.1.11 port 67 (xid=0x13fcc205)
      [paja444@bob ~]$ sudo dhclient -v
      Internet Systems Consortium DHCP Client 4.4.2b1
      Copyright 2004-2019 Internet Systems Consortium.
      All rights reserved.
      For info, please visit https://www.isc.org/software/dhcp/

      Listening on LPF/enp0s3/08:00:27:7b:d9:88
      Sending on   LPF/enp0s3/08:00:27:7b:d9:88
      Sending on   Socket/fallback
      DHCPDISCOVER on enp0s3 to 255.255.255.255 port 67 interval 6 (xid=0x9731711)
      DHCPOFFER of 10.3.1.13 from 10.3.1.11
      DHCPREQUEST for 10.3.1.13 on enp0s3 to 255.255.255.255 port 67 (xid=0x9731711)
      DHCPACK of 10.3.1.13 from 10.3.1.11 (xid=0x9731711)
      bound to 10.3.1.13 -- renewal in 386 seconds.
      ```
    - vérifier qu'il peut `ping` sa passerelle
      ```bash
      [paja444@bob ~]$ ping 10.3.1.254
      PING 10.3.1.254 (10.3.1.254) 56(84) bytes of data.
      64 bytes from 10.3.1.254: icmp_seq=1 ttl=64 time=0.905 ms
      64 bytes from 10.3.1.254: icmp_seq=2 ttl=64 time=0.609 ms
      ^C
      --- 10.3.1.254 ping statistics ---
      2 packets transmitted, 2 received, 0% packet loss, time 1002ms
      rtt min/avg/max/mdev = 0.609/0.757/0.905/0.148 ms
      ```
  - il doit avoir une route par défaut
    - vérifier la présence de la route avec une commande
      ```bash
      [paja444@bob ~]$ ip r s 
      default via 10.3.1.254 dev enp0s3 
      default via 10.3.1.254 dev enp0s3 proto dhcp src 10.3.1.12 metric 100 
      10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.12 metric 100 
      ```

    - vérifier que la route fonctionne avec un `ping` vers une IP
         ```bash
         [paja444@bob ~]$ ping 54.37.153.179
         PING 54.37.153.179 (54.37.153.179) 56(84) bytes of data.
         64 bytes from 54.37.153.179: icmp_seq=1 ttl=61 time=25.8 ms
         64 bytes from 54.37.153.179: icmp_seq=2 ttl=61 time=47.5 ms
         ^C
         --- 54.37.153.179 ping statistics ---
         2 packets transmitted, 2 received, 0% packet loss, time 1002ms
         rtt min/avg/max/mdev = 25.769/36.610/47.452/10.841 ms
         ```
  - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
    - vérifier avec la commande `dig` que ça fonctionne
      ```bash
      [paja444@bob ~]$ dig 8.8.8.8

      ; <<>> DiG 9.16.23-RH <<>> 8.8.8.8
      ;; global options: +cmd
      ;; Got answer:
      ;; ->>HEADER<<- opcode: QUERY, status: NXDOMAIN, id: 53323
      ;; flags: qr aa rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 1, ADDITIONAL: 1

      ;; OPT PSEUDOSECTION:
      ; EDNS: version: 0, flags:; udp: 1232
      ;; QUESTION SECTION:
      ;8.8.8.8.			IN	A

      ;; AUTHORITY SECTION:
      .			86400	IN	SOA	a.root-servers.net. nstld.verisign-grs.com. 2022100200 1800 900 604800 86400

      ;; Query time: 22 msec
      ;; SERVER: 1.1.1.1#53(1.1.1.1)
      ;; WHEN: Sun Oct 02 13:19:13 CEST 2022
      ;; MSG SIZE  rcvd: 111
      ```
    - vérifier un `ping` vers un nom de domaine
      ```bash
      [paja444@bob ~]$ ping ynov.com
      PING ynov.com (172.67.74.226) 56(84) bytes of data.
      64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=1 ttl=61 time=20.0 ms
      64 bytes from 172.67.74.226 (172.67.74.226): icmp_seq=2 ttl=61 time=20.6 ms
      ^C
      --- ynov.com ping statistics ---
      2 packets transmitted, 2 received, 0% packet loss, time 1002ms
      rtt min/avg/max/mdev = 19.971/20.262/20.554/0.291 ms
      ```

### 2. Analyse de trames

🌞**Analyse de trames**

- lancer une capture à l'aide de `tcpdump` afin de capturer un échange DHCP
   ```bash
   [paja444@bob ~]$ sudo tcpdump -w bobDHCPrequest.pcap -i enp0s3
   [sudo] password for paja444: 
   dropped privs to tcpdump
   tcpdump: listening on enp0s3, link-type EN10MB (Ethernet), snapshot length 262144 bytes
   ^C45 packets captured
   48 packets received by filter
   0 packets dropped by kernel
   ```
- demander une nouvelle IP afin de générer un échange DHCP
   ```bash
   [paja444@bob ~]$ sudo dhclient -r
   Removed stale PID file
   [paja444@bob ~]$ sudo dhclient
   ```
- exportez le fichier `.pcapng`
   ```bash
   [paja444@bob ~]$ exit
   logout
   Connection to 10.3.1.12 closed.
   (base) paja@paja-mac ~ % scp paja444@10.3.1.12:/home/paja444/bobDHCPrequest.pcap ./Downloads/
   paja444@10.3.1.12's password: 
   bobDHCPrequest.pcap                           100% 5080     3.5MB/s   00:00  
   ```


🦈 **Capture réseau `tp2_dhcp.pcapng`**
[bobDHCPrequest.pcap](./PACP/bobDHCPrequest.pcap)
