#!/usr/bin/env python3  Line 1
# -*- coding: utf-8 -*- Line 2
#----------------------------------------------------------------------------
# Created By  : Alexandre Pajak
# Created Date: 23/10/2022
# version ='1.0'
# description : simple Flask server for ARP fishing
# ---------------------------------------------------------------------------

from flask import Flask, render_template

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)