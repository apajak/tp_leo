#!/usr/bin/env python3  Line 1
# -*- coding: utf-8 -*- Line 2
#----------------------------------------------------------------------------
# Created By  : Alexandre Pajak
# Created Date: 23/10/2022
# version ='1.0'
# description : ARP network scan 
# ---------------------------------------------------------------------------
import scapy.all as scapy
import sys


def scan(network_address="10.3.1.1/24"):
    request = scapy.ARP()

    request.pdst = network_address
    broadcast = scapy.Ether()

    broadcast.dst = "ff:ff:ff:ff:ff:ff"

    request_broadcast = broadcast / request
    clients = scapy.srp(request_broadcast, timeout=1)[0]
    for element in clients:
        print(element[1].psrc + "	 " + element[1].hwsrc)


if __name__ == "__main__":
    try:
        scan(sys.argv[1])
    except IndexError as e:
        print("scan as default network 10.3.1.1/24")
        scan()
