# TP2 : Ethernet, IP, et ARP

Dans ce TP on va approfondir trois protocoles, qu'on a survolé jusqu'alors :

- **IPv4** *(Internet Protocol Version 4)* : gestion des adresses IP
  - on va aussi parler d'ICMP, de DHCP, bref de tous les potes d'IP quoi !
- **Ethernet** : gestion des adresses MAC
- **ARP** *(Address Resolution Protocol)* : permet de trouver l'adresse MAC de quelqu'un sur notre réseau dont on connaît l'adresse IP

![Seventh Day](./pics/tcpip.jpg)

# Sommaire

- [TP2 : Ethernet, IP, et ARP](#tp2--ethernet-ip-et-arp)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
- [I. Setup IP](#i-setup-ip)
- [II. ARP my bro](#ii-arp-my-bro)
- [II.5 Interlude hackerzz](#ii5-interlude-hackerzz)
- [III. DHCP you too my brooo](#iii-dhcp-you-too-my-brooo)
- [IV. Avant-goût TCP et UDP](#iv-avant-goût-tcp-et-udp)

# 0. Prérequis

**Il vous faudra deux machines**, vous êtes libres :

- toujours possible de se connecter à deux avec un câble
- sinon, votre PC + une VM ça fait le taf, c'est pareil
  - je peux aider sur le setup, comme d'hab

> Je conseille à tous les gens qui n'ont pas de port RJ45 de go PC + VM pour faire vous-mêmes les manips, mais on fait au plus simple hein.

---

**Toutes les manipulations devront être effectuées depuis la ligne de commande.** Donc normalement, plus de screens.

**Pour Wireshark, c'est pareil,** NO SCREENS. La marche à suivre :

- vous capturez le trafic que vous avez à capturer
- vous stoppez la capture (bouton carré rouge en haut à gauche)
- vous sélectionner les paquets/trames intéressants (CTRL + clic)
- File > Export Specified Packets...
- dans le menu qui s'ouvre, cochez en bas "Selected packets only"
- sauvegardez, ça produit un fichier `.pcapng` (qu'on appelle communément "un ptit PCAP frer") que vous livrerez dans le dépôt git

**Si vous voyez le p'tit pote 🦈 c'est qu'il y a un PCAP à produire et à mettre dans votre dépôt git de rendu.**

# I. Setup IP

Le lab, il vous faut deux machine : 

- les deux machines doivent être connectées physiquement
- vous devez choisir vous-mêmes les IPs à attribuer sur les interfaces réseau, les contraintes :
  - IPs privées (évidemment n_n)
  - dans un réseau qui peut contenir au moins 38 adresses IP (il faut donc choisir un masque adapté)
  - oui c'est random, on s'exerce c'est tout, p'tit jog en se levant
  - le masque choisi doit être le plus grand possible (le plus proche de 32 possible) afin que le réseau soit le plus petit possible

🌞 **Mettez en place une configuration réseau fonctionnelle entre les deux machines**

- vous renseignerez dans le compte rendu :
  - les deux IPs choisies, en précisant le masque
  Nous avont choisi les IP : 192.168.1.1/26 et 182.168.1.2/26
  - l'adresse de réseau
  Notre adresse de reseau est donc : 192.168.1.0
  - l'adresse de broadcast
  Notre adresse de broadcast est : 192.168.1.63
- vous renseignerez aussi les commandes utilisées pour définir les adresses IP *via* la ligne de commande
  ```bash
  (base) paja@paja-mac ~ % sudo ipconfig set en7 INFORM 192.168.1.1 255.255.255.192
  ```
  ```bash
  (base) paja@paja-mac ~ % ifconfig en7                                            
  en7: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=6467<RXCSUM,TXCSUM,VLAN_MTU,TSO4,TSO6,CHANNEL_IO,PARTIAL_CSUM,ZEROINVERT_CSUM>
	ether 00:e0:4c:68:2b:94 
	inet6 fe80::409:3174:7081:99f9%en7 prefixlen 64 secured scopeid 0x13 
	inet 192.168.1.1 netmask 0xffffffc0 broadcast 192.168.1.63
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect (1000baseT <full-duplex>)
	status: active
  ```

> Rappel : tout doit être fait *via* la ligne de commandes. Faites-vous du bien, et utilisez Powershell plutôt que l'antique cmd sous Windows svp.

🌞 **Prouvez que la connexion est fonctionnelle entre les deux machines**

- un `ping` suffit !

  ```bash
  (base) paja@paja-mac ~ % ping 192.168.1.2
  PING 192.168.1.2 (192.168.1.2): 56 data bytes
  64 bytes from 192.168.1.2: icmp_seq=0 ttl=64 time=1.077 ms
  64 bytes from 192.168.1.2: icmp_seq=1 ttl=64 time=1.573 ms
  64 bytes from 192.168.1.2: icmp_seq=2 ttl=64 time=1.569 ms
  64 bytes from 192.168.1.2: icmp_seq=3 ttl=64 time=1.507 ms
  64 bytes from 192.168.1.2: icmp_seq=4 ttl=64 time=1.050 ms
  ^C
  ```


🌞 **Wireshark it**

- `ping` ça envoie des paquets de type ICMP (c'est pas de l'IP, c'est un de ses frères)
  - les paquets ICMP sont encapsulés dans des trames Ethernet, comme les paquets IP
  - il existe plusieurs types de paquets ICMP, qui servent à faire des trucs différents
- **déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par `ping`**
  - pour le ping que vous envoyez
  8–Echo Request
  - et le pong que vous recevez en retour
  0-Echo Reply
> Vous trouverez sur [la page Wikipedia de ICMP](https://en.wikipedia.org/wiki/Internet_Control_Message_Protocol) un tableau qui répertorie tous les types ICMP et leur utilité

🦈 **PCAP qui contient les paquets ICMP qui vous ont permis d'identifier les types ICMP**

[PCAP-Ping 🦈 ](./PCAP/ping.pcapng)

# II. ARP my bro

ARP permet, pour rappel, de résoudre la situation suivante :

- pour communiquer avec quelqu'un dans un LAN, il **FAUT** connaître son adresse MAC
- on admet un PC1 et un PC2 dans le même LAN :
  - PC1 veut joindre PC2
  - PC1 et PC2 ont une IP correctement définie
  - PC1 a besoin de connaître la MAC de PC2 pour lui envoyer des messages
  - **dans cette situation, PC1 va utilise le protocole ARP pour connaître la MAC de PC2**
  - une fois que PC1 connaît la mac de PC2, il l'enregistre dans sa **table ARP**

🌞 **Check the ARP table**

- utilisez une commande pour afficher votre table ARP
- déterminez la MAC de votre binome depuis votre table ARP
```bash
(base) paja@paja-mac ~ % arp -a | grep 192.168.1.2
? (192.168.1.2) at 0:e0:4c:68:23:2b on en7 ifscope [ethernet]
```
- déterminez la MAC de la *gateway* de votre réseau 
  - celle de votre réseau physique, WiFi, genre YNOV, car il n'y en a pas dans votre ptit LAN
  - c'est juste pour vous faire manipuler un peu encore :)
  ```bash
  (base) paja@paja-mac ~ % netstat -nr | grep default 
  ```
> Il peut être utile de ré-effectuer des `ping` avant d'afficher la table ARP. En effet : les infos stockées dans la table ARP ne sont stockées que temporairement. Ce laps de temps est de l'ordre de ~60 secondes sur la plupart de nos machines.

🌞 **Manipuler la table ARP**

- utilisez une commande pour vider votre table ARP
```bash
(base) paja@paja-mac ~ % sudo arp -a -d
Password:
10.33.16.0 (10.33.16.0) deleted
10.33.16.4 (10.33.16.4) deleted
10.33.16.8 (10.33.16.8) deleted
10.33.16.12 (10.33.16.12) deleted
10.33.16.14 (10.33.16.14) deleted
10.33.16.16 (10.33.16.16) deleted
10.33.16.40 (10.33.16.40) deleted
10.33.16.43 (10.33.16.43) deleted
10.33.16.44 (10.33.16.44) deleted
10.33.16.51 (10.33.16.51) deleted
10.33.16.54 (10.33.16.54) deleted
10.33.16.57 (10.33.16.57) deleted
10.33.16.59 (10.33.16.59) deleted
10.33.16.61 (10.33.16.61) deleted
10.33.16.63 (10.33.16.63) deleted
10.33.16.65 (10.33.16.65) deleted
10.33.16.66 (10.33.16.66) deleted
10.33.16.69 (10.33.16.69) deleted
10.33.16.73 (10.33.16.73) deleted
10.33.16.83 (10.33.16.83) deleted
10.33.16.94 (10.33.16.94) deleted
10.33.16.96 (10.33.16.96) deleted
10.33.16.97 (10.33.16.97) deleted
10.33.16.101 (10.33.16.101) deleted
10.33.16.104 (10.33.16.104) deleted
10.33.16.107 (10.33.16.107) deleted
10.33.16.108 (10.33.16.108) deleted
10.33.16.127 (10.33.16.127) deleted
10.33.16.129 (10.33.16.129) deleted
10.33.16.132 (10.33.16.132) deleted
10.33.16.135 (10.33.16.135) deleted
10.33.16.144 (10.33.16.144) deleted
10.33.16.145 (10.33.16.145) deleted
10.33.16.146 (10.33.16.146) deleted
10.33.16.148 (10.33.16.148) deleted
10.33.16.150 (10.33.16.150) deleted
10.33.16.151 (10.33.16.151) deleted
10.33.16.156 (10.33.16.156) deleted
10.33.16.157 (10.33.16.157) deleted
10.33.16.158 (10.33.16.158) deleted
10.33.16.159 (10.33.16.159) deleted
10.33.16.160 (10.33.16.160) deleted
10.33.16.163 (10.33.16.163) deleted
10.33.16.164 (10.33.16.164) deleted
10.33.16.166 (10.33.16.166) deleted
10.33.16.168 (10.33.16.168) deleted
10.33.16.170 (10.33.16.170) deleted
10.33.16.171 (10.33.16.171) deleted
10.33.16.172 (10.33.16.172) deleted
10.33.16.173 (10.33.16.173) deleted
10.33.16.174 (10.33.16.174) deleted
10.33.16.175 (10.33.16.175) deleted
10.33.16.176 (10.33.16.176) deleted
10.33.16.185 (10.33.16.185) deleted
10.33.16.187 (10.33.16.187) deleted
10.33.16.190 (10.33.16.190) deleted
10.33.16.191 (10.33.16.191) deleted
10.33.16.192 (10.33.16.192) deleted
10.33.16.193 (10.33.16.193) deleted
10.33.16.195 (10.33.16.195) deleted
10.33.16.196 (10.33.16.196) deleted
10.33.16.197 (10.33.16.197) deleted
10.33.16.198 (10.33.16.198) deleted
10.33.16.199 (10.33.16.199) deleted
10.33.16.200 (10.33.16.200) deleted
10.33.16.204 (10.33.16.204) deleted
10.33.16.205 (10.33.16.205) deleted
10.33.16.207 (10.33.16.207) deleted
10.33.16.208 (10.33.16.208) deleted
10.33.16.209 (10.33.16.209) deleted
10.33.16.210 (10.33.16.210) deleted
10.33.16.212 (10.33.16.212) deleted
10.33.16.213 (10.33.16.213) deleted
10.33.16.214 (10.33.16.214) deleted
10.33.16.215 (10.33.16.215) deleted
10.33.16.217 (10.33.16.217) deleted
10.33.16.219 (10.33.16.219) deleted
10.33.16.222 (10.33.16.222) deleted
10.33.16.223 (10.33.16.223) deleted
10.33.16.224 (10.33.16.224) deleted
10.33.16.225 (10.33.16.225) deleted
10.33.16.227 (10.33.16.227) deleted
10.33.16.228 (10.33.16.228) deleted
10.33.16.232 (10.33.16.232) deleted
10.33.16.233 (10.33.16.233) deleted
10.33.16.235 (10.33.16.235) deleted
10.33.16.237 (10.33.16.237) deleted
10.33.16.238 (10.33.16.238) deleted
10.33.16.239 (10.33.16.239) deleted
10.33.16.240 (10.33.16.240) deleted
10.33.16.242 (10.33.16.242) deleted
10.33.16.243 (10.33.16.243) deleted
10.33.16.246 (10.33.16.246) deleted
10.33.16.248 (10.33.16.248) deleted
10.33.16.249 (10.33.16.249) deleted
10.33.16.251 (10.33.16.251) deleted
10.33.16.252 (10.33.16.252) deleted
10.33.16.253 (10.33.16.253) deleted
10.33.16.254 (10.33.16.254) deleted
10.33.16.255 (10.33.16.255) deleted
10.33.17.0 (10.33.17.0) deleted
10.33.17.1 (10.33.17.1) deleted
10.33.17.2 (10.33.17.2) deleted
10.33.17.3 (10.33.17.3) deleted
10.33.17.4 (10.33.17.4) deleted
10.33.17.6 (10.33.17.6) deleted
10.33.17.8 (10.33.17.8) deleted
10.33.17.9 (10.33.17.9) deleted
10.33.17.12 (10.33.17.12) deleted
10.33.17.14 (10.33.17.14) deleted
10.33.17.16 (10.33.17.16) deleted
10.33.17.17 (10.33.17.17) deleted
10.33.17.18 (10.33.17.18) deleted
10.33.17.22 (10.33.17.22) deleted
10.33.17.26 (10.33.17.26) deleted
10.33.17.29 (10.33.17.29) deleted
10.33.17.31 (10.33.17.31) deleted
10.33.17.47 (10.33.17.47) deleted
10.33.17.48 (10.33.17.48) deleted
10.33.17.51 (10.33.17.51) deleted
10.33.17.55 (10.33.17.55) deleted
10.33.17.57 (10.33.17.57) deleted
10.33.18.81 (10.33.18.81) deleted
10.33.18.83 (10.33.18.83) deleted
10.33.18.89 (10.33.18.89) deleted
10.33.18.90 (10.33.18.90) deleted
10.33.18.93 (10.33.18.93) deleted
10.33.18.94 (10.33.18.94) deleted
10.33.18.97 (10.33.18.97) deleted
10.33.18.101 (10.33.18.101) deleted
10.33.18.102 (10.33.18.102) deleted
10.33.18.103 (10.33.18.103) deleted
10.33.18.108 (10.33.18.108) deleted
10.33.18.114 (10.33.18.114) deleted
10.33.18.116 (10.33.18.116) deleted
10.33.18.121 (10.33.18.121) deleted
10.33.18.123 (10.33.18.123) deleted
10.33.18.126 (10.33.18.126) deleted
10.33.18.128 (10.33.18.128) deleted
10.33.18.131 (10.33.18.131) deleted
10.33.18.132 (10.33.18.132) deleted
10.33.18.133 (10.33.18.133) deleted
delete: cannot locate 10.33.18.161
10.33.18.184 (10.33.18.184) deleted
10.33.18.186 (10.33.18.186) deleted
10.33.18.187 (10.33.18.187) deleted
10.33.18.188 (10.33.18.188) deleted
10.33.18.193 (10.33.18.193) deleted
10.33.19.55 (10.33.19.55) deleted
10.33.19.56 (10.33.19.56) deleted
10.33.19.58 (10.33.19.58) deleted
10.33.19.59 (10.33.19.59) deleted
10.33.19.60 (10.33.19.60) deleted
10.33.19.61 (10.33.19.61) deleted
10.33.19.62 (10.33.19.62) deleted
10.33.19.63 (10.33.19.63) deleted
10.33.19.64 (10.33.19.64) deleted
10.33.19.65 (10.33.19.65) deleted
10.33.19.67 (10.33.19.67) deleted
10.33.19.68 (10.33.19.68) deleted
10.33.19.69 (10.33.19.69) deleted
10.33.19.70 (10.33.19.70) deleted
10.33.19.71 (10.33.19.71) deleted
10.33.19.72 (10.33.19.72) deleted
10.33.19.74 (10.33.19.74) deleted
10.33.19.124 (10.33.19.124) deleted
10.33.19.126 (10.33.19.126) deleted
10.33.19.128 (10.33.19.128) deleted
10.33.19.129 (10.33.19.129) deleted
10.33.19.130 (10.33.19.130) deleted
10.33.19.134 (10.33.19.134) deleted
10.33.19.138 (10.33.19.138) deleted
10.33.19.153 (10.33.19.153) deleted
10.33.19.157 (10.33.19.157) deleted
10.33.19.159 (10.33.19.159) deleted
10.33.19.165 (10.33.19.165) deleted
10.33.19.169 (10.33.19.169) deleted
10.33.19.177 (10.33.19.177) deleted
10.33.19.191 (10.33.19.191) deleted
10.33.19.192 (10.33.19.192) deleted
10.33.19.197 (10.33.19.197) deleted
10.33.19.207 (10.33.19.207) deleted
10.33.19.209 (10.33.19.209) deleted
10.33.19.210 (10.33.19.210) deleted
10.33.19.225 (10.33.19.225) deleted
10.33.19.226 (10.33.19.226) deleted
10.33.19.227 (10.33.19.227) deleted
10.33.19.229 (10.33.19.229) deleted
10.33.19.230 (10.33.19.230) deleted
10.33.19.232 (10.33.19.232) deleted
10.33.19.233 (10.33.19.233) deleted
10.33.19.234 (10.33.19.234) deleted
10.33.19.235 (10.33.19.235) deleted
10.33.19.236 (10.33.19.236) deleted
10.33.19.237 (10.33.19.237) deleted
10.33.19.238 (10.33.19.238) deleted
10.33.19.239 (10.33.19.239) deleted
10.33.19.240 (10.33.19.240) deleted
10.33.19.241 (10.33.19.241) deleted
10.33.19.242 (10.33.19.242) deleted
10.33.19.244 (10.33.19.244) deleted
10.33.19.245 (10.33.19.245) deleted
10.33.19.246 (10.33.19.246) deleted
10.33.19.247 (10.33.19.247) deleted
10.33.19.248 (10.33.19.248) deleted
10.33.19.249 (10.33.19.249) deleted
10.33.19.250 (10.33.19.250) deleted
10.33.19.254 (10.33.19.254) deleted
169.254.219.102 (169.254.219.102) deleted
169.254.226.125 (169.254.226.125) deleted
delete: cannot locate 192.168.1.1
192.168.1.2 (192.168.1.2) deleted
224.0.0.251 (224.0.0.251) deleted
224.0.0.251 (224.0.0.251) deleted
239.255.255.250 (239.255.255.250) deleted
```
- prouvez que ça fonctionne en l'affichant et en constatant les changements
```bash
(base) paja@paja-mac ~ % arp -a
? (10.33.16.4) at 82:54:43:12:29:71 on en0 ifscope [ethernet]
? (10.33.16.12) at c6:20:1c:b0:d:47 on en0 ifscope [ethernet]
? (10.33.16.156) at 3e:da:c8:2a:ea:5e on en0 ifscope [ethernet]
? (10.33.16.197) at 92:52:f6:f1:55:4e on en0 ifscope [ethernet]
? (10.33.18.121) at fa:e7:6c:b:20:e3 on en0 ifscope [ethernet]
? (10.33.18.161) at 78:4f:43:87:f5:11 on en0 ifscope permanent [ethernet]
? (10.33.19.56) at d0:88:c:a1:83:34 on en0 ifscope [ethernet]
? (10.33.19.72) at 32:e3:cd:b6:d6:21 on en0 ifscope [ethernet]
? (10.33.19.249) at 1c:91:80:f3:34:39 on en0 ifscope [ethernet]
? (10.33.19.254) at 0:c0:e7:e0:4:4e on en0 ifscope [ethernet]
ipad-de-pajak-alexandre.local (169.254.219.102) at e6:e4:ab:ed:fd:b3 on en9 [ethernet]
? (192.168.1.1) at 0:e0:4c:68:2b:94 on en7 ifscope permanent [ethernet]
```
- ré-effectuez des pings, et constatez la ré-apparition des données dans la table ARP
```bash
(base) paja@paja-mac ~ % ping 192.168.1.2
PING 192.168.1.2 (192.168.1.2): 56 data bytes
64 bytes from 192.168.1.2: icmp_seq=0 ttl=64 time=1.205 ms
64 bytes from 192.168.1.2: icmp_seq=1 ttl=64 time=0.943 ms
64 bytes from 192.168.1.2: icmp_seq=2 ttl=64 time=1.211 ms
^C
--- 192.168.1.2 ping statistics ---
3 packets transmitted, 3 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 0.943/1.120/1.211/0.125 ms
(base) paja@paja-mac ~ % arp -a
? (10.33.16.4) at 82:54:43:12:29:71 on en0 ifscope [ethernet]
? (10.33.16.12) at c6:20:1c:b0:d:47 on en0 ifscope [ethernet]
? (10.33.16.43) at c8:89:f3:ac:3b:d4 on en0 ifscope [ethernet]
? (10.33.16.156) at 3e:da:c8:2a:ea:5e on en0 ifscope [ethernet]
? (10.33.16.197) at 92:52:f6:f1:55:4e on en0 ifscope [ethernet]
? (10.33.16.237) at 60:e3:2b:b6:34:1a on en0 ifscope [ethernet]
? (10.33.16.242) at 3c:a6:f6:1e:22:c6 on en0 ifscope [ethernet]
? (10.33.18.121) at fa:e7:6c:b:20:e3 on en0 ifscope [ethernet]
? (10.33.18.132) at f6:7a:23:89:cc:8e on en0 ifscope [ethernet]
? (10.33.18.161) at 78:4f:43:87:f5:11 on en0 ifscope permanent [ethernet]
? (10.33.19.56) at d0:88:c:a1:83:34 on en0 ifscope [ethernet]
? (10.33.19.70) at fa:c:ab:27:a1:1e on en0 ifscope [ethernet]
? (10.33.19.72) at 32:e3:cd:b6:d6:21 on en0 ifscope [ethernet]
? (10.33.19.126) at 56:f5:b2:ca:92:9a on en0 ifscope [ethernet]
? (10.33.19.130) at 46:b6:ce:f5:38:2 on en0 ifscope [ethernet]
? (10.33.19.152) at 18:3e:ef:d7:78:db on en0 ifscope [ethernet]
? (10.33.19.153) at 7c:3:ab:1b:d9:8a on en0 ifscope [ethernet]
? (10.33.19.249) at 1c:91:80:f3:34:39 on en0 ifscope [ethernet]
? (10.33.19.254) at 0:c0:e7:e0:4:4e on en0 ifscope [ethernet]
ipad-de-pajak-alexandre.local (169.254.219.102) at e6:e4:ab:ed:fd:b3 on en9 [ethernet]
? (192.168.1.1) at 0:e0:4c:68:2b:94 on en7 ifscope permanent [ethernet]
? (192.168.1.2) at 0:e0:4c:68:23:2b on en7 ifscope [ethernet]
```
> Les échanges ARP sont effectuées automatiquement par votre machine lorsqu'elle essaie de joindre une machine sur le même LAN qu'elle. Si la MAC du destinataire n'est pas déjà dans la table ARP, alors un échange ARP sera déclenché.

🌞 **Wireshark it**

- vous savez maintenant comment forcer un échange ARP : il sufit de vider la table ARP et tenter de contacter quelqu'un, l'échange ARP se fait automatiquement
- mettez en évidence les deux trames ARP échangées lorsque vous essayez de contacter quelqu'un pour la "première" fois
  - déterminez, pour les deux trames, les adresses source et destination
  tram de Broadcast:
      Source: RealtekS_68:2b:94 (00:e0:4c:68:2b:94)
      Destination: Broadcast (ff:ff:ff:ff:ff:ff)
  tram de Reply:
      Source: RealtekS_68:23:2b (00:e0:4c:68:23:2b)
      Destination: RealtekS_68:2b:94 (00:e0:4c:68:2b:94)

  - déterminez à quoi correspond chacune de ces adresses
      RealtekS_68:2b:94 (00:e0:4c:68:2b:94) correspond a l'adresse mac de ma machine
      RealtekS_68:23:2b (00:e0:4c:68:23:2b) correspond a l'adresse mac de la machine de Marie-Lise
      
🦈 **PCAP qui contient les trames ARP**

[PCAP-ARP 🦈 ](./PCAP/ARP.pcapng)

> L'échange ARP est constitué de deux trames : un ARP broadcast et un ARP reply.

# II.5 Interlude hackerzz

**Chose promise chose due, on va voir les bases de l'usurpation d'identité en réseau : on va parler d'*ARP poisoning*.**

> On peut aussi trouver *ARP cache poisoning* ou encore *ARP spoofing*, ça désigne la même chose.

Le principe est simple : on va "empoisonner" la table ARP de quelqu'un d'autre.  
Plus concrètement, on va essayer d'introduire des fausses informations dans la table ARP de quelqu'un d'autre.

Entre introduire des fausses infos et usurper l'identité de quelqu'un il n'y a qu'un pas hihi.

---

Le principe de l'attaque :

- on admet Alice, Bob et Eve, tous dans un LAN, chacun leur PC
- leur configuration IP est ok, tout va bien dans le meilleur des mondes
- **Eve 'lé pa jonti** *(ou juste un agent de la CIA)* : elle aimerait s'immiscer dans les conversations de Alice et Bob
  - pour ce faire, Eve va empoisonner la table ARP de Bob, pour se faire passer pour Alice
  - elle va aussi empoisonner la table ARP d'Alice, pour se faire passer pour Bob
  - ainsi, tous les messages que s'envoient Alice et Bob seront en réalité envoyés à Eve

La place de ARP dans tout ça :

- ARP est un principe de question -> réponse (broadcast -> *reply*)
- IL SE TROUVE qu'on peut envoyer des *reply* à quelqu'un qui n'a rien demandé :)
- il faut donc simplement envoyer :
  - une trame ARP reply à Alice qui dit "l'IP de Bob se trouve à la MAC de Eve" (IP B -> MAC E)
  - une trame ARP reply à Bob qui dit "l'IP de Alice se trouve à la MAC de Eve" (IP A -> MAC E)
- ha ouais, et pour être sûr que ça reste en place, il faut SPAM sa mum, genre 1 reply chacun toutes les secondes ou truc du genre
  - bah ui ! Sinon on risque que la table ARP d'Alice ou Bob se vide naturellement, et que l'échange ARP normal survienne
  - aussi, c'est un truc possible, mais pas normal dans cette utilisation là, donc des fois bon, ça chie, DONC ON SPAM

![Am I ?](./pics/arp_snif.jpg)

---

J'peux vous aider à le mettre en place, mais **vous le faites uniquement dans un cadre privé, chez vous, ou avec des VMs**

**Je vous conseille 3 machines Linux**, Alice Bob et Eve. La commande `[arping](https://sandilands.info/sgordon/arp-spoofing-on-wired-lan)` pourra vous carry : elle permet d'envoyer manuellement des trames ARP avec le contenu de votre choix.

GLHF.

# III. DHCP you too my brooo

![YOU GET AN IP](./pics/dhcp.jpg)

*DHCP* pour *Dynamic Host Configuration Protocol* est notre p'tit pote qui nous file des IP quand on arrive dans un réseau, parce que c'est chiant de le faire à la main :)

Quand on arrive dans un réseau, notre PC contacte un serveur DHCP, et récupère généralement 3 infos :

- **1.** une IP à utiliser
- **2.** l'adresse IP de la passerelle du réseau
- **3.** l'adresse d'un serveur DNS joignable depuis ce réseau

L'échange DHCP consiste en 4 trames : DORA, que je vous laisse google vous-mêmes : D

Renewing a DHCP Lease:
```bash
(base) paja@paja-mac ~ % sudo ipconfig set en0 DHCP
Password:
```

🌞 **Wireshark it**

- identifiez les 4 trames DHCP lors d'un échange DHCP
  - mettez en évidence les adresses source et destination de chaque trame
  trame Discover : Src: Apple_87:f5:11 (78:4f:43:87:f5:11), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
  trame Offer : Src: 10.33.19.254, Dst: 10.33.18.194
  trame Request : Src: Apple_87:f5:11 (78:4f:43:87:f5:11), Dst: Broadcast (ff:ff:ff:ff:ff:ff)
  trame ACK : Src: 10.33.19.254, Dst: 10.33.18.194

- identifiez dans ces 4 trames les informations **1**, **2** et **3** dont on a parlé juste au dessus
  Dans la trame Offer : 
      - Your (client) IP address: 10.33.18.194
      - Router: 10.33.19.254
      - Domain Name Server: 8.8.8.8
        Domain Name Server: 8.8.4.4
        Domain Name Server: 8.1.1.1


🦈 **PCAP qui contient l'échange DORA**

[🦈 DORA.pcapng](./PCAP/DORA.pcapng)

> **Soucis** : l'échange DHCP ne se produit qu'à la première connexion. **Pour forcer un échange DHCP**, ça dépend de votre OS. Sur **GNU/Linux**, avec `dhclient` ça se fait bien. Sur **Windows**, le plus simple reste de définir une IP statique pourrie sur la carte réseau, se déconnecter du réseau, remettre en DHCP, se reconnecter au réseau. Sur **MacOS**, je connais peu mais Internet dit qu'c'est po si compliqué, appelez moi si besoin.

# IV. Avant-goût TCP et UDP

TCP et UDP ce sont les deux protocoles qui utilisent des ports. Si on veut accéder à un service, sur un serveur, comme un site web :

- il faut pouvoir joindre en terme d'IP le correspondant
  - on teste que ça fonctionne avec un `ping` généralement
- il faut que le serveur fasse tourner un programme qu'on appelle "service" ou "serveur"
  - le service "écoute" sur un port TCP ou UDP : il attend la connexion d'un client
- le client **connaît par avance** le port TCP ou UDP sur lequel le service écoute
- en utilisant l'IP et le port, il peut se connecter au service en utilisant un moyen adapté :
  - un navigateur web pour un site web
  - un `ncat` pour se connecter à un autre `ncat`
  - et plein d'autres, **de façon générale on parle d'un client, et d'un serveur**

---

🌞 **Wireshark it**

- déterminez à quelle IP et quel port votre PC se connecte quand vous regardez une vidéo Youtube

iP serveur TCP youtube:  Src: 91.68.245.81 
Port server TCP youtube: Src Port: 443

IP Client TCP youtube: Dst: 10.33.18.198 (mon IP)
Port Client TCP youtube: Dst Port: 51531

  - il sera sûrement plus simple de repérer le trafic Youtube en fermant tous les autres onglets et toutes les autres applications utilisant du réseau

🦈 **PCAP qui contient un extrait de l'échange qui vous a permis d'identifier les infos**

[🦈Youtube_pcapng](./PCAP/youtube.pcapng)
