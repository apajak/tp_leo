# B2 Réseau 2022 - TP1

# TP1 - Mise en jambes

# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

En utilisant la ligne de commande (CLI) de votre OS :

**🌞 Affichez les infos des cartes réseau de votre PC**

```bash
(base) paja@paja-mac ~ % ifconfig en0
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	ether 38:f9:d3:a7:6b:e9
	inet 10.33.18.101 netmask 0xfffffc00 broadcast 10.33.19.255
```

```bash
(base) paja@paja-mac ~ %  ifconfig en7
en7: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	ether 00:e0:4c:68:23:2b
```
vu que j'ai un Mac, je n'ai pas d'adresse IP car je ne l'utilise pas 

**🌞 Affichez votre gateway**

```bash
(base) paja@paja-mac ~ % route get default | grep gateway
    gateway: 10.33.19.254
```
  
### En graphique (GUI : Graphical User Interface)

En utilisant l'interface graphique de votre OS :  

**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**

adresse MAC :/System Preferences/Network/WI-Fi/Advanced../Wi-Fi/
adresse IP et gateway: /System Preferences/Network/WI-Fi/Advanced../TCP-IP

### Questions

- 🌞 à quoi sert la [gateway](../../cours/lexique.md#passerelle-ou-gateway) dans le réseau d'YNOV ?

La gateway est le lien entre le réseau D'YNOV et le réseau d'Internet entre autre la LAN et la WAN.

## 2. Modifications des informations

### A. Modification d'adresse IP (part 1)  

🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

```bash
(base) paja@paja-mac ~ %  ifconfig en0
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	inet 10.33.1.251 netmask 0xfffffc00 broadcast 10.33.3.255
```

🌞 **Il est possible que vous perdiez l'accès internet.** Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération.

Je n'ai pas perdu ma connexion internet car l'IP que j'ai choisit n'était pas déjà utiliser dans la LAN.

---


# II. Exploration locale en duo

## 3. Modification d'adresse IP

🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

- vérifiez à l'aide de commandes que vos changements ont pris effet
```bash
(base) paja@paja-mac ~ % ifconfig en7
en7: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	options=6467<RXCSUM,TXCSUM,VLAN_MTU,TSO4,TSO6,CHANNEL_IO,PARTIAL_CSUM,ZEROINVERT_CSUM>
	ether 48:65:ee:1f:18:56
	inet6 fe80::c0b:b7e3:78f0:e34e%en7 prefixlen 64 secured scopeid 0x17
	inet 192.168.1.1 netmask 0xfffffffc broadcast 192.168.1.3
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect (1000baseT <full-duplex>)
	status: active
```
- utilisez `ping` pour tester la connectivité entre les deux machines
```bash
(base) paja@paja-mac ~ %  ping 192.168.1.2
PING 192.168.1.2 (192.168.1.2): 56 data bytes
64 bytes from 192.168.1.2: icmp_seq=0 ttl=64 time=1.933 ms
64 bytes from 192.168.1.2: icmp_seq=1 ttl=64 time=1.486 ms
```
- affichez et consultez votre table ARP
```bash
(base) paja@paja-mac ~ %  arp -a | grep 192.168.1.2
? (192.168.1.2) at 0:e0:4c:68:2b:94 on en7 ifscope [ethernet]
```

## 4. Utilisation d'un des deux comme gateway

- 🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu

  - encore une fois, un ping vers un DNS connu comme `1.1.1.1` ou `8.8.8.8` c'est parfait
  ```bash
  (base) paja@paja-mac ~ % ping 8.8.8.8
  PING 8.8.8.8 (8.8.8.8): 56 data bytes
  ```
- 🌞 utiliser un `traceroute` ou `tracert` pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)
PC d'Erwan:
```bash
PS C:\WINDOWS\system32> tracert 8.8.8.8

Tracing route to dns.google [8.8.8.8]
over a maximum of 30 hops:

  1     1 ms     1 ms     1 ms  PAJA-MAC [192.168.1.2]
```
Mon PC connecté a internet:
```bash
traceroute to 8.8.8.8 (8.8.8.8), 64 hops max, 52 byte packets
 1  10.33.19.254 (10.33.19.254)  8.092 ms  3.146 ms  3.122 ms
 2  137.149.196.77.rev.sfr.net (77.196.149.137)  2.941 ms  4.731 ms  4.234 ms
 3  108.97.30.212.rev.sfr.net (212.30.97.108)  8.517 ms  8.312 ms  8.590 ms
 4  222.172.136.77.rev.sfr.net (77.136.172.222)  21.382 ms  20.900 ms  21.524 ms
 5  221.172.136.77.rev.sfr.net (77.136.172.221)  20.874 ms  22.218 ms  21.204 ms
 6  186.144.6.194.rev.sfr.net (194.6.144.186)  27.384 ms  25.128 ms  24.583 ms
 7  186.144.6.194.rev.sfr.net (194.6.144.186)  23.571 ms  23.763 ms  24.716 ms
 8  72.14.194.30 (72.14.194.30)  19.977 ms  27.560 ms  21.617 ms
 9  * * *
10  108.170.234.50 (108.170.234.50)  28.594 ms
    dns.google (8.8.8.8)  22.619 ms  24.384 ms
```
## 5. Petit chat privé

On va créer un chat extrêmement simpliste à l'aide de `netcat` (abrégé `nc`). Il est souvent considéré comme un bon couteau-suisse quand il s'agit de faire des choses avec le réseau.

Sous GNU/Linux et MacOS vous l'avez sûrement déjà, sinon débrouillez-vous pour l'installer :). Les Windowsien, ça se passe [ici](https://eternallybored.org/misc/netcat/netcat-win32-1.11.zip) (from https://eternallybored.org/misc/netcat/).  

Une fois en possession de `netcat`, vous allez pouvoir l'utiliser en ligne de commande. Comme beaucoup de commandes sous GNU/Linux, Mac et Windows, on peut utiliser l'option `-h` (`h` pour `help`) pour avoir une aide sur comment utiliser la commande.  

Sur un Windows, ça donne un truc comme ça :

```schema
C:\Users\It4\Desktop\netcat-win32-1.11>nc.exe -h
[v1.11 NT www.vulnwatch.org/netcat/]
connect to somewhere:   nc [-options] hostname port[s] [ports] ...
listen for inbound:     nc -l -p port [options] [hostname] [port]
options:
        -d              detach from console, background mode

        -e prog         inbound program to exec [dangerous!!]
        -g gateway      source-routing hop point[s], up to 8
        -G num          source-routing pointer: 4, 8, 12, ...
        -h              this cruft
        -i secs         delay interval for lines sent, ports scanned
        -l              listen mode, for inbound connects
        -L              listen harder, re-listen on socket close
        -n              numeric-only IP addresses, no DNS
        -o file         hex dump of traffic
        -p port         local port number
        -r              randomize local and remote ports
        -s addr         local source address
        -t              answer TELNET negotiation
        -u              UDP mode
        -v              verbose [use twice to be more verbose]
        -w secs         timeout for connects and final net reads
        -z              zero-I/O mode [used for scanning]
port numbers can be individual or ranges: m-n [inclusive]
```

L'idée ici est la suivante :

- l'un de vous jouera le rôle d'un *serveur*
- l'autre sera le *client* qui se connecte au *serveur*

Précisément, on va dire à `netcat` d'*écouter sur un port*. Des ports, y'en a un nombre fixe (65536, on verra ça plus tard), et c'est juste le numéro de la porte à laquelle taper si on veut communiquer avec le serveur.

Si le serveur écoute à la porte 20000, alors le client doit demander une connexion en tapant à la porte numéro 20000, simple non ?  

Here we go :

- 🌞 **sur le PC *serveur*** avec par exemple l'IP 192.168.1.1
  - `nc.exe -l -p 8888`
    - "`netcat`, écoute sur le port numéro 8888 stp"
  - il se passe rien ? Normal, faut attendre qu'un client se connecte
```bash
(base) paja@paja-mac ~ % nc -l 8888
eroifjqepif
coucou
$
```
- 🌞 **sur le PC *client*** avec par exemple l'IP 192.168.1.2
  - `nc.exe 192.168.1.1 8888`
    - "`netcat`, connecte toi au port 8888 de la machine 192.168.1.1 stp"
  - une fois fait, vous pouvez taper des messages dans les deux sens
- appelez-moi quand ça marche ! :)
  - si ça marche pas, essayez d'autres options de `netcat`
```bash
PS C:\netcat-1.11> .\nc.exe 192.168.1.2 8888 
  eroifjqepif
  coucou
  $
```
---

- 🌞 pour aller un peu plus loin
  - le serveur peut préciser sur quelle IP écouter, et ne pas répondre sur les autres
  - par exemple, on écoute sur l'interface Ethernet, mais pas sur la WiFI
  - pour ce faire `nc.exe -l -p PORT_NUMBER IP_ADDRESS`
  - par exemple `nc.exe -l -p 9999 192.168.1.37`
  - on peut aussi accepter uniquement les connexions internes à la machine en écoutant sur `127.0.0.1`
```bash
PS C:\netcat-1.11> .\nc.exe -l -p 9999 192.168.1.2
  meow
    luruzg
```
```bash
(base) paja@paja-mac ~ % nc 192.168.1.1 9999
meow
```
## 6. Firewall

Toujours par 2.

Le but est de configurer votre firewall plutôt que de le désactiver

- Activez votre firewall
```bash
(base) paja@paja-mac ~ % sudo pfctl -e
No ALTQ support in kernel
ALTQ related functions disabled
pf enabled
```
- 🌞 Autoriser les `ping`
  - configurer le firewall de votre OS pour accepter le `ping`
  - aidez vous d'internet
  - on rentrera dans l'explication dans un prochain cours mais sachez que `ping` envoie un message *ICMP de type 8* (demande d'ECHO) et reçoit un message *ICMP de type 0* (réponse d'écho) en retour
- 🌞 Autoriser le traffic sur le port qu'utilise `nc`
  - on parle bien d'ouverture de **port** TCP et/ou UDP
  - on ne parle **PAS** d'autoriser le programme `nc`
  - choisissez arbitrairement un port entre 1024 et 20000
  - vous utiliserez ce port pour [communiquer avec `netcat`](#5-petit-chat-privé-) par groupe de 2 toujours
  - le firewall du *PC serveur* devra avoir un firewall activé et un `netcat` qui fonctionne

```bash
(base) paja@paja-mac ~ % sudo nano /etc/pf.conf
```

```bash
#
# Default PF configuration file.
#
# This file contains the main ruleset, which gets automatically loaded
# at startup.  PF will not be automatically enabled, however.  Instead,
# each component which utilizes PF is responsible for enabling and disabling
# PF via -E and -X as documented in pfctl(8).  That will ensure that PF
# is disabled only when the last enable reference is released.
#
# Care must be taken to ensure that the main ruleset does not get flushed,
# as the nested anchors rely on the anchor point defined here. In addition,
# to the anchors loaded by this file, some system services would dynamically
# insert anchors into the main ruleset. These anchors will be added only when
# the system service is used and would removed on termination of the service.
#
# See pf.conf(5) for syntax.
#

# 
# com.apple anchor point
# 
scrub-anchor "com.apple/*"
nat-anchor "com.apple/*"
rdr-anchor "com.apple/*"
dummynet-anchor "com.apple/*"
anchor "com.apple/*"
load anchor "com.apple" from "/etc/pf.anchors/com.apple"

set skip on lo0
# create a set skip rule for the loopback device
# block all connection (in/out)
block all

allow tcp for nc at 2000 port
pass in proto tcp to port { 2000 }
pass out proto tcp to port { 2000 }

# allow ping
pass out inet proto icmp icmp-type { echoreq }
pass in inet proto icmp icmp-type { echorep }
```

```bash
(base) paja@paja-mac ~ % sudo sudo pfctl -f /etc/pf.conf
```

```bash
(base) paja@paja-mac ~ % sudo reboot
```

```bash
(base) paja@paja-mac /etc % sudo sudo pfctl -si
Password:
No ALTQ support in kernel
ALTQ related functions disabled
Status: Enabled for 0 days 00:08:58           Debug: Urgent

State Table                          Total             Rate
  current entries                        0               
  searches                            7369           13.7/s
  inserts                                0            0.0/s
  removals                               0            0.0/s
Counters
  match                               3631            6.7/s
  bad-offset                             0            0.0/s
  fragment                               0            0.0/s
  short                                  0            0.0/s
  normalize                              0            0.0/s
  memory                                 0            0.0/s
  bad-timestamp                          0            0.0/s
  congestion                             0            0.0/s
  ip-option                              0            0.0/s
  proto-cksum                            0            0.0/s
  state-mismatch                         0            0.0/s
  state-insert                           0            0.0/s
  state-limit                            0            0.0/s
  src-limit                              0            0.0/s
  synproxy                               0            0.0/s
  dummynet                               0            0.0/s
  invalid-port                           0            0.0/s
```

```bash
(base) paja@paja-mac /etc % nc -l 2000                     
coucou
```
```bash
(base) paja@paja-mac ~ % nc 192.168.1.13 2000
coucou
```


# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

Bon ok vous savez définir des IPs à la main. Mais pour être dans le réseau YNOV, vous l'avez jamais fait.  

C'est le **serveur DHCP** d'YNOV qui vous a donné une IP.

Une fois que le serveur DHCP vous a donné une IP, vous enregistrer un fichier appelé *bail DHCP* qui contient, entre autres :

- l'IP qu'on vous a donné
- le réseau dans lequel cette IP est valable

🌞Exploration du DHCP, depuis votre PC

- afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV

```bash
(base) paja@paja-mac ~ % ipconfig getpacket en0 | grep server_identifier      
server_identifier (ip): 192.168.1.1
```
- cette adresse a une durée de vie limitée. C'est le principe du ***bail DHCP*** (ou *DHCP lease*). Trouver la date d'expiration de votre bail DHCP
- vous pouvez vous renseigner un peu sur le fonctionnement de DHCP dans les grandes lignes. On aura sûrement un cours là dessus :)

## 2. DNS

Le protocole DNS permet la résolution de noms de domaine vers des adresses IP. Ce protocole permet d'aller sur `google.com` plutôt que de devoir connaître et utiliser l'adresse IP du serveur de Google.  

Un **serveur DNS** est un serveur à qui l'on peut poser des questions (= effectuer des requêtes) sur un nom de domaine comme `google.com`, afin d'obtenir les adresses IP liées au nom de domaine.  

Si votre navigateur fonctionne "normalement" (il vous permet d'aller sur `google.com` par exemple) alors votre ordinateur connaît forcément l'adresse d'un serveur DNS. Et quand vous naviguez sur internet, il effectue toutes les requêtes DNS à votre place, de façon automatique.

- 🌞 trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```bash
(base) paja@paja-mac ~ % ipconfig getpacket en0 | grep domain_name_server
domain_name_server (ip_mult): {192.168.1.1}
```

- 🌞 utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

  - faites un *lookup* (*lookup* = "dis moi à quelle IP se trouve tel nom de domaine")
    - pour `google.com`
        ```bash
        (base) paja@paja-mac ~ % dig google.com +short
        216.58.215.46
        ```
    - pour `ynov.com`
        ```bash
        (base) paja@paja-mac ~ % dig ynov.com +short
        104.26.11.233
        104.26.10.233
        172.67.74.226
        ```
    - interpréter les résultats de ces commandes
  - déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes
  ```bash
    (base) paja@paja-mac ~ % cat /etc/resolv.conf
  #
  # macOS Notice
  #
  # This file is not consulted for DNS hostname resolution, address
  # resolution, or the DNS query routing mechanism used by most
  # processes on this system.
  #
  # To view the DNS configuration used by this system, use:
  #   scutil --dns
  #
  # SEE ALSO
  #   dns-sd(1), scutil(8)
  #
  # This file is automatically generated.
  #
  search home
  nameserver 2a01:cb19:749:d00:32b1:b5ff:fe67:74a
  nameserver 192.168.1.1
  ```
  par defaut dig questionne donc le DNS a l'IP: 192.168.1.1

  On peut en revanche utiliser un serveur DNS en particulier avec @IP_DU_DNS en argument dans la commande dig.
  Ici @8.8.8.8 pour le serveur DNS de google.
  ```bash
  (base) paja@paja-mac ~ % dig @8.8.8.8 google.com +short
  142.250.179.78
  ```
  - faites un *reverse lookup* (= "dis moi si tu connais un nom de domaine pour telle IP")
    - pour l'adresse `78.74.21.21`
        ```bash
        (base) paja@paja-mac ~ % dig -x 78.74.21.21 +short
        host-78-74-21-21.homerun.telia.com.
        ```
    - pour l'adresse `92.146.54.88`
        ```bash
        (base) paja@paja-mac ~ % dig -x 92.146.54.88 +short
        ```
    - interpréter les résultats
      la première ip est celle du site homerun.telia.com
      la seconde ip est inconnue donc ne possède pas de nom de domaine.
    - *si vous vous demandez, j'ai pris des adresses random :)*

# IV. Wireshark

Wireshark est un outil qui permet de visualiser toutes les trames qui sortent et entrent d'une carte réseau.

Il peut :

- enregistrer le trafic réseau, pour l'analyser plus tard
- afficher le trafic réseau en temps réel

**On peut TOUT voir.**

Un peu austère aux premiers abords, une manipulation très basique permet d'avoir une très bonne compréhension de ce qu'il se passe réellement.

- téléchargez l'outil [Wireshark](https://www.wireshark.org/)
- 🌞 utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence :
  - un `ping` entre vous et la passerelle

  ![pingWireShark](./pics/pingWireshark.PNG)


  - un `netcat` entre vous et votre mate, branché en RJ45
  
  N'ayant pas recu le screen qu'on avait de cette aprés midi avec Erwan et notre netcat je te met ici a la place les trams de    communication entre le serveur tcp de mon serveur OVH  et le client tcp de l'application IOS que j'ai developper cet été. 

  ip serveur ovh:
    ```bash
      paja444@SRVInfra:~/yule_alpagame/mainServer$ ip a | grep lo
  1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
      link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
      inet 127.0.0.1/8 scope host lo
      inet 54.37.153.179/32 brd 54.37.153.179 scope global dynamic ens3
      inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
    ```
  ip emulateur IOS:
    ```bash
      (base) paja@paja-mac ~ % ifconfig en0
      en0: flags=8963<UP,BROADCAST,SMART,RUNNING,PROMISC,SIMPLEX,MULTICAST> mtu 1500
      options=400<CHANNEL_IO>
      ether 78:4f:43:87:f5:11 
      inet6 fe80::c22:aee2:e49b:71b0%en0 prefixlen 64 secured scopeid 0x5 
      inet6 2a01:cb19:749:d00:102e:9704:ce07:3b9e prefixlen 64 autoconf secured 
      inet6 2a01:cb19:749:d00:f559:d6d4:a3ef:4c9a prefixlen 64 autoconf temporary 
      inet 192.168.1.13 netmask 0xffffff00 broadcast 192.168.1.255
      nd6 options=201<PERFORMNUD,DAD>
      media: autoselect
      status: active
    ```

    ```bash
      paja444@SRVInfra:~/yule_alpagame/mainServer$ python3 main.py 
        udp:AlexDefaultLobby was added.
        TCP server is listening..
        Connected to: 90.120.143.230:50776
        Thread Number: 1
        *CONNECTAS*
        Connected to YuleDatabase.db
        Record select successfully 
        the sqlite connection is closed
        SERVER: user password found
        *GET UDP REQUEST *
        UDPlist[UDP(name='AlexDefaultLobby', uuid='cefdbafe-bd26-4139-b3bd-2ec11262d91a', ip='127.0.0.1', port=20001, master_uuid='', master_username='', client_number=0, udp_client=[])]
        Connected to: 90.120.143.230:54382
        Thread Number: 2
        *CONNECTAS*
        Connected to YuleDatabase.db
        Record select successfully 
        the sqlite connection is closed
        SERVER: user password found
        *GET UDP REQUEST *
        UDPlist[UDP(name='AlexDefaultLobby', uuid='cefdbafe-bd26-4139-b3bd-2ec11262d91a', ip='127.0.0.1', port=20001, master_uuid='', master_username='', client_number=0, udp_client=[])]
        Server response: 
    ```

  ![YuleApp_Login_tcp_request_WireShark](./pics/YuleApp_Login_tcp_request_WireShark.png)
  > Du coup il va falloir que je chiffre certaines données car on voit bien qu'elle sont facilement accessible. ^^

  
  - une requête DNS. Identifiez dans la capture le serveur DNS à qui vous posez la question.

  ![pingWireShark](./pics/DNS_request_WireShark.png)

  
  - prenez moi des screens des trames en question
  - on va prendre l'habitude d'utiliser Wireshark souvent dans les cours, pour visualiser ce qu'il se passe

# Bilan

**Vu pendant le TP :**

- visualisation de vos interfaces réseau (en GUI et en CLI)
- extraction des informations IP
  - adresse IP et masque
  - calcul autour de IP : adresse de réseau, etc.
- connaissances autour de/aperçu de :
  - un outil de diagnostic simple : `ping`
  - un outil de scan réseau : `nmap`
  - un outil qui permet d'établir des connexions "simples" (on y reviendra) : `netcat`
  - un outil pour faire des requêtes DNS : `nslookup` ou `dig`
  - un outil d'analyse de trafic : `wireshark`
- manipulation simple de vos firewalls

**Conclusion :**

- Pour permettre à un ordinateur d'être connecté en réseau, il lui faut **une liaison physique** (par câble ou par *WiFi*).  
- Pour réceptionner ce lien physique, l'ordinateur a besoin d'**une carte réseau**. La carte réseau porte une [adresse MAC](../../cours/lexique.md#mac-media-access-control).  
- **Pour être membre d'un réseau particulier, une carte réseau peut porter une adresse IP.**
Si deux ordinateurs reliés physiquement possèdent une adresse IP dans le même réseau, alors ils peuvent communiquer.  
- **Un ordintateur qui possède plusieurs cartes réseau** peut réceptionner du trafic sur l'une d'entre elles, et le balancer sur l'autre, servant ainsi de "pivot". Cet ordinateur **est appelé routeur**.
- Il existe dans la plupart des réseaux, certains équipements ayant un rôle particulier :
  - un équipement appelé **[*passerelle*](../../cours/lexique.md#passerelle-ou-gateway)**. C'est un routeur, et il nous permet de sortir du réseau actuel, pour en joindre un autre, comme Internet par exemple
  - un équipement qui agit comme **serveur DNS** : il nous permet de connaître les IP derrière des noms de domaine
  - un équipement qui agit comme **serveur DHCP** : il donne automatiquement des IP aux clients qui rejoigne le réseau
  - **chez vous, c'est votre Box qui fait les trois :)**

🌞 Ce soleil est un troll. **Lisez et prenez le temps d'appréhender le texte de conclusion juste au dessus si ces notions ne vous sont pas familières.**
