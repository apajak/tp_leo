# TP4 : TCP, UDP et services réseau

Dans ce TP on va explorer un peu les protocoles TCP et UDP. On va aussi mettre en place des services qui font appel à ces protocoles.

# 0. Prérequis

➜ Pour ce TP, on va se servir de VMs Rocky Linux. 1Go RAM c'est large large. Vous pouvez redescendre la mémoire vidéo aussi.  

➜ Les firewalls de vos VMs doivent **toujours** être actifs (et donc correctement configurés).

➜ **Si vous voyez le p'tit pote 🦈 c'est qu'il y a un PCAP à produire et à mettre dans votre dépôt git de rendu.**

# I. First steps

Faites-vous un petit top 5 des applications que vous utilisez sur votre PC souvent, des applications qui utilisent le réseau : un site que vous visitez souvent, un jeu en ligne, Spotify, j'sais po moi, n'importe.

🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

- avec Wireshark, on va faire les chirurgiens réseau
- déterminez, pour chaque application :
  - IP et port du serveur auquel vous vous connectez
  - le port local que vous ouvrez pour vous connecter

  1. **Discord (vocal):**
    - IP/Port: 162.159.128.233:443 UDP
    - Port: 58812
      ```bash
        (base) paja@paja-mac ~ % lsof -nP -i udp 
        Discord    1635 paja   24u  IPv4 0x47c9a756c16dcbe9      0t0  UDP 192.168.1.13:58812->66.22.197.82:50012
        Discord    1666 paja   64u  IPv4 0x47c9a756c16dd519      0t0  UDP *:60539
      ```
      [Discord.PCAP](./PCAP/discord.pcapng)

  2. **Messenger (message text):**
    - IP/Port: 52.211.164.227:443 TCP
    - Port: 51650
      ```bash
      (base) paja@paja-mac ~ % lsof -nP -i
      Messenger 77167 paja   51u  IPv4 0x47c9a765232aa581      0t0  TCP 192.168.1.13:51650->52.211.164.227:443
      Messenger 77167 paja   54u  IPv6 0x47c9a76526e22cc9      0t0  TCP [::1]:3103 (LISTEN)
      ```
      [Messenger.PCAP](./PCAP/messenger1.pcapng)

  3. **Youtube (streaming vidéo):**
    - IP/Port: 173.194.3.9:443 TCP
    - Port: 58029
      ```bash
      (base) paja@paja-mac ~ % lsof -nP -i 
      com.apple  1606 paja   55u  IPv4 0x47c9a76526e0dad1      0t0  TCP 192.168.1.13:58029->173.194.3.9:443 (ESTABLISHED)
      ```
      [Youtube.PCAP](./PCAP/youtube1.pcapng)

  4. **install Adobe Illustrator**
    - IP/Port: 34.250.67.152:443 TCP
    - Port: 58693
      ```bash
      adobe_lic 94666 paja    6u  IPv4 0x47c9a765232b1029      0t0  TCP 192.168.1.13:58693->34.250.67.152:443 (ESTABLISHED)
      ```
      [Illustrator.pcapng](./PCAP/install_illustrator.pcapng)

  5. **Maps (chargement carte):**
    - IP/Port: 2.18.245.42:443 TCP
    - Port: 64253
      ```bash
      (base) paja@paja-mac ~ % lsof -nP -i TCP
      Maps      17104 paja   53u  IPv4 0x6834f49554b0749d      0t0  TCP 10.33.18.221:64253->2.18.245.42:443 (ESTABLISHED)
      Maps      17104 paja   71u  IPv4 0x6834f49554b0149d      0t0  TCP 10.33.18.221:64254->2.18.245.42:443 (ESTABLISHED)
      ```
      [Maps.PCAP](./PCAP/Maps.pcapng)

> Dès qu'on se connecte à un serveur, notre PC ouvre un port random. Une fois la connexion TCP ou UDP établie, entre le port de notre PC et le port du serveur qui est en écoute, on parle de tunnel TCP ou de tunnel UDP.


> Aussi, TCP ou UDP ? Comment le client sait ? Il sait parce que le serveur a décidé ce qui était le mieux pour tel ou tel type de trafic (un jeu, une page web, etc.) et que le logiciel client est codé pour utiliser TCP ou UDP en conséquence.

🌞 **Demandez l'avis à votre OS**

- votre OS est responsable de l'ouverture des ports, et de placer un programme en "écoute" sur un port
- il est aussi responsable de l'ouverture d'un port quand une application demande à se connecter à distance vers un serveur
- bref il voit tout quoi
- utilisez la commande adaptée à votre OS pour repérer, dans la liste de toutes les connexions réseau établies, la connexion que vous voyez dans Wireshark, pour chacune des 5 applications

**Il faudra ajouter des options adaptées aux commandes pour y voir clair. Pour rappel, vous cherchez des connexions TCP ou UDP.**


🦈🦈🦈🦈🦈 **Bah ouais, captures Wireshark à l'appui évidemment.** Une capture pour chaque application, qui met bien en évidence le trafic en question.

# II. Mise en place

Allumez une VM Linux pour la suite.

## 1. SSH

Connectez-vous en SSH à votre VM.

🌞 **Examinez le trafic dans Wireshark**

- donnez un sens aux infos devant vos yeux, capturez un peu de trafic, et coupez la capture, sélectionnez une trame random et regardez dedans, vous laissez pas brainfuck par Wireshark n_n
- **déterminez si SSH utilise TCP ou UDP**
  - pareil réfléchissez-y deux minutes, logique qu'on utilise pas UDP non ?
- **repérez le *3-Way Handshake* à l'établissement de la connexion**
  - c'est le `SYN` `SYNACK` `ACK`
- **repérez le FIN FINACK à la fin d'une connexion**
- entre le *3-way handshake* et l'échange `FIN`, c'est juste une bouillie de caca chiffré, dans un tunnel TCP

🌞 **Demandez aux OS**

- repérez, avec un commande adaptée, la connexion SSH depuis votre machine
- ET repérez la connexion SSH depuis votre VM

```bash
(base) paja@paja-mac ~ % lsof -nP -i TCP
ssh       95266 paja    3u  IPv4 0x47c9a76526de0581      0t0  TCP 10.3.1.1:58999->10.3.1.11:22 (ESTABLISHED)
```

```bash
[paja444@tp4 ~]$ sudo ss -tunlp
[sudo] password for paja444: 
Netid   State    Recv-Q   Send-Q     Local Address:Port       Peer Address:Port   Process                             
udp     UNCONN   0        0              127.0.0.1:323             0.0.0.0:*       users:(("chronyd",pid=695,fd=5))   
udp     UNCONN   0        0                  [::1]:323                [::]:*       users:(("chronyd",pid=695,fd=6))   
tcp     LISTEN   0        128              0.0.0.0:22              0.0.0.0:*       users:(("sshd",pid=715,fd=3))      
tcp     LISTEN   0        128                 [::]:22                 [::]:*       users:(("sshd",pid=715,fd=4)) 
```
[ssh.pcapng](./PCAP/ssh2.pcap)

🦈 **Je veux une capture clean avec le 3-way handshake, un peu de trafic au milieu et une fin de connexion**

## 2. NFS

Allumez une deuxième VM Linux pour cette partie.

Vous allez installer un serveur NFS. Un serveur NFS c'est juste un programme qui écoute sur un port (comme toujours en fait, oèoèoè) et qui propose aux clients d'accéder à des dossiers à travers le réseau.

Une de vos VMs portera donc le serveur NFS, et l'autre utilisera un dossier à travers le réseau.

🌞 **Mettez en place un petit serveur NFS sur l'une des deux VMs**

- j'vais pas ré-écrire la roue, google it, ou [go ici](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=1)
- partagez un dossier que vous avez créé au préalable dans `/srv`
- vérifiez que vous accédez à ce dossier avec l'autre machine : [le client NFS](https://www.server-world.info/en/note?os=Rocky_Linux_8&p=nfs&f=2)

```bash
[paja444@tp4 ~]$ sudo dnf install nfs-utils
[sudo] password for paja444: 
Rocky Linux 9 - BaseOS                                                                5.5 kB/s | 3.6 kB     00:00    
Rocky Linux 9 - BaseOS                                                                1.4 MB/s | 1.7 MB     00:01    
Rocky Linux 9 - AppStream                                                             8.1 kB/s | 3.6 kB     00:00    
Rocky Linux 9 - AppStream                                                             4.6 MB/s | 6.0 MB     00:01    
Rocky Linux 9 - Extras                                                                2.5 kB/s | 2.9 kB     00:01    
Dependencies resolved.
======================================================================================================================
 Package                        Architecture           Version                           Repository              Size
======================================================================================================================
Installing:
 nfs-utils                      x86_64                 1:2.5.4-10.el9                    baseos                 422 k
Installing dependencies:
 gssproxy                       x86_64                 0.8.4-4.el9                       baseos                 108 k
 keyutils                       x86_64                 1.6.1-4.el9                       baseos                  62 k
 libev                          x86_64                 4.33-5.el9                        baseos                  52 k
 libnfsidmap                    x86_64                 1:2.5.4-10.el9                    baseos                  61 k
 libtirpc                       x86_64                 1.3.2-1.el9                       baseos                  93 k
 libverto-libev                 x86_64                 0.3.2-3.el9                       baseos                  13 k
 python3-pyyaml                 x86_64                 5.4.1-6.el9                       baseos                 191 k
 quota                          x86_64                 1:4.06-6.el9                      baseos                 190 k
 quota-nls                      noarch                 1:4.06-6.el9                      baseos                  78 k
 rpcbind                        x86_64                 1.2.6-2.el9                       baseos                  56 k
 sssd-nfs-idmap                 x86_64                 2.6.2-4.el9_0.1                   baseos                  35 k

Transaction Summary
======================================================================================================================
Install  12 Packages

Total download size: 1.3 M
Installed size: 3.9 M
Is this ok [y/N]: y
Downloading Packages:
(1/12): libverto-libev-0.3.2-3.el9.x86_64.rpm                                         105 kB/s |  13 kB     00:00    
(2/12): quota-nls-4.06-6.el9.noarch.rpm                                               262 kB/s |  78 kB     00:00    
(3/12): libtirpc-1.3.2-1.el9.x86_64.rpm                                               478 kB/s |  93 kB     00:00    
(4/12): quota-4.06-6.el9.x86_64.rpm                                                   523 kB/s | 190 kB     00:00    
(5/12): rpcbind-1.2.6-2.el9.x86_64.rpm                                                706 kB/s |  56 kB     00:00    
(6/12): libev-4.33-5.el9.x86_64.rpm                                                   1.0 MB/s |  52 kB     00:00    
(7/12): sssd-nfs-idmap-2.6.2-4.el9_0.1.x86_64.rpm                                     898 kB/s |  35 kB     00:00    
(8/12): python3-pyyaml-5.4.1-6.el9.x86_64.rpm                                         1.1 MB/s | 191 kB     00:00    
(9/12): gssproxy-0.8.4-4.el9.x86_64.rpm                                               1.9 MB/s | 108 kB     00:00    
(10/12): libnfsidmap-2.5.4-10.el9.x86_64.rpm                                          1.7 MB/s |  61 kB     00:00    
(11/12): keyutils-1.6.1-4.el9.x86_64.rpm                                              1.6 MB/s |  62 kB     00:00    
(12/12): nfs-utils-2.5.4-10.el9.x86_64.rpm                                            2.8 MB/s | 422 kB     00:00    
----------------------------------------------------------------------------------------------------------------------
Total                                                                                 1.4 MB/s | 1.3 MB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                              1/1 
  Installing       : libtirpc-1.3.2-1.el9.x86_64                                                                 1/12 
  Installing       : libnfsidmap-1:2.5.4-10.el9.x86_64                                                           2/12 
  Running scriptlet: rpcbind-1.2.6-2.el9.x86_64                                                                  3/12 
  Installing       : rpcbind-1.2.6-2.el9.x86_64                                                                  3/12 
  Running scriptlet: rpcbind-1.2.6-2.el9.x86_64                                                                  3/12 
Created symlink /etc/systemd/system/multi-user.target.wants/rpcbind.service → /usr/lib/systemd/system/rpcbind.service.
Created symlink /etc/systemd/system/sockets.target.wants/rpcbind.socket → /usr/lib/systemd/system/rpcbind.socket.

  Installing       : keyutils-1.6.1-4.el9.x86_64                                                                 4/12 
  Installing       : libev-4.33-5.el9.x86_64                                                                     5/12 
  Installing       : libverto-libev-0.3.2-3.el9.x86_64                                                           6/12 
  Installing       : gssproxy-0.8.4-4.el9.x86_64                                                                 7/12 
  Running scriptlet: gssproxy-0.8.4-4.el9.x86_64                                                                 7/12 
  Installing       : python3-pyyaml-5.4.1-6.el9.x86_64                                                           8/12 
  Installing       : quota-nls-1:4.06-6.el9.noarch                                                               9/12 
  Installing       : quota-1:4.06-6.el9.x86_64                                                                  10/12 
  Running scriptlet: nfs-utils-1:2.5.4-10.el9.x86_64                                                            11/12 
  Installing       : nfs-utils-1:2.5.4-10.el9.x86_64                                                            11/12 
  Running scriptlet: nfs-utils-1:2.5.4-10.el9.x86_64                                                            11/12 
  Installing       : sssd-nfs-idmap-2.6.2-4.el9_0.1.x86_64                                                      12/12 
  Running scriptlet: sssd-nfs-idmap-2.6.2-4.el9_0.1.x86_64                                                      12/12 
  Verifying        : libverto-libev-0.3.2-3.el9.x86_64                                                           1/12 
  Verifying        : quota-1:4.06-6.el9.x86_64                                                                   2/12 
  Verifying        : quota-nls-1:4.06-6.el9.noarch                                                               3/12 
  Verifying        : libtirpc-1.3.2-1.el9.x86_64                                                                 4/12 
  Verifying        : python3-pyyaml-5.4.1-6.el9.x86_64                                                           5/12 
  Verifying        : rpcbind-1.2.6-2.el9.x86_64                                                                  6/12 
  Verifying        : libev-4.33-5.el9.x86_64                                                                     7/12 
  Verifying        : sssd-nfs-idmap-2.6.2-4.el9_0.1.x86_64                                                       8/12 
  Verifying        : gssproxy-0.8.4-4.el9.x86_64                                                                 9/12 
  Verifying        : nfs-utils-1:2.5.4-10.el9.x86_64                                                            10/12 
  Verifying        : libnfsidmap-1:2.5.4-10.el9.x86_64                                                          11/12 
  Verifying        : keyutils-1.6.1-4.el9.x86_64                                                                12/12 

Installed:
  gssproxy-0.8.4-4.el9.x86_64          keyutils-1.6.1-4.el9.x86_64          libev-4.33-5.el9.x86_64                 
  libnfsidmap-1:2.5.4-10.el9.x86_64    libtirpc-1.3.2-1.el9.x86_64          libverto-libev-0.3.2-3.el9.x86_64       
  nfs-utils-1:2.5.4-10.el9.x86_64      python3-pyyaml-5.4.1-6.el9.x86_64    quota-1:4.06-6.el9.x86_64               
  quota-nls-1:4.06-6.el9.noarch        rpcbind-1.2.6-2.el9.x86_64           sssd-nfs-idmap-2.6.2-4.el9_0.1.x86_64   

Complete!
```
```bash
[paja444@tp4 ~]$ sudo nano /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = srv.NFStp4
```
```bash
[paja444@tp4 home]$ sudo mkdir nfsshare
```
```bash
[paja444@tp4 ~]$ sudo nano /etc/exports
/home/nfsshare 10.3.1.1/24(rw,no_root_squash)
```
```bash
systemctl enable --now rpcbind nfs-server 
```
```bash
[paja444@tp4 ~]$ sudo firewall-cmd --add-service=nfs 
success
```



```bash
[paja444@tp4-2 ~]$ sudo dnf install nfs-utils -y
Last metadata expiration check: 0:00:12 ago on Mon 17 Oct 2022 11:52:50 AM CEST.
Dependencies resolved.
============================================================================================================================
 Package                          Architecture             Version                           Repository                Size
============================================================================================================================
Installing:
 nfs-utils                        x86_64                   1:2.5.4-10.el9                    baseos                   422 k
Installing dependencies:
 gssproxy                         x86_64                   0.8.4-4.el9                       baseos                   108 k
 keyutils                         x86_64                   1.6.1-4.el9                       baseos                    62 k
 libev                            x86_64                   4.33-5.el9                        baseos                    52 k
 libnfsidmap                      x86_64                   1:2.5.4-10.el9                    baseos                    61 k
 libtirpc                         x86_64                   1.3.2-1.el9                       baseos                    93 k
 libverto-libev                   x86_64                   0.3.2-3.el9                       baseos                    13 k
 python3-pyyaml                   x86_64                   5.4.1-6.el9                       baseos                   191 k
 quota                            x86_64                   1:4.06-6.el9                      baseos                   190 k
 quota-nls                        noarch                   1:4.06-6.el9                      baseos                    78 k
 rpcbind                          x86_64                   1.2.6-2.el9                       baseos                    56 k
 sssd-nfs-idmap                   x86_64                   2.6.2-4.el9_0.1                   baseos                    35 k

Transaction Summary
============================================================================================================================
Install  12 Packages

Total download size: 1.3 M
Installed size: 3.9 M
Downloading Packages:
(1/12): libverto-libev-0.3.2-3.el9.x86_64.rpm                                                85 kB/s |  13 kB     00:00    
(2/12): quota-nls-4.06-6.el9.noarch.rpm                                                     301 kB/s |  78 kB     00:00    
(3/12): libtirpc-1.3.2-1.el9.x86_64.rpm                                                     415 kB/s |  93 kB     00:00    
(4/12): quota-4.06-6.el9.x86_64.rpm                                                         497 kB/s | 190 kB     00:00    
(5/12): python3-pyyaml-5.4.1-6.el9.x86_64.rpm                                               1.2 MB/s | 191 kB     00:00    
(6/12): libev-4.33-5.el9.x86_64.rpm                                                         807 kB/s |  52 kB     00:00    
(7/12): sssd-nfs-idmap-2.6.2-4.el9_0.1.x86_64.rpm                                           922 kB/s |  35 kB     00:00    
(8/12): rpcbind-1.2.6-2.el9.x86_64.rpm                                                      658 kB/s |  56 kB     00:00    
(9/12): libnfsidmap-2.5.4-10.el9.x86_64.rpm                                                 1.0 MB/s |  61 kB     00:00    
(10/12): gssproxy-0.8.4-4.el9.x86_64.rpm                                                    1.2 MB/s | 108 kB     00:00    
(11/12): keyutils-1.6.1-4.el9.x86_64.rpm                                                    1.1 MB/s |  62 kB     00:00    
(12/12): nfs-utils-2.5.4-10.el9.x86_64.rpm                                                  2.8 MB/s | 422 kB     00:00    
----------------------------------------------------------------------------------------------------------------------------
Total                                                                                       1.5 MB/s | 1.3 MB     00:00     
Running transaction check
Transaction check succeeded.
Running transaction test
Transaction test succeeded.
Running transaction
  Preparing        :                                                                                                    1/1 
  Installing       : libtirpc-1.3.2-1.el9.x86_64                                                                       1/12 
  Installing       : libnfsidmap-1:2.5.4-10.el9.x86_64                                                                 2/12 
  Running scriptlet: rpcbind-1.2.6-2.el9.x86_64                                                                        3/12 
  Installing       : rpcbind-1.2.6-2.el9.x86_64                                                                        3/12 
  Running scriptlet: rpcbind-1.2.6-2.el9.x86_64                                                                        3/12 
Created symlink /etc/systemd/system/multi-user.target.wants/rpcbind.service → /usr/lib/systemd/system/rpcbind.service.
Created symlink /etc/systemd/system/sockets.target.wants/rpcbind.socket → /usr/lib/systemd/system/rpcbind.socket.

  Installing       : keyutils-1.6.1-4.el9.x86_64                                                                       4/12 
  Installing       : libev-4.33-5.el9.x86_64                                                                           5/12 
  Installing       : libverto-libev-0.3.2-3.el9.x86_64                                                                 6/12 
  Installing       : gssproxy-0.8.4-4.el9.x86_64                                                                       7/12 
  Running scriptlet: gssproxy-0.8.4-4.el9.x86_64                                                                       7/12 
  Installing       : python3-pyyaml-5.4.1-6.el9.x86_64                                                                 8/12 
  Installing       : quota-nls-1:4.06-6.el9.noarch                                                                     9/12 
  Installing       : quota-1:4.06-6.el9.x86_64                                                                        10/12 
  Running scriptlet: nfs-utils-1:2.5.4-10.el9.x86_64                                                                  11/12 
  Installing       : nfs-utils-1:2.5.4-10.el9.x86_64                                                                  11/12 
  Running scriptlet: nfs-utils-1:2.5.4-10.el9.x86_64                                                                  11/12 
  Installing       : sssd-nfs-idmap-2.6.2-4.el9_0.1.x86_64                                                            12/12 
  Running scriptlet: sssd-nfs-idmap-2.6.2-4.el9_0.1.x86_64                                                            12/12 
  Verifying        : libverto-libev-0.3.2-3.el9.x86_64                                                                 1/12 
  Verifying        : quota-1:4.06-6.el9.x86_64                                                                         2/12 
  Verifying        : quota-nls-1:4.06-6.el9.noarch                                                                     3/12 
  Verifying        : libtirpc-1.3.2-1.el9.x86_64                                                                       4/12 
  Verifying        : python3-pyyaml-5.4.1-6.el9.x86_64                                                                 5/12 
  Verifying        : rpcbind-1.2.6-2.el9.x86_64                                                                        6/12 
  Verifying        : libev-4.33-5.el9.x86_64                                                                           7/12 
  Verifying        : sssd-nfs-idmap-2.6.2-4.el9_0.1.x86_64                                                             8/12 
  Verifying        : gssproxy-0.8.4-4.el9.x86_64                                                                       9/12 
  Verifying        : nfs-utils-1:2.5.4-10.el9.x86_64                                                                  10/12 
  Verifying        : libnfsidmap-1:2.5.4-10.el9.x86_64                                                                11/12 
  Verifying        : keyutils-1.6.1-4.el9.x86_64                                                                      12/12 

Installed:
  gssproxy-0.8.4-4.el9.x86_64            keyutils-1.6.1-4.el9.x86_64            libev-4.33-5.el9.x86_64                   
  libnfsidmap-1:2.5.4-10.el9.x86_64      libtirpc-1.3.2-1.el9.x86_64            libverto-libev-0.3.2-3.el9.x86_64         
  nfs-utils-1:2.5.4-10.el9.x86_64        python3-pyyaml-5.4.1-6.el9.x86_64      quota-1:4.06-6.el9.x86_64                 
  quota-nls-1:4.06-6.el9.noarch          rpcbind-1.2.6-2.el9.x86_64             sssd-nfs-idmap-2.6.2-4.el9_0.1.x86_64     

Complete!
```
```bash
[paja444@tp4-2 ~]$ sudo nano /etc/idmapd.conf
[General]
#Verbosity = 0
# The following should be set to the local NFSv4 domain name
# The default is the host's DNS domain name.
Domain = srv.NFStp4
```
```bash
[paja444@tp4-2 ~]$ sudo mount -t nfs 10.3.1.11:/home/nfsshare /mnt
[paja444@tp4-2 ~]$ df -h
Filesystem                Size  Used Avail Use% Mounted on
devtmpfs                  462M     0  462M   0% /dev
tmpfs                     481M     0  481M   0% /dev/shm
tmpfs                     193M  3.0M  190M   2% /run
/dev/mapper/rl-root       6.2G  1.2G  5.1G  18% /
/dev/sda1                1014M  194M  821M  20% /boot
tmpfs                      97M     0   97M   0% /run/user/1000
10.3.1.11:/home/nfsshare  6.2G  1.2G  5.1G  18% /mnt
```

```bash
[paja444@tp4-2 mnt]$ sudo nano /etc/fstab
#
# /etc/fstab
# Created by anaconda on Fri Sep 30 13:39:32 2022
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=11474274-a6df-4e4d-8f56-4f6bc3956d33 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
10.3.1.11:/home/nfsshare /mnt               nfs     defaults        0 0
```


> Si besoin, comme d'hab, je peux aider à la compréhension, n'hésitez pas à m'appeler.

🌞 **Wireshark it !**

- une fois que c'est en place, utilisez `tcpdump` pour capturer du trafic NFS
- déterminez le port utilisé par le serveur

🌞 **Demandez aux OS**

- repérez, avec un commande adaptée, la connexion NFS sur le client et sur le serveur

```bash
[paja444@vm1 ~]$ netstat -an | grep 10.3.1.11:2049
tcp        0      0 10.3.1.11:2049          10.3.1.12:1019          ESTABLISHED 
```
le port utiliser par defaut du serveur est `2049`.
celui du client est ici `1019`.
[nfs.pcapng](./PCAP/nfs.pcap)

🦈 **Et vous me remettez une capture de trafic NFS** la plus complète possible. J'ai pas dit que je voulais le plus de trames possible, mais juste, ce qu'il faut pour avoir un max d'infos sur le trafic

## 3. DNS

🌞 Utilisez une commande pour effectuer une requête DNS depuis une des VMs
```bash
[paja444@vm1 ~]$ host -t a gitlab.com
gitlab.com has address 172.65.251.78
[paja444@vm1 ~]$ dig gitlab.com

; <<>> DiG 9.16.23-RH <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 25531
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;gitlab.com.			IN	A

;; ANSWER SECTION:
gitlab.com.		83	IN	A	172.65.251.78

;; Query time: 23 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Fri Oct 21 10:39:27 CEST 2022
;; MSG SIZE  rcvd: 55
```
[dnsRequest.pcap](./PCAP/dns.pcap)

l'ip:port du serveur dns contacter est `1.1.1.1:53`.
- capturez le trafic avec un `tcpdump`
- déterminez le port et l'IP du serveur DNS auquel vous vous connectez
