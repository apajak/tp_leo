# TP1 : (re)Familiaration avec un système GNU/Linux

Dans ce TP, on va passer en revue des éléments de configurations élémentaires du système.

Vous pouvez effectuer ces actions dans la première VM. On la clonera ensuite avec toutes les configurations pré-effectuées.

Au menu :

- gestion d'utilisateurs
  - sudo
  - SSH et clés
- configuration réseau
- gestion de partitions
- gestion de services

## Sommaire

- [TP1 : (re)Familiaration avec un système GNU/Linux](#tp1--refamiliaration-avec-un-système-gnulinux)
  - [Sommaire](#sommaire)
  - [0. Préparation de la machine](#0-préparation-de-la-machine)
  - [I. Utilisateurs](#i-utilisateurs)
    - [1. Création et configuration](#1-création-et-configuration)
    - [2. SSH](#2-ssh)
  - [II. Partitionnement](#ii-partitionnement)
    - [1. Préparation de la VM](#1-préparation-de-la-vm)
    - [2. Partitionnement](#2-partitionnement)
  - [III. Gestion de services](#iii-gestion-de-services)
  - [1. Interaction avec un service existant](#1-interaction-avec-un-service-existant)
  - [2. Création de service](#2-création-de-service)
    - [A. Unité simpliste](#a-unité-simpliste)
    - [B. Modification de l'unité](#b-modification-de-lunité)

## 0. Préparation de la machine

> **POUR RAPPEL** pour chacune des opérations, vous devez fournir dans le compte-rendu : comment réaliser l'opération ET la preuve que l'opération a été bien réalisée

🌞 **Setup de deux machines Rocky Linux configurées de façon basique.**

- **un accès internet (via la carte NAT)**
  - carte réseau dédiée
  - route par défaut
      ```bash
        [paja444@node1 ~]$ ip a
        1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
            link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
            inet 127.0.0.1/8 scope host lo
            valid_lft forever preferred_lft forever
            inet6 ::1/128 scope host 
            valid_lft forever preferred_lft forever
        2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
            link/ether 08:00:27:13:8e:5c brd ff:ff:ff:ff:ff:ff
            inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
            valid_lft 86216sec preferred_lft 86216sec
            inet6 fe80::a00:27ff:fe13:8e5c/64 scope link noprefixroute 
            valid_lft forever preferred_lft forever
        3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
            link/ether 08:00:27:a7:36:66 brd ff:ff:ff:ff:ff:ff
            inet 10.101.1.11/24 brd 10.101.1.255 scope global noprefixroute enp0s8
            valid_lft forever preferred_lft forever
            inet6 fe80::a00:27ff:fea7:3666/64 scope link 
            valid_lft forever preferred_lft forever
        [paja444@node1 ~]$ ping google.com
        PING google.com (142.250.74.238) 56(84) bytes of data.
        64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=63 time=21.3 ms
        64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=63 time=22.2 ms
        64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=3 ttl=63 time=673 ms
        ^C
        --- google.com ping statistics ---
        4 packets transmitted, 3 received, 25% packet loss, time 3003ms
        rtt min/avg/max/mdev = 21.294/238.876/673.180/307.099 ms
      ```
        ```bash
        [paja444@node2 ~]$ ip a
        1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
            link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
            inet 127.0.0.1/8 scope host lo
            valid_lft forever preferred_lft forever
            inet6 ::1/128 scope host 
            valid_lft forever preferred_lft forever
        2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
            link/ether 08:00:27:d3:45:ef brd ff:ff:ff:ff:ff:ff
            inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic noprefixroute enp0s3
            valid_lft 86144sec preferred_lft 86144sec
            inet6 fe80::a00:27ff:fed3:45ef/64 scope link noprefixroute 
            valid_lft forever preferred_lft forever
        3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
            link/ether 08:00:27:8c:de:a6 brd ff:ff:ff:ff:ff:ff
            inet 10.101.1.12/24 brd 10.101.1.255 scope global noprefixroute enp0s8
            valid_lft forever preferred_lft forever
            inet6 fe80::a00:27ff:fe8c:dea6/64 scope link 
            valid_lft forever preferred_lft forever
        [paja444@node2 ~]$ ping google.com
        PING google.com (142.250.74.238) 56(84) bytes of data.
        64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=63 time=23.0 ms
        64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=63 time=23.8 ms
        ^C
        --- google.com ping statistics ---
        2 packets transmitted, 2 received, 0% packet loss, time 1001ms
        rtt min/avg/max/mdev = 22.990/23.372/23.755/0.382 ms
        ```

- **un accès à un réseau local** (les deux machines peuvent se `ping`) (via la carte Host-Only)
  - carte réseau dédiée (host-only sur VirtualBox)
  - les machines doivent posséder une IP statique sur l'interface host-only
      ```bash
        [paja444@node1 ~]$ ping 10.101.1.12
        PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
        64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.916 ms
        64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.415 ms
        ^C
        --- 10.101.1.12 ping statistics ---
        2 packets transmitted, 2 received, 0% packet loss, time 1002ms
        rtt min/avg/max/mdev = 0.415/0.665/0.916/0.250 ms
      ```
      ```bash
        [paja444@node2 ~]$ ping 10.101.1.12
        PING 10.101.1.12 (10.101.1.12) 56(84) bytes of data.
        64 bytes from 10.101.1.12: icmp_seq=1 ttl=64 time=0.236 ms
        64 bytes from 10.101.1.12: icmp_seq=2 ttl=64 time=0.051 ms
        ^C
        --- 10.101.1.12 ping statistics ---
        2 packets transmitted, 2 received, 0% packet loss, time 1025ms
        rtt min/avg/max/mdev = 0.051/0.143/0.236/0.092 ms
      ```

- **vous n'utilisez QUE `ssh` pour administrer les machines**

- **les machines doivent avoir un nom**
  - référez-vous au mémo
  - les noms que doivent posséder vos machines sont précisés dans le tableau plus bas

      ```bash
        [paja444@localhost ~]$ sudo hostname node1.tp1.b2
        [paja444@localhost ~]$ echo 'node1.tp1.b2' | sudo tee /etc/hostname
        [sudo] password for paja444: 
        node1.tp1.b2
      ```
      ```bash
        [paja444@localhost ~]$ sudo hostname node2.tp1.b2
        [paja444@localhost ~]$ echo 'node2.tp1.b2' | sudo tee /etc/hostname
        [sudo] password for paja444: 
        node2.tp1.b2
      ```


- **utiliser `1.1.1.1` comme serveur DNS**
  - référez-vous au mémo

        ```bash
        [paja444@node1 ~]$ sudo nano /etc/resolv.conf 
        [paja444@node1 ~]$ sudo cat /etc/resolv.conf
        # Generated by NetworkManager
        search tp1.b2
        nameserver 1.1.1.1
        ```
        ```bash
        [paja444@node2 ~]$ sudo nano /etc/resolv.conf 
        [sudo] password for paja444: 
        [paja444@node2 ~]$ sudo cat /etc/resolv.conf 
        # Generated by NetworkManager
        search tp1.b2
        nameserver 1.1.1.1
        ```
  - vérifier avec le bon fonctionnement avec la commande `dig`
    - avec `dig`, demander une résolution du nom `ynov.com`
    - mettre en évidence la ligne qui contient la réponse : l'IP qui correspond au nom demandé

        ```bash
        [paja444@node1 ~]$ dig ynov.com +short
        104.26.11.233
        172.67.74.226
        104.26.10.233
        ```
        ```bash
        [paja444@node2 ~]$ dig ynov.com +short
        104.26.11.233
        104.26.10.233
        172.67.74.226
        ```
    - mettre en évidence la ligne qui contient l'adresse IP du serveur qui vous a répondu

        ```bash
        [paja444@node1 ~]$ dig ynov.com | grep SERVER:
        ;; SERVER: 1.1.1.1#53(1.1.1.1)
        ```
        ```bash
        [paja444@node2 ~]$ dig ynov.com | grep SERVER:
        ;; SERVER: 1.1.1.1#53(1.1.1.1)
        ```


- **les machines doivent pouvoir se joindre par leurs noms respectifs**
  - fichier `/etc/hosts`
  - assurez-vous du bon fonctionnement avec des `ping <NOM>`

        ```bash
        [paja444@node1 ~]$ echo '10.101.1.12 node2.tp1.b2' | sudo tee /etc/hosts
        10.101.1.12 node2.tp1.b2
        [paja444@node1 ~]$ cat /etc/hosts
        10.101.1.12 node2.tp1.b2
        [paja444@node1 ~]$ ping node2.tp1.b2
        PING node2.tp1.b2 (10.101.1.12) 56(84) bytes of data.
        64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=1 ttl=64 time=0.444 ms
        64 bytes from node2.tp1.b2 (10.101.1.12): icmp_seq=2 ttl=64 time=0.777 ms
        ^C
        --- node2.tp1.b2 ping statistics ---
        2 packets transmitted, 2 received, 0% packet loss, time 1001ms
        rtt min/avg/max/mdev = 0.444/0.610/0.777/0.166 ms
        ```
        ```bash
        [paja444@node2 ~]$ echo '10.101.1.11 node1.tp1.b2' | sudo tee /etc/hosts
        [sudo] password for paja444: 
        10.101.1.11 node1.tp1.b2
        [paja444@node2 ~]$ ping node1.tp1.b2
        PING node1.tp1.b2 (10.101.1.11) 56(84) bytes of data.
        64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=1 ttl=64 time=0.453 ms
        64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=2 ttl=64 time=0.502 ms
        64 bytes from node1.tp1.b2 (10.101.1.11): icmp_seq=3 ttl=64 time=0.473 ms
        ^C
        --- node1.tp1.b2 ping statistics ---
        3 packets transmitted, 3 received, 0% packet loss, time 2044ms
        rtt min/avg/max/mdev = 0.453/0.476/0.502/0.020 ms
        ```

- **le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**
  - commande `firewall-cmd`

        ```bash
        [paja444@node1 ~]$ sudo firewall-cmd --remove-service cockpit --permanent
        success
        [paja444@node1 ~]$ sudo firewall-cmd --remove-service dhcpv6-client --permanent
        success
        [paja444@node1 ~]$ sudo firewall-cmd --list-all
        public (active)
        target: default
        icmp-block-inversion: no
        interfaces: enp0s3 enp0s8
        sources: 
        services: dhcpv6-client ssh
        ports: 
        protocols: 
        forward: yes
        masquerade: no
        forward-ports: 
        source-ports: 
        icmp-blocks: 
        rich rules: 
        ```
        ```bash
        [paja444@node2 ~]$ sudo firewall-cmd --remove-service cockpit --permanent
        [sudo] password for paja444: 
        success
        [paja444@node2 ~]$ sudo firewall-cmd --remove-service dhcpv6-client --permanent
        success
        [paja444@node2 ~]$ sudo firewall-cmd --list-all
        public (active)
        target: default
        icmp-block-inversion: no
        interfaces: enp0s3 enp0s8
        sources: 
        services: cockpit dhcpv6-client ssh
        ports: 
        protocols: 
        forward: yes
        masquerade: no
        forward-ports: 
        source-ports: 
        icmp-blocks: 
        rich rules: 
        ```


Pour le réseau des différentes machines (ce sont les IP qui doivent figurer sur les interfaces host-only):

| Name               | IP            |
|--------------------|---------------|
| 🖥️ `node1.tp1.b2` | `10.101.1.11` |
| 🖥️ `node2.tp1.b2` | `10.101.1.12` |
| Votre hôte         | `10.101.1.1`  |

## I. Utilisateurs

[Une section dédiée aux utilisateurs est dispo dans le mémo Linux.](../../cours/memos/commandes.md#gestion-dutilisateurs).

### 1. Création et configuration

🌞 **Ajouter un utilisateur à la machine**, qui sera dédié à son administration

- précisez des options sur la commande d'ajout pour que :
  - le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans `/home`
  - le shell de l'utilisateur soit `/bin/bash`

        ```bash
        [paja444@node1 ~]$ sudo useradd admin -m -s /bin/bash
        ```
        ```bash
        [paja444@node2 ~]$ sudo useradd admin -m -s /bin/bash
        ```
- prouvez que vous avez correctement créé cet utilisateur
  - et aussi qu'il a le bon shell et le bon homedir

        ```bash
        [paja444@node1 ~]$ sudo useradd admin -m -s /bin/bash
        admin:x:1001:1001::/home/admin:/bin/bash
        ```
        ```bash
        [paja444@node2 ~]$ cat /etc/passwd | grep admin
        admin:x:1001:1001::/home/admin:/bin/bash
        ```

🌞 **Créer un nouveau groupe `admins`** qui contiendra les utilisateurs de la machine ayant accès aux droits de `root` *via* la commande `sudo`.

Pour permettre à ce groupe d'accéder aux droits `root` :

- il faut modifier le fichier `/etc/sudoers`
- on ne le modifie jamais directement à la main car en cas d'erreur de syntaxe, on pourrait bloquer notre accès aux droits administrateur
- la commande `visudo` permet d'éditer le fichier, avec un check de syntaxe avant fermeture
- ajouter une ligne basique qui permet au groupe d'avoir tous les droits (inspirez vous de la ligne avec le groupe `wheel`)

        ```bash
        [paja444@node1 ~]$ sudo groupadd admins
        [paja444@node1 ~]$ sudo visudo
        [paja444@node1 ~]$ sudo cat /etc/sudoers | grep admins
        %admins ALL=(ALL)	ALL
        ```
        ```bash
        [paja444@node2 ~]$ sudo groupadd admins
        [paja444@node2 ~]$ sudo visudo
        [sudo] password for paja444: 
        [paja444@node2 ~]$ sudo cat /etc/sudoers | grep admins
        %admins ALL=(ALL)	ALL
        ```
🌞 **Ajouter votre utilisateur à ce groupe `admins`**

> Essayez d'effectuer une commande avec `sudo` peu importe laquelle, juste pour tester que vous avez le droit d'exécuter des commandes sous l'identité de `root`. Vous pouvez aussi utiliser `sudo -l` pour voir les droits `sudo` auquel votre utilisateur courant a accès.

```bash
[paja444@node1 ~]$ sudo usermod -aG admins admin
[paja444@node1 ~]$ su admin
Password: 
[admin@node1 paja444]$ sudo cat /etc/sudoers | grep admins

We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.

[sudo] password for admin: 
%admins ALL=(ALL)	ALL
```
```bash
[paja444@node2 ~]$ sudo usermod -aG admins admin
[paja444@node2 ~]$ su admin
Password: 
[admin@node2 paja444]$ sudo cat /etc/sudoers | grep admins

We trust you have received the usual lecture from the local System
Administrator. It usually boils down to these three things:

    #1) Respect the privacy of others.
    #2) Think before you type.
    #3) With great power comes great responsibility.

[sudo] password for admin: 
%admins ALL=(ALL)	ALL
```

---

1. Utilisateur créé et configuré
2. Groupe `admins` créé
3. Groupe `admins` ajouté au fichier `/etc/sudoers`
4. Ajout de l'utilisateur au groupe `admins`

### 2. SSH

[Une section dédiée aux clés SSH existe dans le cours.](../../cours/SSH/README.md)

Afin de se connecter à la machine de façon plus sécurisée, on va configurer un échange de clés SSH lorsque l'on se connecte à la machine.

🌞 **Pour cela...**

- il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance (vous :) )
  - génération de clé depuis VOTRE poste donc
  - sur Windows, on peut le faire avec le programme `puttygen.exe` qui est livré avec `putty.exe`

        ```bash
        (base) paja@paja-mac ~ % ssh-keygen -t rsa -b 4096
        Generating public/private rsa key pair.
        Enter file in which to save the key (/Users/paja/.ssh/id_rsa):    
        /Users/paja/.ssh/id_rsa already exists.
        Overwrite (y/n)? n
        ```
- déposer la clé dans le fichier `/home/<USER>/.ssh/authorized_keys` de la machine que l'on souhaite administrer
  - vous utiliserez l'utilisateur que vous avez créé dans la partie précédente du TP
  - on peut le faire à la main
  - ou avec la commande `ssh-copy-id`

        ```bash
        (base) paja@paja-mac ~ % ssh-copy-id admin@10.101.1.11
        /usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/Users/paja/.ssh/id_rsa.pub"
        /usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
        /usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
        paja444@10.101.1.11's password: 

        Number of key(s) added:        1

        Now try logging into the machine, with:   "ssh 'paja444@10.101.1.11'"
        and check to make sure that only the key(s) you wanted were added.

        (base) paja@paja-mac ~ % ssh-copy-id paja444@10.101.1.12
        /usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/Users/paja/.ssh/id_rsa.pub"
        /usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
        /usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
        paja444@10.101.1.12's password: 

        Number of key(s) added:        1

        Now try logging into the machine, with:   "ssh 'admin@10.101.1.12'"
        and check to make sure that only the key(s) you wanted were added.
        ```
        ```bash
        (base) paja@paja-mac ~ % ssh paja444@10.101.1.11
        Last login: Mon Nov 14 15:33:10 2022 from 10.101.1.1
        [paja444@node1 ~]$ 
        ```
        ```bash
        (base) paja@paja-mac ~ % ssh paja444@10.101.1.12
        Last login: Mon Nov 14 16:19:56 2022
        [paja444@node2 ~]$ 
        ```
🌞 **Assurez vous que la connexion SSH est fonctionnelle**, sans avoir besoin de mot de passe.

## II. Partitionnement

[Il existe une section dédiée au partitionnement dans le cours](../../cours/part/)

### 1. Préparation de la VM

⚠️ **Uniquement sur `node1.tp1.b2`.**

Ajout de deux disques durs à la machine virtuelle, de 3Go chacun.

### 2. Partitionnement

⚠️ **Uniquement sur `node1.tp1.b2`.**

🌞 **Utilisez LVM** pour...

- agréger les deux disques en un seul *volume group*
        
        ```bash
        [admin@node1 ~]$ sudo pvcreate /dev/sdb
        [sudo] password for admin: 
        Physical volume "/dev/sdb" successfully created.
        [admin@node1 ~]$ sudo pvcreate /dev/sdc
        Physical volume "/dev/sdc" successfully created.
        [admin@node1 ~]$ sudo vgcreate data /dev/sdb
        Volume group "data" successfully created
        [admin@node1 ~]$ sudo vgextend data /dev/sdc
        Volume group "data" successfully extended
        [admin@node1 ~]$ sudo vgs
        Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VBc5740b74-47b83225_ PVID     hrkxd2ZT4RYlEgtHmntlaoIinFUclgfY last seen on /dev/sda2 not found.
        VG   #PV #LV #SN Attr   VSize VFree
        data   2   0   0 wz--n- 5.99g 5.99g
        ```
- créer 3 *logical volumes* de 1 Go chacun

        ```bash
        [admin@node1 ~]$ sudo lvcreate -L 1G data -n logical_volume_1
        Logical volume "logical_volume_1" created.
        [admin@node1 ~]$ sudo lvcreate -L 1G data -n logical_volume_2
        Logical volume "logical_volume_2" created.
        [admin@node1 ~]$ sudo lvcreate -L 1G data -n logical_volume_3
        Logical volume "logical_volume_3" created.
        [admin@node1 ~]$ sudo lvs
        Devices file sys_wwid t10.ATA_____VBOX_HARDDISK___________________________VBc5740b74-47b83225_ PVID hrkxd2ZT4RYlEgtHmntlaoIinFUclgfY last seen on /dev/sda2 not found.
        LV               VG   Attr       LSize Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
        logical_volume_1 data -wi-a----- 1.00g                                                    
        logical_volume_2 data -wi-a----- 1.00g                                                    
        logical_volume_3 data -wi-a----- 1.00g  
        ```
- formater ces partitions en `ext4`

        ```bash
        [admin@node1 ~]$ mkfs -t ext4 /dev/data/logical_volume_1
        mke2fs 1.46.5 (30-Dec-2021)
        mkfs.ext4: Permission denied while trying to determine filesystem size
        [admin@node1 ~]$ sudo !!
        sudo mkfs -t ext4 /dev/data/logical_volume_1
        mke2fs 1.46.5 (30-Dec-2021)
        Creating filesystem with 262144 4k blocks and 65536 inodes
        Filesystem UUID: 0bc16352-dc91-402c-a84b-308c6059b7a8
        Superblock backups stored on blocks: 
            32768, 98304, 163840, 229376

        Allocating group tables: done                            
        Writing inode tables: done                            
        Creating journal (8192 blocks): done
        Writing superblocks and filesystem accounting information: done
       [admin@node1 ~]$ mkfs -t ext4 /dev/data/logical_volume_1
        mke2fs 1.46.5 (30-Dec-2021)
        mkfs.ext4: Permission denied while trying to determine filesystem size
        [admin@node1 ~]$ sudo !!
        sudo mkfs -t ext4 /dev/data/logical_volume_1
        mke2fs 1.46.5 (30-Dec-2021)
        Creating filesystem with 262144 4k blocks and 65536 inodes
        Filesystem UUID: 0bc16352-dc91-402c-a84b-308c6059b7a8
        Superblock backups stored on blocks: 
            32768, 98304, 163840, 229376

        Allocating group tables: done                            
        Writing inode tables: done                            
        Creating journal (8192 blocks): done
        Writing superblocks and filesystem accounting information: done

        [admin@node1 ~]$ sudo mkfs -t ext4 /dev/data/logical_volume_2
        mke2fs 1.46.5 (30-Dec-2021)
        Creating filesystem with 262144 4k blocks and 65536 inodes
        Filesystem UUID: 374120b4-3416-4324-ac01-ca6397bb3f91
        Superblock backups stored on blocks: 
            32768, 98304, 163840, 229376

        Allocating group tables: done                            
        Writing inode tables: done                            
        Creating journal (8192 blocks): done
        Writing superblocks and filesystem accounting information: done

        [admin@node1 ~]$ sudo mkfs -t ext4 /dev/data/logical_volume_3
        mke2fs 1.46.5 (30-Dec-2021)
        Creating filesystem with 262144 4k blocks and 65536 inodes
        Filesystem UUID: f1d4d56f-3fd2-4a23-9bd3-852f8b0980fa
        Superblock backups stored on blocks: 
            32768, 98304, 163840, 229376

        Allocating group tables: done                            
        Writing inode tables: done                            
        Creating journal (8192 blocks): done
        Writing superblocks and filesystem accounting information: done
        ```

- monter ces partitions pour qu'elles soient accessibles aux points de montage `/mnt/part1`, `/mnt/part2` et `/mnt/part3`.

        ```bash
        [admin@node1 ~]$ sudo mkdir /mnt/part1
        [admin@node1 ~]$ sudo mkdir /mnt/part2
        [admin@node1 ~]$ sudo mkdir /mnt/part3
        [admin@node1 ~]$ sudo mount /dev/data/logical_volume_1 /mnt/part1
        [admin@node1 ~]$ sudo mount /dev/data/logical_volume_2 /mnt/part2
        [admin@node1 ~]$ sudo mount /dev/data/logical_volume_3 /mnt/part3
        [admin@node1 ~]$ df -h
        Filesystem                         Size  Used Avail Use% Mounted on
        devtmpfs                           462M     0  462M   0% /dev
        tmpfs                              481M     0  481M   0% /dev/shm
        tmpfs                              193M  3.0M  190M   2% /run
        /dev/mapper/rl-root                6.2G  1.1G  5.1G  18% /
        /dev/sda1                         1014M  194M  821M  20% /boot
        tmpfs                               97M     0   97M   0% /run/user/1001
        /dev/mapper/data-logical_volume_1  974M   24K  907M   1% /mnt/part1
        /dev/mapper/data-logical_volume_2  974M   24K  907M   1% /mnt/part2
        /dev/mapper/data-logical_volume_3  974M   24K  907M   1% /mnt/part3
        ```
        ```bash
        [admin@node1 ~]$ sudo nano /etc/fstab
        [sudo] password for admin: 
        [admin@node1 ~]$ sudo cat /etc/fstab

        #
        # /etc/fstab
        # Created by anaconda on Fri Sep 30 13:39:32 2022
        #
        # Accessible filesystems, by reference, are maintained under '/dev/disk/'.
        # See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
        #
        # After editing this file, run 'systemctl daemon-reload' to update systemd
        # units generated from this file.
        #
        /dev/mapper/rl-root     /                       xfs     defaults        0 0
        UUID=11474274-a6df-4e4d-8f56-4f6bc3956d33 /boot                   xfs     defaults        0 0
        /dev/mapper/rl-swap     none                    swap    defaults        0 0
        /dev/data/logical_volume_1 /mnt/part1 ext4 defaults 0 0
        /dev/data/logical_volume_2 /mnt/part2 ext4 defaults 0 0
        /dev/data/logical_volume_3 /mnt/part3 ext4 defaults 0 0
        [admin@node1 ~]$ sudo umount /mnt/part1
        [sudo] password for admin: 
        [admin@node1 ~]$ sudo mount -av
        /                        : ignored
        /boot                    : already mounted
        none                     : ignored
        mount: /mnt/part1 does not contain SELinux labels.
            You just mounted a file system that supports labels which does not
            contain labels, onto an SELinux box. It is likely that confined
            applications will generate AVC messages and not be allowed access to
            this file system.  For more details see restorecon(8) and mount(8).
        /mnt/part1               : successfully mounted
        /mnt/part2               : already mounted
        /mnt/part3               : already mounted
        ```


🌞 **Grâce au fichier `/etc/fstab`**, faites en sorte que cette partition soit montée automatiquement au démarrage du système.

✨**Bonus** : amusez vous avez les options de montage. Quelques options intéressantes :

- `noexec`
- `ro`
- `user`
- `nosuid`
- `nodev`
- `protect`

## III. Gestion de services

Au sein des systèmes GNU/Linux les plus utilisés, c'est *systemd* qui est utilisé comme gestionnaire de services (entre autres).

Pour manipuler les services entretenus par *systemd*, on utilise la commande `systemctl`.

On peut lister les unités `systemd` actives de la machine `systemctl list-units -t service`.

**Référez-vous au mémo pour voir les autres commandes `systemctl` usuelles.**

## 1. Interaction avec un service existant

⚠️ **Uniquement sur `node1.tp1.b2`.**

Parmi les services système déjà installés sur Rocky, il existe `firewalld`. Cet utilitaire est l'outil de firewalling de Rocky.

🌞 **Assurez-vous que...**

- l'unité est démarrée

  ```bash
    [admin@node1 ~]$ systemctl is-active firewalld.service
    active
  ```
- l'unitée est activée (elle se lance automatiquement au démarrage)

  ```bash
    [admin@node1 ~]$ systemctl is-enabled firewalld
    enabled
  ```

## 2. Création de service

![Création de service systemd](./pics/create_service.png)

### A. Unité simpliste

⚠️ **Uniquement sur `node1.tp1.b2`.**

🌞 **Créer un fichier qui définit une unité de service** 

- le fichier `web.service`
- dans le répertoire `/etc/systemd/system`

```bash
[admin@node1 ~]$ sudo nano /etc/systemd/system/web.service
```
Déposer le contenu suivant :

```
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888

[Install]
WantedBy=multi-user.target
```

Le but de cette unité est de lancer un serveur web sur le port 8888 de la machine. **N'oubliez pas d'ouvrir ce port dans le firewall.**

```bash
[admin@node1 ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
```
Une fois l'unité de service créée, il faut demander à *systemd* de relire les fichiers de configuration :

```bash
$ sudo systemctl daemon-reload
```

Enfin, on peut interagir avec notre unité :

```bash
$ sudo systemctl status web
$ sudo systemctl start web
$ sudo systemctl enable web
```
```bash
[admin@node1 ~]$ sudo systemctl status web
○ web.service - Very simple web service
     Loaded: loaded (/etc/systemd/system/web.service; disabled; vendor preset: disabled)
     Active: inactive (dead)
[admin@node1 ~]$ sudo systemctl start web
[admin@node1 ~]$ sudo systemctl enable web
Created symlink /etc/systemd/system/multi-user.target.wants/web.service → /etc/systemd/system/web.service.
```

🌞 **Une fois le service démarré, assurez-vous que pouvez accéder au serveur web**

- avec un navigateur depuis votre PC
- ou la commande `curl` depuis l'autre machine (je veux ça dans le compte-rendu :3)

```bash
[admin@node2 paja444]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="afs/">afs/</a></li>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
[admin@node2 paja444]$ 
```
- sur l'IP de la VM, port 8888

```bash
(base) paja@paja-mac ~ % curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="afs/">afs/</a></li>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
(base) paja@paja-mac ~ % 
```

### B. Modification de l'unité

🌞 **Préparez l'environnement pour exécuter le mini serveur web Python**

- créer un utilisateur `web`

```bash
[admin@node1 ~]$ sudo useradd web -m -s /bin/bash
```
- créer un dossier `/var/www/meow/`

```bash
[admin@node1 ~]$ sudo mkdir /var/www
[admin@node1 ~]$ sudo mkdir /var/www/meow/
```
- créer un fichier dans le dossier `/var/www/meow/` (peu importe son nom ou son contenu, c'est pour tester)

```bash
[admin@node1 ~]$ sudo touch /var/www/meow/test
[admin@node1 ~]$ sudo chown web /var/www/meow/test
```
- montrez à l'aide d'une commande les permissions positionnées sur le dossier et son contenu

```bash
[admin@node1 ~]$ ls -al /var/www/meow/test
-rw-r--r--. 1 web root 0 Nov 14 22:02 /var/www/meow/test
```

> Pour que tout fonctionne correctement, il faudra veiller à ce que le dossier et le fichier appartiennent à l'utilisateur `web` et qu'il ait des droits suffisants dessus.

🌞 **Modifiez l'unité de service `web.service` créée précédemment en ajoutant les clauses**

- `User=` afin de lancer le serveur avec l'utilisateur `web` dédié
- `WorkingDirectory=` afin de lancer le serveur depuis le dossier créé au dessus : `/var/www/meow/`
- ces deux clauses sont à positionner dans la section `[Service]` de votre unité

```bash
[Unit]
Description=Very simple web service

[Service]
ExecStart=/usr/bin/python3 -m http.server 8888
User=web
WorkingDirectory=/var/www/meow/
[Install]
WantedBy=multi-user.target
```

🌞 **Vérifiez le bon fonctionnement avec une commande `curl`**

```bash
[admin@node1 paja444]$ sudo systemctl status web.service
[sudo] password for admin: 
● web.service - Very simple web service
     Loaded: loaded (/etc/systemd/system/web.service; enabled; vendor preset: disabled)
     Active: active (running) since Mon 2022-11-14 22:32:26 CET; 31s ago
   Main PID: 796 (python3)
      Tasks: 1 (limit: 5907)
     Memory: 13.3M
        CPU: 147ms
     CGroup: /system.slice/web.service
             └─796 /usr/bin/python3 -m http.server 8888

Nov 14 22:32:26 node1.tp1.b2 systemd[1]: Started Very simple web service.
```
```bash
[admin@node2 paja444]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="afs/">afs/</a></li>
<li><a href="bin/">bin@</a></li>
<li><a href="boot/">boot/</a></li>
<li><a href="dev/">dev/</a></li>
<li><a href="etc/">etc/</a></li>
<li><a href="home/">home/</a></li>
<li><a href="lib/">lib@</a></li>
<li><a href="lib64/">lib64@</a></li>
<li><a href="media/">media/</a></li>
<li><a href="mnt/">mnt/</a></li>
<li><a href="opt/">opt/</a></li>
<li><a href="proc/">proc/</a></li>
<li><a href="root/">root/</a></li>
<li><a href="run/">run/</a></li>
<li><a href="sbin/">sbin@</a></li>
<li><a href="srv/">srv/</a></li>
<li><a href="sys/">sys/</a></li>
<li><a href="tmp/">tmp/</a></li>
<li><a href="usr/">usr/</a></li>
<li><a href="var/">var/</a></li>
</ul>
<hr>
</body>
</html>
[admin@node2 paja444]$ curl 10.101.1.11:8888
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>Directory listing for /</title>
</head>
<body>
<h1>Directory listing for /</h1>
<hr>
<ul>
<li><a href="test">test</a></li>
</ul>
<hr>
</body>
</html>
```
