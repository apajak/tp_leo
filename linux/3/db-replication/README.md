# Module 2 : Réplication de base de données

Il y a plein de façons de mettre en place de la réplication de base données de type MySQL (comme MariaDB).

MariaDB possède un mécanisme de réplication natif qui peut très bien faire l'affaire pour faire des tests comme les nôtres.

Une réplication simple est une configuration de type "master-slave". Un des deux serveurs est le *master* l'autre est un *slave*.

Le *master* est celui qui reçoit les requêtes SQL (des applications comme NextCloud) et qui les traite.

Le *slave* ne fait que répliquer les donneés que le *master* possède.

La [doc officielle de MariaDB](https://mariadb.com/kb/en/setting-up-replication/) ou encore [cet article cool](https://cloudinfrastructureservices.co.uk/setup-mariadb-replication/) expose de façon simple comment mettre en place une telle config.

Pour ce module, vous aurez besoin d'un deuxième serveur de base de données.

## Install:

```bash 
[paja444@db-replication ~]$ sudo dnf install mariadb-server
```
```bash
[paja444@db-replication ~]$ sudo systemctl start mariadb
[paja444@db-replication ~]$ sudo systemctl enable mariadb
```
```bash
[paja444@db-replication ~]$ sudo mysql_secure_installation
[...] (entrée à chaque question)
```
```bash
[paja444@db-replication ~]$ mysqladmin -u root -p version
Enter password: 
mysqladmin  Ver 9.1 Distrib 10.5.16-MariaDB, for Linux on x86_64
Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Server version		10.5.16-MariaDB
Protocol version	10
Connection		Localhost via UNIX socket
UNIX socket		/var/lib/mysql/mysql.sock
Uptime:			1 min 0 sec

Threads: 1  Questions: 21  Slow queries: 0  Opens: 20  Open tables: 13  Queries per second avg: 0.350
```
## configuration DB:


```bash
[paja444@db ~]$ sudo vi /etc/my.cnf
```
```bash
#
# This group is read both both by the client and the server
# use it for options that affect everything
#
[client-server]
#
# include all files from the config directory
#
!includedir /etc/my.cnf.d
[mysqld]
bind-address=10.102.1.12
server-id=1
log_bin=mysql-bin
binlog-format=ROW
```
```bash
[paja444@db ~]$ sudo systemctl restart mariadb
```
```bash
[paja444@db ~]$ sudo mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 4
Server version: 10.5.16-MariaDB-log MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> 
MariaDB [(none)]> 
MariaDB [(none)]> CREATE USER 'replusr'@'%' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.071 sec)

MariaDB [(none)]> GRANT REPLICATION SLAVE ON *.* TO 'replusr'@'%';
Query OK, 0 rows affected (0.014 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.003 sec)

[paja444@db ~]$ sudo mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 4
Server version: 10.5.16-MariaDB-log MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> STOP SLAVE;
Query OK, 0 rows affected, 1 warning (0.002 sec)

MariaDB [(none)]> SHOW MASTER STATUS;
+------------------+----------+--------------+------------------+
| File             | Position | Binlog_Do_DB | Binlog_Ignore_DB |
+------------------+----------+--------------+------------------+
| mysql-bin.000003 |      342 |              |                  |
+------------------+----------+--------------+------------------+
1 row in set (0.001 sec)

MariaDB [(none)]> UNLOCK TABLES;
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> exit
Bye
```
```bash
[paja444@db ~]$ mysqldump -u root -p nextcloud >> nextcloud.sql
Enter password: 
mysqldump: Got error: 1698: "Access denied for user 'root'@'localhost'" when trying to connect
[paja444@db ~]$ sudo !!
sudo mysqldump -u root -p nextcloud >> nextcloud.sql
Enter password: 
[paja444@db ~]$ ls
nextcloud.sql
[paja444@db ~]$ scp nextcloud.sql paja444@10.102.1.14:~/
The authenticity of host '10.102.1.14 (10.102.1.14)' can't be established.
ED25519 key fingerprint is SHA256:WKJYXZhWFk62sD8TjQuylbNOlJkLX2YPmOFpszodUlc.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? YES
Warning: Permanently added '10.102.1.14' (ED25519) to the list of known hosts.
paja444@10.102.1.14's password: 
nextcloud.sql                                                     100%  159KB   8.3MB/s   00:00  
```

## configuration DB-replication

```bash
[paja444@db-replication ~]$ sudo vi /etc/my.cnf
```
```bash
#
# This group is read both both by the client and the server
# use it for options that affect everything
#
[client-server]

#
# include all files from the config directory
#
!includedir /etc/my.cnf.d
[mysqld]
bind-address=10.102.1.14
server-id=2
binlog-format=ROW
```

```bash
[paja444@db-replication ~]$ sudo mysql -u root -p nextcloud<nextcloud.sql
```
```bash
[paja444@db-replication ~]$ sudo mysql -u root -p
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> exit
Bye
```
```bash
[paja444@db-replication ~]$ sudo mysql -u root -p nextcloud<nextcloud.sql
Enter password: 
```
```mysql
[paja444@db-replication ~]$ mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 6
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> stop slave;
Query OK, 0 rows affected, 1 warning (0.001 sec)

MariaDB [(none)]> CHANGE MASTER TO MASTER_HOST = '10.102.1.12', MASTER_USER = 'replusr', MASTER_PASSWORD = 'pewpewpew', MASTER_LOG
_FILE = 'mysql-bin.000003', MASTER_LOG_POS = 342;
Query OK, 0 rows affected (0.039 sec)

MariaDB [(none)]> start slave;
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> show slave status \G
*************************** 1. row ***************************
                Slave_IO_State: Waiting for master to send event
                   Master_Host: 10.102.1.12
                   Master_User: replusr
                   Master_Port: 3306
                 Connect_Retry: 60
               Master_Log_File: mysql-bin.000003
           Read_Master_Log_Pos: 342
                Relay_Log_File: mariadb-relay-bin.000002
                 Relay_Log_Pos: 555
         Relay_Master_Log_File: mysql-bin.000003
              Slave_IO_Running: Yes
             Slave_SQL_Running: Yes
               Replicate_Do_DB: 
           Replicate_Ignore_DB: 
            Replicate_Do_Table: 
        Replicate_Ignore_Table: 
       Replicate_Wild_Do_Table: 
   Replicate_Wild_Ignore_Table: 
                    Last_Errno: 0
                    Last_Error: 
                  Skip_Counter: 0
           Exec_Master_Log_Pos: 342
               Relay_Log_Space: 866
               Until_Condition: None
                Until_Log_File: 
                 Until_Log_Pos: 0
            Master_SSL_Allowed: No
            Master_SSL_CA_File: 
            Master_SSL_CA_Path: 
               Master_SSL_Cert: 
             Master_SSL_Cipher: 
                Master_SSL_Key: 
         Seconds_Behind_Master: 0
 Master_SSL_Verify_Server_Cert: No
                 Last_IO_Errno: 0
                 Last_IO_Error: 
                Last_SQL_Errno: 0
                Last_SQL_Error: 
   Replicate_Ignore_Server_Ids: 
              Master_Server_Id: 1
                Master_SSL_Crl: 
            Master_SSL_Crlpath: 
                    Using_Gtid: No
                   Gtid_IO_Pos: 
       Replicate_Do_Domain_Ids: 
   Replicate_Ignore_Domain_Ids: 
                 Parallel_Mode: optimistic
                     SQL_Delay: 0
           SQL_Remaining_Delay: NULL
       Slave_SQL_Running_State: Slave has read all relay log; waiting for more updates
              Slave_DDL_Groups: 0
Slave_Non_Transactional_Groups: 0
    Slave_Transactional_Groups: 0
1 row in set (0.001 sec)
```

## test

création d'une nouvelle base sur la DB Master:

```mysql
[paja444@db ~]$ sudo mysql -u root -p
[sudo] password for paja444: 
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.5.16-MariaDB-log MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> create database toto;
Query OK, 1 row affected (0.002 sec)

MariaDB [(none)]> exit
Bye
```
vérification sur la DB Slave

```mysql
[paja444@db-replication ~]$ mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 7
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
| toto               |
+--------------------+
5 rows in set (0.002 sec)

MariaDB [(none)]> exit
Bye

```


✨ **Bonus** : Faire en sorte que l'utilisateur créé en base de données ne soit utilisable que depuis l'autre serveur de base de données

- inspirez-vous de la création d'utilisateur avec `CREATE USER` effectuée dans le TP2

✨ **Bonus** : Mettre en place un setup *master-master* où les deux serveurs sont répliqués en temps réel, mais les deux sont capables de traiter les requêtes.

![Replication](../pics/replication.jpg)
