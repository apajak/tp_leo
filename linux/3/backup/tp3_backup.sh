#!/bin/bash
# Script de sauvegarde de la DB MariaDB
# Auteur : Pajak Alexandrer
# Date : 2022-11-21

# Variables
BACKUP_TIME_STAMP="$(date +%Y%m%d%H%M%S)"
LOG_TIMESTAMP="[$(date +%Y-%m-%d_%H:%M:%S)]"
BACKUP_DIR='/srv/backup/'
LOG_FILE='/var/log/backup.log'
NEXTCLOUD_DATA_DIR='/var/www/tp2_nextcloud/data/'
NEXTCLOUD_APPS_DIR='/var/www/tp2_nextcloud/apps/'
NEXTCLOUD_THEMES_DIR='/var/www/tp2_nextcloud/themes/'
NEXTCLOUD_CONFIG_DIR='/var/www/tp2_nextcloud/config/'
BACKUP_FILE_NAME="nextcloud_${BACKUP_TIME_STAMP}.tar.gz"

## preflight checks

# check if the backup directory exists
if [ ! -d "${BACKUP_DIR}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Backup directory ${BACKUP_DIR} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Backup directory ${BACKUP_DIR} does not exist." >> ${LOG_FILE}
    exit 1
fi

# check if the log file exists
if [ ! -f "${LOG_FILE}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Log file ${LOG_FILE} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Log file ${LOG_FILE} does not exist." >> ${LOG_FILE}
    exit 1
fi

# check if the nextcloud data directory exists
if [ ! -d "${NEXTCLOUD_DATA_DIR}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud data directory ${NEXTCLOUD_DATA_DIR} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud data directory ${NEXTCLOUD_DATA_DIR} does not exist." >> ${LOG_FILE}
    exit 1
fi

# check if the nextcloud apps directory exists
if [ ! -d "${NEXTCLOUD_APPS_DIR}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud apps directory ${NEXTCLOUD_APPS_DIR} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud apps directory ${NEXTCLOUD_APPS_DIR} does not exist." >> ${LOG_FILE}
    exit 1
fi

# check if the nextcloud themes directory exists
if [ ! -d "${NEXTCLOUD_THEMES_DIR}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud themes directory ${NEXTCLOUD_THEMES_DIR} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud themes directory ${NEXTCLOUD_THEMES_DIR} does not exist." >> ${LOG_FILE}
    exit 1
fi
# check if the nextcloud config directory exists
if [ ! -d "${NEXTCLOUD_CONFIG_DIR}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud config directory ${NEXTCLOUD_CONFIG_DIR} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud config directory ${NEXTCLOUD_CONFIG_DIR} does not exist." >> ${LOG_FILE}
    exit 1
fi

# do the nextcloud backup
echo "${LOG_TIMESTAMP} INFO: Starting backup of Nextcloud data, apps, themes and config directories."
echo "${LOG_TIMESTAMP} INFO: Starting backup of Nextcloud data, apps, themes and config directories." >> ${LOG_FILE}
tar -czf ${BACKUP_DIR}${BACKUP_FILE_NAME} ${NEXTCLOUD_DATA_DIR} ${NEXTCLOUD_APPS_DIR} ${NEXTCLOUD_THEMES_DIR} ${NEXTCLOUD_CONFIG_DIR}




# check if the backup was successful
if [ $? -eq 0 ]; then
    echo "${LOG_TIMESTAMP} INFO: Backup of Nextcloud data, apps, themes and config directories completed successfully."
    echo "${LOG_TIMESTAMP} INFO: Backup of Nextcloud data, apps, themes and config directories completed successfully." >> ${LOG_FILE}
else
    echo "${LOG_TIMESTAMP} ERROR: Backup of Nextcloud data, apps, themes and config directories failed."
    echo "${LOG_TIMESTAMP} ERROR: Backup of Nextcloud data, apps, themes and config directories failed." >> ${LOG_FILE}
    exit 1
fi



