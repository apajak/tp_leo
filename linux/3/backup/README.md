# Module 4 : Sauvegarde du système de fichiers

Dans cette partie, **on va monter un *serveur de sauvegarde* qui sera chargé d'accueillir les sauvegardes des autres machines**, en particulier du serveur Web qui porte NextCloud.

> Si vous avez fait le module 4, vous pouvez aussi l'utiliser pour stocker les dumps de la base de données.

Le *serveur de sauvegarde* sera un serveur NFS. NFS est un protocole qui permet de partager un dossier à travers le réseau.

Ainsi, notre *serveur de sauvegarde* pourra partager un dossier différent à chaque machine qui a besoin de stocker des données sur le long terme.

Dans le cadre du TP, le serveur partagera un dossier à la machine `web.tp2.linux`.

Sur la machine `web.tp2.linux` s'exécutera à un intervalles réguliers un script qui effectue une sauvegarde des données importantes de NextCloud et les place dans le dossier partagé.

Ainsi, ces données seront archivées sur le *serveur de sauvegarde*.

![Backup everything](../pics/backup_everything.jpg)

## Sommaire

- [Module 4 : Sauvegarde du système de fichiers](#module-4--sauvegarde-du-système-de-fichiers)
  - [Sommaire](#sommaire)
  - [I. Script de backup](#i-script-de-backup)
    - [1. Ecriture du script](#1-ecriture-du-script)
    - [2. Clean it](#2-clean-it)
    - [3. Service et timer](#3-service-et-timer)
  - [II. NFS](#ii-nfs)
    - [1. Serveur NFS](#1-serveur-nfs)
    - [2. Client NFS](#2-client-nfs)

## I. Script de backup

Partie à réaliser sur `web.tp2.linux`.

### 1. Ecriture du script

➜ **Ecrire le script `bash`**

- il s'appellera `tp3_backup.sh`
- il devra être stocké dans le dossier `/srv` sur la machine `web.tp2.linux`
- le script doit commencer par un *shebang* qui indique le chemin du programme qui exécutera le contenu du script
  - ça ressemble à ça si on veut utiliser `/bin/bash` pour exécuter le contenu de notre script :

```bash
[paja444@web ~]$ sudo nano /srv/tp3_backup.sh
[sudo] password for paja444: 
```
```bash
#!/bin/bash
# Script de sauvegarde de la DB MariaDB
# Auteur : Pajak Alexandrer
# Date : 2022-11-21

# Variables
BACKUP_TIME_STAMP="$(date +%Y%m%d%H%M%S)"
LOG_TIMESTAMP="[$(date +%Y-%m-%d_%H:%M:%S)]"
BACKUP_DIR='/srv/backup/'
LOG_FILE='/var/log/backup.log'
NEXTCLOUD_DATA_DIR='/var/www/tp2_nextcloud/data/'
NEXTCLOUD_APPS_DIR='/var/www/tp2_nextcloud/apps/'
NEXTCLOUD_THEMES_DIR='/var/www/tp2_nextcloud/themes/'
NEXTCLOUD_CONFIG_DIR='/var/www/tp2_nextcloud/config/'
BACKUP_FILE_NAME="nextcloud_${BACKUP_TIME_STAMP}.tar.gz"

## preflight checks

# check if the backup directory exists
if [ ! -d "${BACKUP_DIR}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Backup directory ${BACKUP_DIR} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Backup directory ${BACKUP_DIR} does not exist." >> ${LOG_FILE}
    exit 1
fi

# check if the log file exists
if [ ! -f "${LOG_FILE}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Log file ${LOG_FILE} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Log file ${LOG_FILE} does not exist." >> ${LOG_FILE}
    exit 1
fi

# check if the nextcloud data directory exists
if [ ! -d "${NEXTCLOUD_DATA_DIR}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud data directory ${NEXTCLOUD_DATA_DIR} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud data directory ${NEXTCLOUD_DATA_DIR} does not exist." >> ${LOG_FILE}
    exit 1
fi

# check if the nextcloud apps directory exists
if [ ! -d "${NEXTCLOUD_APPS_DIR}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud apps directory ${NEXTCLOUD_APPS_DIR} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud apps directory ${NEXTCLOUD_APPS_DIR} does not exist." >> ${LOG_FILE}
    exit 1
fi

# check if the nextcloud themes directory exists
if [ ! -d "${NEXTCLOUD_THEMES_DIR}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud themes directory ${NEXTCLOUD_THEMES_DIR} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud themes directory ${NEXTCLOUD_THEMES_DIR} does not exist." >> ${LOG_FILE}
    exit 1
fi
# check if the nextcloud config directory exists
if [ ! -d "${NEXTCLOUD_CONFIG_DIR}" ]; then
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud config directory ${NEXTCLOUD_CONFIG_DIR} does not exist."
    echo "${LOG_TIMESTAMP} ERROR: Nextcloud config directory ${NEXTCLOUD_CONFIG_DIR} does not exist." >> ${LOG_FILE}
    exit 1
fi

# do the backup
echo "${LOG_TIMESTAMP} INFO: Starting backup of Nextcloud data, apps, themes and config directories."
echo "${LOG_TIMESTAMP} INFO: Starting backup of Nextcloud data, apps, themes and config directories." >> ${LOG_FILE}
tar -czf ${BACKUP_DIR}${BACKUP_FILE_NAME} ${NEXTCLOUD_DATA_DIR} ${NEXTCLOUD_APPS_DIR} ${NEXTCLOUD_THEMES_DIR} ${NEXTCLOUD_CONFIG_DIR}

# check if the backup was successful
if [ $? -eq 0 ]; then
    echo "${LOG_TIMESTAMP} INFO: Backup of Nextcloud data, apps, themes and config directories completed successfully."
    echo "${LOG_TIMESTAMP} INFO: Backup of Nextcloud data, apps, themes and config directories completed successfully." >> ${LOG_FILE}
else
    echo "${LOG_TIMESTAMP} ERROR: Backup of Nextcloud data, apps, themes and config directories failed."
    echo "${LOG_TIMESTAMP} ERROR: Backup of Nextcloud data, apps, themes and config directories failed." >> ${LOG_FILE}
    exit 1
fi
```

- pour apprendre quels dossiers il faut sauvegarder dans tout le bordel de NextCloud, [il existe une page de la doc officielle qui vous informera](https://docs.nextcloud.com/server/latest/admin_manual/maintenance/backup.html)
- vous devez compresser les dossiers importants
  - au format `.zip` ou `.tar.gz`
  - le fichier produit sera stocké dans le dossier `/srv/backup/`
  - il doit comporter la date, l'heure la minute et la seconde où a été effectué la sauvegarde
    - par exemple : `nextcloud_2211162108.tar.gz`

> On utilise la notation américaine de la date `yymmdd` avec l'année puis le mois puis le jour, comme ça, un tri alphabétique des fichiers correspond à un tri dans l'ordre temporel :)

### 2. Clean it

On va rendre le script un peu plus propre vous voulez bien ?

➜ **Utiliser des variables** déclarées en début de script pour stocker les valeurs suivantes :

- le nom du fichier `.tar.gz` ou `zip` produit par le script

```bash
# Déclaration d'une variable toto qui contient la string "tata"
toto="tata"

# Appel de la variable toto
# Notez l'utilisation du dollar et des double quotes
echo "$toto"
```

---

➜ **Commentez le script**

- au minimum un en-tête sous le shebang
  - date d'écriture du script
  - nom/pseudo de celui qui l'a écrit
  - un résumé TRES BREF de ce que fait le script

---

➜ **Environnement d'exécution du script**

- créez un utilisateur sur la machine `web.tp2.linux`
  - il s'appellera `backup`
  - son homedir sera `/srv/backup/`
  - son shell sera `/usr/bin/nologin`

  ```bash
[paja444@web ~]$ sudo useradd backup -d /srv/backup/ -s /usr/bin/nologin
  ```
  ```bash
[paja444@web www]$ sudo usermod -aG apache backup
[paja444@web backup]$ sudo chown backup /srv/backup
[paja444@web backup]$ sudo chown backup /srv/tp3_backup.sh

  ```
  ```bash
[paja444@web srv]$ sudo touch /var/log/backup.log
[paja444@web log]$ sudo chown backup backup.log 
  ```

  ```bash
[paja444@web srv]$ ls -al
total 4
drwxr-xr-x.  3 root   root   41 Nov 21 21:47 .
dr-xr-xr-x. 18 root   root  235 Nov 18 18:03 ..
drwxr--r--.  2 backup root  179 Nov 21 22:22 backup
-rwxr--r--.  1 backup root 3259 Nov 21 21:43 tp3_backup.sh
  ```
- cet utilisateur sera celui qui lancera le script
- le dossier `/srv/backup/` doit appartenir au user `backup`


- pour tester l'exécution du script en tant que l'utilisateur `backup`, utilisez la commande suivante :

```bash
$ sudo -u backup /srv/tp3_backup.sh
```
```bash
[paja444@web srv]$ sudo -u backup /srv/tp3_backup.sh
[2022-11-21_22:22:33] INFO: Starting backup of Nextcloud data, apps, themes and config directories.
tar: Removing leading `/' from member names
tar: Removing leading `/' from hard link targets
[2022-11-21_22:22:33] INFO: Backup of Nextcloud data, apps, themes and config directories completed successfully.
```

### 3. Service et timer

➜ **Créez un *service*** système qui lance le script

- inspirez-vous du *service* créé à la fin du TP1
- la seule différence est que vous devez rajouter `Type=oneshot` dans la section `[Service]` pour indiquer au système que ce service ne tournera pas à l'infini (comme le fait un serveur web par exemple) mais se terminera au bout d'un moment
- vous appelerez le service `backup.service`
- assurez-vous qu'il fonctionne en utilisant des commandes `systemctl`

```bash
[paja444@web ~]$ sudo nano /etc/systemd/system/backup.service
```
```bash
[Unit]
Description=Nextcloud backup service

[Service]
User=backup  
ExecStart=/srv/tp3_backup.sh 
Type=oneshot


[Install]
WantedBy=multi-user.target
```
```bash
[paja444@web ~]$ sudo systemctl status backup
○ backup.service - Nextcloud backup service
     Loaded: loaded (/etc/systemd/system/backup.service; disabled; vendor preset: disabled)
     Active: inactive (dead)
[paja444@web ~]$ sudo systemctl start backup
```
```bash
[paja444@web ~]$ cat /var/log/backup.log 
[2022-11-21_22:29:01] INFO: Starting backup of Nextcloud data, apps, themes and config directories.
[2022-11-21_22:29:01] INFO: Backup of Nextcloud data, apps, themes and config directories completed successfully.
```

➜ **Créez un *timer*** système qui lance le *service* à intervalles réguliers

- le fichier doit être créé dans le même dossier
- le fichier doit porter le même nom
- l'extension doit être `.timer` au lieu de `.service`
- ainsi votre fichier s'appellera `backup.timer`
- la syntaxe est la suivante :

```bash
[paja444@web ~]$ sudo nano /etc/systemd/system/backup.timer
```
```systemd
[Unit]
Description=Run service backup

[Timer]
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=timers.target
```

> [La doc Arch est cool à ce sujet.](https://wiki.archlinux.org/title/systemd/Timers)

- une fois le fichier créé :

```bash
[paja444@web ~]$ sudo systemctl daemon-reload
[paja444@web ~]$ sudo systemctl start backup.timer
[paja444@web ~]$ sudo systemctl enable backup.timer
Created symlink /etc/systemd/system/timers.target.wants/backup.timer → /etc/systemd/system/backup.timer.
[paja444@web ~]$ sudo systemctl status backup.timer
● backup.timer - Run service backup
     Loaded: loaded (/etc/systemd/system/backup.timer; enabled; vendor preset: disabled)
     Active: active (waiting) since Mon 2022-11-21 22:32:47 CET; 13s ago
      Until: Mon 2022-11-21 22:32:47 CET; 13s ago
    Trigger: Tue 2022-11-22 04:00:00 CET; 5h 26min left
   Triggers: ● backup.service

Nov 21 22:32:47 web.tp2.linux systemd[1]: Started Run service backup.
[paja444@web ~]$ sudo systemctl list-timers
NEXT                        LEFT          LAST                        PASSED    UNIT                         ACTIVATES                     
Mon 2022-11-21 23:11:50 CET 38min left    Mon 2022-11-21 21:43:42 CET 49min ago dnf-makecache.timer          dnf-makecache.service
Tue 2022-11-22 00:00:00 CET 1h 26min left Mon 2022-11-21 14:07:31 CET 8h ago    logrotate.timer              logrotate.service
Tue 2022-11-22 04:00:00 CET 5h 26min left n/a                         n/a       backup.timer                 backup.service
Tue 2022-11-22 14:22:21 CET 15h left      Mon 2022-11-21 14:22:21 CET 8h ago    systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service

4 timers listed.
Pass --all to see loaded but inactive timers, too.
```

## II. NFS

### 1. Serveur NFS

🖥️ **VM `storage.tp3.linux`**

**N'oubliez pas de dérouler la [📝**checklist**📝](../../2/README.md#checklist).**

➜ **Préparer un dossier à partager** sur le réseaucsur la machine `storage.tp3.linux`

- créer un dossier `/srv/nfs_shares`
- créer un sous-dossier `/srv/nfs_shares/web.tp2.linux/`

> Et ouais pour pas que ce soit le bordel, on va appeler le dossier comme la machine qui l'utilisera :)

➜ **Installer le serveur NFS**

- installer le paquet `nfs-utils`
- créer le fichier `/etc/exports` avec le contenu suivant
```bash
[paja444@storage ~]$ cat /etc/exports
/srv/nfs_shares/web.tp2.linux   10.102.1.11(rw,sync,no_root_squash,no_subtree_check)
```
- ouvrir les ports firewall nécessaires
```bash
[paja444@storage ~]$ sudo firewall-cmd --permanent --add-service=nfs
success
[paja444@storage ~]$ sudo firewall-cmd --permanent --add-service=mountd
success
[paja444@storage ~]$ sudo firewall-cmd --permanent --add-service=rpc-bind
success
[paja444@storage ~]$ sudo firewall-cmd --reload
success
```
- démarrer le service

```bash
[paja444@storage ~]$ sudo systemctl start nfs-server
[paja444@storage ~]$ sudo systemctl status nfs-server
[paja444@storage ~]$ sudo systemctl status nfs-server
● nfs-server.service - NFS server and services
     Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; disabled; vendor preset: disabled)
    Drop-In: /run/systemd/generator/nfs-server.service.d
             └─order-with-mounts.conf
     Active: active (exited) since Tue 2022-11-22 18:57:17 CET; 8s ago
    Process: 1740 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=0/SUCCESS)
    Process: 1741 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
    Process: 1758 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then systemctl reload gssproxy ; fi (code=exited, status=0/SUCCESS)
   Main PID: 1758 (code=exited, status=0/SUCCESS)
        CPU: 37ms

Nov 22 18:57:17 storage.tp3.linux systemd[1]: Starting NFS server and services...
Nov 22 18:57:17 storage.tp3.linux systemd[1]: Finished NFS server and services.
```
- je vous laisse check l'internet pour trouver [ce genre de lien](https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-rocky-linux-9) pour + de détails


```bash
[paja444@storage ~]$ sudo mkdir /srv/nfs_shares
[sudo] password for paja444: 
[paja444@storage ~]$ sudo mkdir /srv/nfs_shares/web.tp2.linux/
[paja444@storage ~]$ dnf provides nfs-utils
Rocky Linux 9 - BaseOS                                                                                                        1.4 MB/s | 1.7 MB     00:01    
Rocky Linux 9 - AppStream                                                                                                     5.2 MB/s | 6.0 MB     00:01    
Rocky Linux 9 - Extras                                                                                                         13 kB/s | 6.6 kB     00:00    
nfs-utils-1:2.5.4-10.el9.x86_64 : NFS utilities and supporting clients and daemons for the kernel NFS server
Repo        : baseos
Matched from:
Provide    : nfs-utils = 1:2.5.4-10.el9
[paja444@storage ~]$ sudo dnf install nfs-utils
```
### 2. Client NFS

➜ **Installer un client NFS sur `web.tp2.linux`**

- il devra monter le dossier `/srv/nfs_shares/web.tp2.linux/` qui se trouve sur `storage.tp3.linux`
- le dossier devra être monté sur `/srv/backup/`
- je vous laisse là encore faire vos recherches pour réaliser ça !
- faites en sorte que le dossier soit automatiquement monté quand la machine s'allume

```bash
[paja444@web ~]$ sudo dnf install nfs-utils
[paja444@web ~]$ sudo firewall-cmd --permanent --add-service=nfs
success
[paja444@web ~]$ sudo firewall-cmd --permanent --add-service=mountd
success
[paja444@web ~]$ sudo firewall-cmd --permanent --add-service=rpc-bind
success
[paja444@web ~]$ sudo firewall-cmd --reload
success
[paja444@web ~]$ sudo systemctl start nfs-utils
[paja444@web ~]$ sudo mount 10.102.1.15:/srv/nfs_shares/web.tp2.linux /srv/backup
^C
[paja444@web ~]$ sudo systemctl status nfs-utils
● nfs-utils.service - NFS server and client services
     Loaded: loaded (/usr/lib/systemd/system/nfs-utils.service; static)
     Active: active (exited) since Mon 2022-11-21 23:35:48 CET; 2min 17s ago
    Process: 3176 ExecStart=/bin/true (code=exited, status=0/SUCCESS)
   Main PID: 3176 (code=exited, status=0/SUCCESS)
        CPU: 3ms

Nov 21 23:35:48 web.tp2.linux systemd[1]: Starting NFS server and client services...
Nov 21 23:35:48 web.tp2.linux systemd[1]: Finished NFS server and client services.
[paja444@web ~]$ sudo mount 10.102.1.15:/srv/nfs_shares/web.tp2.linux /srv/backup
```
```bash
[paja444@web ~]$ sudo df -h
Filesystem                                 Size  Used Avail Use% Mounted on
devtmpfs                                   462M     0  462M   0% /dev
tmpfs                                      481M     0  481M   0% /dev/shm
tmpfs                                      193M  5.3M  187M   3% /run
/dev/mapper/rl-root                        6.2G  2.6G  3.7G  41% /
/dev/sda1                                 1014M  224M  791M  23% /boot
tmpfs                                       97M     0   97M   0% /run/user/1000
10.102.1.15:/srv/nfs_shares/web.tp2.linux  6.2G  1.2G  5.1G  19% /srv/backup
```

```bash
[paja444@web srv]$ sudo nano /etc/fstab
[paja444@web srv]$ cat /etc/fstab

#
# /etc/fstab
# Created by anaconda on Fri Sep 30 13:39:32 2022
#
# Accessible filesystems, by reference, are maintained under '/dev/disk/'.
# See man pages fstab(5), findfs(8), mount(8) and/or blkid(8) for more info.
#
# After editing this file, run 'systemctl daemon-reload' to update systemd
# units generated from this file.
#
/dev/mapper/rl-root     /                       xfs     defaults        0 0
UUID=11474274-a6df-4e4d-8f56-4f6bc3956d33 /boot                   xfs     defaults        0 0
/dev/mapper/rl-swap     none                    swap    defaults        0 0
10.102.1.15:/srv/nfs_shares/web.tp2.linux               /srv/backup      nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
```
➜ **Tester la restauration des données** sinon ça sert à rien :)

- livrez-moi la suite de commande que vous utiliseriez pour restaurer les données dans une version antérieure

```bash
[paja444@web ~]$ sudo -u backup tar -xzf nextcloud_20221122000517.tar.gz 
[paja444@web ~]$ sudo -u backup rsync -Aax /srv/backup/var/ /var/www/tp2_nextcloud
```
