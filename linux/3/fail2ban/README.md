# Module 7 : Fail2Ban

## Install:

sur chaque machine:

si pas déja installer:
```bash
[paja444@web ~]$ sudo dnf install fail2ban -y
```
installation du paquet fail2ban:
```bash
[paja444@web ~]$ sudo dnf install fail2ban -y
```
démarer et activer fail2ban:
```bash
[paja444@web ~]$ sudo systemctl start fail2ban.service
[paja444@web ~]$ sudo systemctl enable fail2ban.service
Created symlink /etc/systemd/system/multi-user.target.wants/fail2ban.service → /usr/lib/systemd/system/fail2ban.service.
[paja444@web ~]$ sudo systemclt status fail2ban.service
sudo: systemclt: command not found
[paja444@web ~]$ sudo systemctl status fail2ban.service
● fail2ban.service - Fail2Ban Service
     Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disabled)
     Active: active (running) since Wed 2022-11-23 19:40:21 CET; 41s ago
       Docs: man:fail2ban(1)
   Main PID: 1944 (fail2ban-server)
      Tasks: 3 (limit: 5907)
     Memory: 10.3M
        CPU: 130ms
     CGroup: /system.slice/fail2ban.service
             └─1944 /usr/bin/python3 -s /usr/bin/fail2ban-server -xf start

Nov 23 19:40:21 web.tp2.linux systemd[1]: Starting Fail2Ban Service...
Nov 23 19:40:21 web.tp2.linux systemd[1]: Started Fail2Ban Service.
Nov 23 19:40:21 web.tp2.linux fail2ban-server[1944]: 2022-11-23 19:40:21,554 fail2ban.configreader >
Nov 23 19:40:21 web.tp2.linux fail2ban-server[1944]: Server ready
```

## configuration:

copie et modification du fichier de conf pour garder l'original intact:

```bash
[paja444@web fail2ban]$ sudo cp jail.conf jail.local
[paja444@web fail2ban]$ sudo vi jail.conf
```
```bash
# "bantime" is the number of seconds that a host is banned.
bantime  = 10m

# A host is banned if it has generated "maxretry" during the last "findtime"
# seconds.
findtime  = 1m

# "maxretry" is the number of failures before a host get banned.
maxretry = 3
...
[sshd]

# To use more aggressive sshd modes set filter parameter "mode" in jail.local:
# normal (default), ddos, extra or aggressive (combines all).
# See "tests/files/logs/sshd" or "filter.d/sshd.conf" for usage example and details.
#mode   = normal
enabled = true
port    = ssh
logpath = %(sshd_log)s
backend = %(sshd_backend)s
```

```bash
[paja444@web fail2ban]$ sudo systemctl restart fail2ban
```

## test:

```bash
[paja444@db ~]$ ssh paja444@10.102.1.11
The authenticity of host '10.102.1.11 (10.102.1.11)' can't be established.
ED25519 key fingerprint is SHA256:WKJYXZhWFk62sD8TjQuylbNOlJkLX2YPmOFpszodUlc.
This host key is known by the following other names/addresses:
    ~/.ssh/known_hosts:1: 10.102.1.14
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.102.1.11' (ED25519) to the list of known hosts.
paja444@10.102.1.11's password: 
Permission denied, please try again.
paja444@10.102.1.11's password: 
Permission denied, please try again.
paja444@10.102.1.11's password: 
paja444@10.102.1.11: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
```

```bash
[paja444@web fail2ban]$ sudo fail2ban-client status
Status
|- Number of jail:	1
`- Jail list:	sshd
[paja444@web fail2ban]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed:	0
|  |- Total failed:	3
|  `- Journal matches:	_SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned:	1
   |- Total banned:	1
   `- Banned IP list:	10.102.1.12
```

## unban:

```bash
[paja444@web fail2ban]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed:	1
|  |- Total failed:	21
|  `- Journal matches:	_SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned:	1
   |- Total banned:	2
   `- Banned IP list:	10.102.1.12
```

```bash
[paja444@web fail2ban]$ fail2ban-client set sshd unbanip 10.102.1.12
2022-11-23 20:10:40,809 fail2ban                [2472]: ERROR   Permission denied to socket: /var/run/fail2ban/fail2ban.sock, (you must be root)
[paja444@web fail2ban]$ sudo !!
sudo fail2ban-client set sshd unbanip 10.102.1.12
1
```

```bash
[paja444@web fail2ban]$ sudo fail2ban-client status sshd
Status for the jail: sshd
|- Filter
|  |- Currently failed:	1
|  |- Total failed:	21
|  `- Journal matches:	_SYSTEMD_UNIT=sshd.service + _COMM=sshd
`- Actions
   |- Currently banned:	0
   |- Total banned:	2
   `- Banned IP list:	
```
