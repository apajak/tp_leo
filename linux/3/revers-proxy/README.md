# Module 1 : Reverse Proxy

Un reverse proxy est donc une machine que l'on place devant un autre service afin d'accueillir les clients et servir d'intermédiaire entre le client et le service.

L'utilisation d'un reverse proxy peut apporter de nombreux bénéfices :

- décharger le service HTTP de devoir effectuer le chiffrement HTTPS (coûteux en performances)
- répartir la charge entre plusieurs services
- effectuer de la mise en cache
- fournir un rempart solide entre un hacker potentiel et le service et les données importantes
- servir de point d'entrée unique pour accéder à plusieurs services web

![Not sure](../pics/reverse_proxy.png)

## Sommaire

- [Module 1 : Reverse Proxy](#module-1--reverse-proxy)
  - [Sommaire](#sommaire)
- [I. Intro](#i-intro)
- [II. Setup](#ii-setup)
- [III. HTTPS](#iii-https)

# I. Intro

# II. Setup

🖥️ **VM `proxy.tp3.linux`**

**N'oubliez pas de dérouler la [📝**checklist**📝](#checklist).**

➜ **On utilisera NGINX comme reverse proxy**

- installer le paquet `nginx`

```bash
[paja444@proxy ~]$ dnf provides nginx
Rocky Linux 9 - BaseOS                                              622 kB/s | 1.7 MB     00:02    
Rocky Linux 9 - AppStream                                           1.4 MB/s | 6.0 MB     00:04    
Rocky Linux 9 - Extras                                               10 kB/s | 6.6 kB     00:00    
nginx-1:1.20.1-10.el9.x86_64 : A high performance web server and reverse proxy server
Repo        : appstream
Matched from:
Provide    : nginx = 1:1.20.1-10.el9

[paja444@proxy ~]$ dnf install nginx
Error: This command has to be run with superuser privileges (under the root user on most systems).
[paja444@proxy ~]$ sudo !!
```
- démarrer le service `nginx`

```bash
[paja444@proxy ~]$ sudo systemctl start nginx
[paja444@proxy ~]$ sduo systemctl enable nginx
-bash: sduo: command not found
[paja444@proxy ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[paja444@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-17 15:50:09 CET; 23s ago
   Main PID: 10698 (nginx)
      Tasks: 2 (limit: 5907)
     Memory: 1.9M
        CPU: 37ms
     CGroup: /system.slice/nginx.service
             ├─10698 "nginx: master process /usr/sbin/nginx"
             └─10699 "nginx: worker process"

Nov 17 15:50:09 proxy.tp3.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 17 15:50:09 proxy.tp3.linux nginx[10696]: nginx: the configuration file /etc/nginx/nginx.conf s>
Nov 17 15:50:09 proxy.tp3.linux nginx[10696]: nginx: configuration file /etc/nginx/nginx.conf test >
Nov 17 15:50:09 proxy.tp3.linux systemd[1]: Started The nginx HTTP and reverse proxy server.
```
- utiliser la commande `ss` pour repérer le port sur lequel NGINX écoute

```bash
[paja444@proxy ~]$ sudo ss -tunlp | grep nginx
tcp   LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=10699,fd=6),("nginx",pid=10698,fd=6))
tcp   LISTEN 0      511             [::]:80           [::]:*    users:(("nginx",pid=10699,fd=7),("nginx",pid=10698,fd=7))
```
- ouvrir un port dans le firewall pour autoriser le trafic vers NGINX

```bash
[paja444@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[paja444@proxy ~]$ sudo firewall-cmd --reload
success
[paja444@proxy ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
[paja444@proxy ~]$ sudo firewall-cmd --remove cockpit --permanent
usage: see firewall-cmd man page
firewall-cmd: error: ambiguous option: --remove could match --remove-lockdown-whitelist-command, --remove-lockdown-whitelist-context, --remove-lockdown-whitelist-uid, --remove-lockdown-whitelist-user, --remove-interface, --remove-source, --remove-ingress-zone, --remove-egress-zone, --remove-rich-rule, --remove-service, --remove-port, --remove-protocol, --remove-source-port, --remove-forward, --remove-masquerade, --remove-icmp-block, --remove-icmp-block-inversion, --remove-forward-port, --remove-entry, --remove-entries-from-file, --remove-destination, --remove-module, --remove-helper, --remove-include, --remove-passthrough, --remove-chain, --remove-rule, --remove-rules
[paja444@proxy ~]$ sudo firewall-cmd --remove-service cockpit --permanent
success
[paja444@proxy ~]$ sudo firewall-cmd --remove-service dhcpv6-client --permanent
success
[paja444@proxy ~]$ sudo firewall-cmd --reload
success
[paja444@proxy ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources: 
  services: ssh
  ports: 80/tcp
  protocols: 
  forward: yes
  masquerade: no
  forward-ports: 
  source-ports: 
  icmp-blocks: 
  rich rules: 
```
- utiliser une commande `ps -ef` pour déterminer sous quel utilisateur tourne NGINX

```bash
[paja444@proxy ~]$ sudo ps -ef | grep nginx
root       10698       1  0 15:50 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx      10699   10698  0 15:50 ?        00:00:00 nginx: worker process
paja444    10784     821  0 15:53 pts/0    00:00:00 grep --color=auto nginx
```
- vérifier que le page d'accueil NGINX est disponible en faisant une requête HTTP sur le port 80 de la machine

```bash
[paja444@proxy ~]$ curl -silent localhost:80 -SL | head -n 10
HTTP/1.1 200 OK
Server: nginx/1.20.1
Date: Thu, 17 Nov 2022 14:59:09 GMT
Content-Type: text/html
Content-Length: 7620
Last-Modified: Wed, 06 Jul 2022 02:37:36 GMT
Connection: keep-alive
ETag: "62c4f570-1dc4"
Accept-Ranges: bytes
```

➜ **Configurer NGINX**

- nous ce qu'on veut, c'pas une page d'accueil moche, c'est que NGINX agisse comme un reverse proxy entre les clients et notre serveur Web
- deux choses à faire :
  - créer un fichier de configuration NGINX
    - la conf est dans `/etc/nginx`
    - procédez comme pour Apache : repérez les fichiers inclus par le fichier de conf principal, et créez votre fichier de conf en conséquence

    ```bash
[paja444@proxy ~]$ sudo nano /etc/nginx/conf.d/myConf.conf
[paja444@proxy nginx]$ cat /etc/nginx/conf.d/myConf.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying 
        proxy_pass http://10.102.1.11:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
    ```
  - NextCloud est un peu exigeant, et il demande à être informé si on le met derrière un reverse proxy
    - y'a donc un fichier de conf NextCloud à modifier
    - c'est un fichier appelé `config.php`

    ```bash
[paja444@web ~]$ sudo nano /var/www/tp2_nextcloud/config/config.php
    ```

```bash
<?php
$CONFIG = array (
  'instanceid' => 'oczxfn6bqp3d',
  'passwordsalt' => 'HX38E48cCc7F9vha7SqBA2/uIyw2f6',
  'secret' => 'wNFI6fqo1oJkRTF7w4Gnj3qbzzO9c6tdKrsOqq6SAJc8lWrg',
  'trusted_domains' => 
  array (
    0 => 'web.tp2.linux',
  ),
  'datadirectory' => '/var/www/tp2_nextcloud/data',
  'dbtype' => 'mysql',
  'version' => '25.0.0.15',
  'overwrite.cli.url' => 'http://web.tp2.linux',
  'dbname' => 'nextcloud',
  'dbhost' => '10.102.1.12:3306',
  'dbport' => '',
  'dbtableprefix' => 'oc_',
  'mysql.utf8mb4' => true,
  'dbuser' => 'nextcloud',
  'dbpassword' => 'pewpewpew',
  'installed' => true,
  'trusted_proxies' => 
  array (
    0 => '10.102.1.13',
  ),
  'overwritehost' => 'web.tp2.linux',
  'overwriteprotocol' => 'http',
);
```

Référez-vous à monsieur Google pour tout ça :)

Exemple de fichier de configuration minimal NGINX.:

```nginx
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying 
        proxy_pass http://<IP_DE_NEXTCLOUD>:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```
```bash
[paja444@web ~]$ sudo systemctl restart httpd
[paja444@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
    Drop-In: /usr/lib/systemd/system/httpd.service.d
             └─php81-php-fpm.conf
     Active: active (running) since Thu 2022-11-17 16:16:32 CET; 8s ago
       Docs: man:httpd.service(8)
   Main PID: 1245 (httpd)
     Status: "Started, listening on: port 80"
      Tasks: 213 (limit: 5907)
     Memory: 26.8M
        CPU: 135ms
     CGroup: /system.slice/httpd.service
             ├─1245 /usr/sbin/httpd -DFOREGROUND
             ├─1246 /usr/sbin/httpd -DFOREGROUND
             ├─1247 /usr/sbin/httpd -DFOREGROUND
             ├─1248 /usr/sbin/httpd -DFOREGROUND
             └─1249 /usr/sbin/httpd -DFOREGROUND

Nov 17 16:16:32 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 17 16:16:32 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 17 16:16:32 web.tp2.linux httpd[1245]: Server configured, listening on: port 80
```
```bash
[paja444@proxy ~]$ sudo systemctl restart nginx
[paja444@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-17 16:32:38 CET; 6s ago
    Process: 10965 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 10966 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 10967 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 10968 (nginx)
      Tasks: 2 (limit: 5907)
     Memory: 2.0M
        CPU: 40ms
     CGroup: /system.slice/nginx.service
             ├─10968 "nginx: master process /usr/sbin/nginx"
             └─10969 "nginx: worker process"

Nov 17 16:32:38 proxy.tp3.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 17 16:32:38 proxy.tp3.linux nginx[10966]: nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
Nov 17 16:32:38 proxy.tp3.linux nginx[10966]: nginx: configuration file /etc/nginx/nginx.conf test is successful
Nov 17 16:32:38 proxy.tp3.linux systemd[1]: Started The nginx HTTP and reverse proxy server.
```



➜ **Modifier votre fichier `hosts` de VOTRE PC**

- pour que le service soit joignable avec le nom `web.tp2.linux`

```bash
(base) paja@paja-mac ~ % sudo nano /etc/hosts
##
# Host Database
#
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
##
127.0.0.1       localhost
255.255.255.255 broadcasthost
::1             localhost
# Added by Docker Desktop
# To allow the same kube context to work on the host and the container:
127.0.0.1 kubernetes.docker.internal
# End of section
10.102.1.13     web.tp2.linux
```
- c'est à dire que `web.tp2.linux` doit pointer vers l'IP de `proxy.tp3.linux`
- autrement dit, pour votre PC :
  - `web.tp2.linux` pointe vers l'IP du reverse proxy
  - `proxy.tp3.linux` ne pointe vers rien
  - faire un `ping` manuel vers l'IP de `proxy.tp3.linux` fonctionne
  - faire un `ping` manuel vers l'IP de `web.tp2.linux` ne fonctionne pas
  - taper `http://web.tp2.linux` permet d'accéder au site (en passant de façon transparente par l'IP du proxy)

> Oui vous ne rêvez pas : le nom d'une machine donnée pointe vers l'IP d'une autre ! Ici, on fait juste en sorte qu'un certain nom permette d'accéder au service, sans se soucier de qui porte réellement ce nom.

✨ **Bonus** : rendre le serveur `web.tp2.linux` injoignable sauf depuis l'IP du reverse proxy. En effet, les clients ne doivent pas joindre en direct le serveur web : notre reverse proxy est là pour servir de serveur frontal.

# III. HTTPS

Le but de cette section est de permettre une connexion chiffrée lorsqu'un client se connecte. Avoir le ptit HTTPS :)

Le principe :

- on génère une paire de clés sur le serveur `proxy.tp3.linux`

```bash
[paja444@proxy ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
[paja444@proxy ssl_key]$ sudo cp server.crt web.tp2.linux.crt
[paja444@proxy ssl_key]$ sudo mv web.tp2.linux.crt /etc/pki/tls/certs
[paja444@proxy ssl_key]$ sudo mv web.tp2.linux.key /etc/pki/tls/private
```

  - une des deux clés sera la clé privée : elle restera sur le serveur et ne bougera jamais
  - l'autre est la clé publique : elle sera stockée dans un fichier appelé *certificat*
    - le *certificat* est donné à chaque client qui se connecte au site
- on ajuste la conf NGINX
  - on lui indique le chemin vers le certificat et la clé privée afin qu'il puisse les utiliser pour chiffrer le trafic
  - on lui demande d'écouter sur le port convetionnel pour HTTPS : 443 en TCP

  ```bash
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp2.linux;

    # Port d'écoute de NGINX
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    ssl_certificate         /etc/pki/tls/certs/web.tp2.linux.crt;
    ssl_certificate_key     /etc/pki/tls/private/web.tp2.linux.key;


    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying 
        proxy_pass http://10.102.1.11:80;
        
    }
    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }

}
server {
    listen 80 default_server;
    listen [::]:80 default_server;
    return 301 https://$host$request_uri;
}
  ```
```bash
[paja444@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[paja444@proxy ~]$ sudo firewall-cmd --reload
```
```bash
[paja444@proxy ~]$ sudo systemctl restart nginx
[paja444@proxy ~]$ 
[paja444@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-17 18:09:44 CET; 21s ago
    Process: 11332 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
    Process: 11333 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
    Process: 11334 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 11335 (nginx)
      Tasks: 2 (limit: 5907)
     Memory: 2.7M
        CPU: 57ms
     CGroup: /system.slice/nginx.service
             ├─11335 "nginx: master process /usr/sbin/nginx"
             └─11336 "nginx: worker process"

Nov 17 18:09:44 proxy.tp3.linux systemd[1]: Starting The nginx HTTP and reverse proxy server...
Nov 17 18:09:44 proxy.tp3.linux nginx[11333]: nginx: the configuration file /etc/nginx/nginx.conf s>
Nov 17 18:09:44 proxy.tp3.linux nginx[11333]: nginx: configuration file /etc/nginx/nginx.conf test >
Nov 17 18:09:44 proxy.tp3.linux systemd[1]: Started The nginx HTTP and reverse proxy server.
lines 1-18/18 (END)
```
Je vous laisse Google vous-mêmes "nginx reverse proxy nextcloud" ou ce genre de chose :)
