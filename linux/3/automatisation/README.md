# Module 6 : Automatiser le déploiement

## download:

```bash
[paja444@automatisation ~]$ sudo dnf install git
```
```bash
[paja444@automatisation ~]$ git clone https://gitlab.com/apajak/tp3-automatisation.git
```

## usage:

### web server
give nessesary permissions
```bash
[paja444@automatisation ~]$ sudo chmod ug+x tp3-automatisation/automat/tp3_automation_nextcloud.sh 
```
go to dir `tp3-automatisation/automat/`
```bash
[paja444@automatisation ~]$ cd tp3-automatisation/automat
```
run script: `tp3_automation_nextcloud.sh` as root
```bash
[paja444@automatisation automat]$ sudo ./tp3_automation_nextcloud.sh 
```
### database server
give nessesary permissions
```bash
sudo chmod ug+x tp3-automatisation/automat/tp3_automation_db.sh 
```
go to dir `tp3-automatisation/automat/`
```bash
[paja444@automatisation ~]$ cd tp3-automatisation/automat
```
run script: `tp3_automation_db.sh`
```bash
[paja444@automatisation automat]$ sudo ./tp3_automation_db.sh 
```
