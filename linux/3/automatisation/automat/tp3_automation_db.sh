#!/bin/bash
# Script d'automatisation mariadb
# Auteur : Pajak Alexandre
# Date : 2022-11-23

echo "Welcome to the tp3_automation_db.sh script"

# Variables
# get user imput for the nextcloud user password
read -p "Enter the password for the user: " USER_PASSWORD
# get root password for mariadb from user
read -p "Enter the root password for mariadb: " ROOT_PASSWORD

## preflight checks
# check if the script is run as root
if [ "$EUID" -ne 0 ]
  then echo "[error]: Please run as root"
  exit
fi

## install mariadb
echo "[info]: mariadb installation"
dnf install mariadb-server -y
# run mariadb
echo "[info]: run mariadb"
systemctl start mariadb
# enable mariadb
echo "[info]: enable mariadb"
systemctl enable mariadb
# if mariadb is running continue
if [ "$(systemctl is-active mariadb)" = "active" ]
then
    echo "[info]: mariadb is running"
else
    echo "[error]: Something went wrong with mariadb"
    exit
fi

## mysql_secure_installation
# set root password
echo "[info]: set root password"
mysql_secure_installation <<EOF
y
$ROOT_PASSWORD
$ROOT_PASSWORD
y
y
y
y
EOF
# allow 3306
echo "[info]: allow 3306"
sudo firewall-cmd --add-port=3306/tcp --permanent 
# reload firewall
echo "[info]: reload firewall"

# connect to mysql
echo "[info]: connect to mysql"
mysql -u root -p$ROOT_PASSWORD <<EOF
CREATE USER 'nextcloud'@'localhost' IDENTIFIED BY '$USER_PASSWORD';
CREATE USER 'nextcloud'@'10.102.1.16' IDENTIFIED BY '${USER_PASSWORD}';
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.16';
FLUSH PRIVILEGES;
exit
EOF

echo "[info]: mariadb install and configuration done successfully"
