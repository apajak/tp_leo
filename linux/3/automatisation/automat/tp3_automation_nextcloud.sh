#!/bin/bash
# Script d'automatisation nextcloud
# Auteur : Pajak Alexandrer
# Date : 2022-11-21

echo "Welcome to the tp3_automation_nextcloud.sh script"

# Variables
## preflight checks
# check if the script is run as root
if [ "$EUID" -ne 0 ]
  then echo "[error]: Please run as root"
  exit
fi

## install apache
echo "[info]: apache installation"
dnf install httpd -y
# run apache
echo "[info]: run apache"
systemctl start httpd
# enable apache
echo "[info]: enable apache"
systemctl enable httpd
# if apache2 is running continue
if [ "$(systemctl is-active httpd)" = "active" ]
then
    echo "[info]: Apache2 is running"
else
    echo "[error]: Something went wrong with httpd"
    exit
fi
# enable firewall
echo "[info]: enable firewall"
ufw enable
# allow 80 and 443
echo "[info]: allow 80"
sudo firewall-cmd --add-port=80/tcp --permanent
# reload firewall
echo "[info]: reload firewall"
sudo firewall-cmd --reload

# install php
echo "[info]: install php"
dnf config-manager --set-enabled crb
dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
dnf module list php
dnf module enable php:remi-8.1 -y
dnf install -y php81-php

## install nextcloud
echo "[info]: install nextcloud"
echo "[info]: make a directory for nextcloud"
mkdir /var/www/tp2_nextcloud/
# install curl
echo "[info]: install curl and unzip"
dnf install curl -y
dnf install unzip -y
# download https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip and unzip it in /var/www/tp2_nextcloud/
echo "[info]: download nextcloud"
curl -L https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip -o /var/www/tp2_nextcloud/nextcloud.zip
# unzip nextcloud.zip
echo "[info]: unzip nextcloud"
unzip /var/www/tp2_nextcloud/nextcloud.zip -d /var/www/tp2_nextcloud/
# change owner
echo "[info]: change owner"
chown -R apache:apache /var/www/tp2_nextcloud/

# copy ./myconf.conf to /etc/httpd/conf.d/
echo "[info]: copy ./myconf.conf to /etc/httpd/conf.d/"
cp ./myconf.conf /etc/httpd/conf.d

# restart apache
echo "[info]: restart apache"
systemctl restart httpd

# check if nextcloud is running
echo "[info]: check if nextcloud is running"
if [ "$(systemctl is-active httpd)" = "active" ]
then
    echo "[info]: Nextcloud is running"
else
    echo "[error]: Something went wrong with nextcloud"
    exit
fi

# install mariadb
echo "[info]: apache and nextcloud installation done" 
echo "[info]: for install mariadb"
echo "[info]: please run tp3_automation_db.sh on your db server for the database easy installation.."




