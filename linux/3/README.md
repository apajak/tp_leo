# TP3 : Amélioration de la solution NextCloud

Ce TP se présente sous forme modulaire : il est en plusieurs parties.  
Vous êtes libres d'attaquer les parties qui vous attirent le plus, et il n'est pas attendu de la part de tout le monde de poncer l'intégralité du contenu.

Les différents modules de ce TP s'organisent tous autour des mêmes objectifs : mieux maîtriser l'installation actuelle de NextCloud, augmenter son niveau de qualité et de sécurité, et aller vers des techniques modernes d'administration.

Pour y voir plus clair, j'ai découpé le TP en plusieurs fichiers Markdown, chacun s'attardant sur un sujet donné.

La suite du document est un index des différentes parties du TP.

Pour ce qui est du compte-rendu, j'attendrai que vous me remettez une suite d'instructions pour réaliser les modules que vous choisissez. Un peu comme un petit article/tuto qui explique comment monter le truc. Pas de blabla, les commandes suffisent comme d'habitude.

A chaque fois que vous mettez en place un truc, **vous devez tester que ça fonctionne**.

## Sommaire

- [TP3 : Amélioration de la solution NextCloud](#tp3--amélioration-de-la-solution-nextcloud)
  - [Sommaire](#sommaire)
  - [Stack Web](#stack-web)
    - [Module 1 Reverse Proxy](#module-1-reverse-proxy)
  - [Base de Données](#base-de-données)
    - [Module 2 Réplication de base de données](#module-2-réplication-de-base-de-données)
    - [Module 3 Sauvegarde de base de données](#module-3-sauvegarde-de-base-de-données)
  - [Pérennité de la solution](#pérennité-de-la-solution)
    - [Module 4 Sauvegarde du système de fichiers](#module-4-sauvegarde-du-système-de-fichiers)
    - [Module 5 Monitoring](#module-5-monitoring)
  - [Automatisation](#automatisation)
    - [Module 6 Automatiser le déploiement](#module-6-automatiser-le-déploiement)
  - [Système](#système)
    - [Module 7 Fail2ban](#module-7-fail2ban)

## Stack Web

#### [Stack Web](./revers-proxy)


## Base de Données

#### [Base de Données](./db-replication)

### [Module 3 Sauvegarde de base de données](./3-db-backup/README.md)

#### [DB-Backup](./db-backup)

## Pérennité de la solution

### [Module 4 Sauvegarde du système de fichiers](./4-backup/README.md)

#### [Backup](./backup)

### [Module 5 Monitoring](./5-monitoring/README.md)

#### [Monitoring](./monitoring)

## Automatisation

#### [automatisation](./automatisation)

## Système

#### [Fail2ban](./fail2ban)
