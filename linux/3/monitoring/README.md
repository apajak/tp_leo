# Module 5 : Monitoring

Dans ce sujet on va installer un outil plutôt clé en main pour mettre en place un monitoring simple de nos machines.

L'outil qu'on va utiliser est [Netdata](https://learn.netdata.cloud/docs/agent/packaging/installer/methods/kickstart).

➜ **Je vous laisse suivre la doc pour le mettre en place** [ou ce genre de lien](https://wiki.crowncloud.net/?How_to_Install_Netdata_on_Rocky_Linux_9). Vous n'avez pas besoin d'utiliser le "Netdata Cloud" machin truc. Faites simplement une install locale.

Installez-le sur `web.tp2.linux` et `db.tp2.linux`.

```bash
[paja444@web ~]$ wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh
[paja444@web ~]$ sudo systemctl start netdata
[paja444@web ~]$ sudo systemctl enable netdata
[paja444@web ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
     Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-22 01:11:43 CET; 51s ago
   Main PID: 4524 (netdata)
      Tasks: 59 (limit: 5907)
     Memory: 94.5M
        CPU: 4.642s
     CGroup: /system.slice/netdata.service
             ├─4524 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
             ├─4528 /usr/sbin/netdata --special-spawn-server
             ├─4741 bash /usr/libexec/netdata/plugins.d/tc-qos-helper.sh 1
             ├─4746 /usr/libexec/netdata/plugins.d/apps.plugin 1
             └─4749 /usr/libexec/netdata/plugins.d/go.d.plugin 1
```
```bash
[paja444@web netdata]$ sudo bash edit-config health_alarm_notify.conf
```
sur discord créer un "webhook" dans le channel voulut.
```bash
#------------------------------------------------------------------------------
# discord (discordapp.com) global notification options

# multiple recipients can be given like this:
#                  "CHANNEL1 CHANNEL2 ..."

# enable/disable sending discord notifications
SEND_DISCORD="YES"

# Create a webhook by following the official documentation -
# https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
DISCORD_WEBHOOK_URL="https://discord.com/api/webhooks/1044696554616914061/Id5FjiWr0Jp4QiSrhflrU3CQcY5SxwI8izoeVlGlr2-kw7-2Dk4HMPST6SDLqQyOURB8"

# if a role's recipients are not configured, a notification will be send to
# this discord channel (empty = do not send a notification for unconfigured
# roles):
DEFAULT_RECIPIENT_DISCORD="web-alertes"


#------------------------------------------------------------------------------
```
```bash
[paja444@web netdata]$ sudo firewall-cmd --permanent --add-port=19999/tcp
[paja444@web netdata]$ sudo firewall-cmd --reload
```

## pour tester:

```bash
[paja444@web netdata]$ sudo dnf install stress-ng 
```
```bash
[paja444@web netdata]$ stress-ng --vm 2 --vm-bytes 1G --timeout 60s
```
```
netdata on web.tp2.linux
BOT
 — Today at 9:10 PM
web.tp2.linux is critical, mem.available (system), ram available = 0.88%
ram available = 0.88%
percentage of estimated amount of RAM available for userspace processes, without causing swapping
mem.available
system
Image

web.tp2.linux•Today at 2:01 AM
```
## pour acceder a l'interface web http://10.102.1.11:19999/

