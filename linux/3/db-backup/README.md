# Module 3 : Sauvegarde de base de données

Dans cette partie le but va être d'écrire un script `bash` qui récupère le contenu de la base de données utilisée par NextCloud, afin d'être en mesure de restaurer les données plus tard si besoin.

Le script utilisera la commande `mysqldump` qui permet de récupérer le contenu de la base de données sous la forme d'un fichier `.sql`.

Ce fichier `.sql` on pourra ensuite le compresser et le placer dans un dossier dédié afin de l'archiver.

Une fois le script fonctionnel, on créera alors un service qui permet de déclencher l'exécution de ce script dans de bonnes conditions.

Enfin, un *timer* permettra de déclencher l'exécution du *service* à intervalles réguliers.

![Kitten me](../pics/kittenme.jpg)

## I. Script dump

➜ **Créer un utilisateur DANS LA BASE DE DONNEES**

- inspirez-vous des commandes SQL que je vous ai données au TP2
- l'utilisateur doit pouvoir se connecter depuis `localhost`
- il doit avoir les droits sur la base de données `nextcloud` qu'on a créé au TP2

```bash
[paja444@db ~]$ sudo mysql -u root -p
[sudo] password for paja444: 
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 5
Server version: 10.5.16-MariaDB-log MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'adminDB'@'localhost' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.037 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'adminDB'@'localhost';
Query OK, 0 rows affected (0.007 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

- l'idée est d'avoir un utilisateur qui est dédié aux dumps de la base
  - votre script l'utilisera pour se connecter à la base et extraire les données


➜ **Ecrire le script `bash`**

création du fihcier script

```bash
[paja444@db ~]$ sudo nano /srv/tp3_db_dump.sh
```
```bash
#!/bin/bash
# Script de sauvegarde de la DB MariaDB
# Auteur : Pajak Alexandrer
# Date : 2022-11-21

# Variables
BACKUP_TIME_STAMP="$(date +%Y%m%d%H%M%S)"
LOG_TIMESTAMP="[$(date +%Y-%m-%d_%H:%M:%S)]"
BACKUP_DIR='/srv/db_dumps/'
LOG_FILE='/var/log/db_dumps.log'
ENV_FILE='/srv/.env'
DB_USER='adminDB'

# read options and add default values
# -h : help
# -d : database name
# -u : database user
# -p : database user password
# -f : directory where to store the backup
# -l : log file
# -e : .env file
while getopts "hd:u:p:f:l:e:" opt; do
    case $opt in
        h)
            echo "Usage: $0 [-h] [-d database_name] [-u database_user] [-p database_user_password] [-f backup_directory] [-l log_file] [-e .env_file]"
            echo "Options:"
            echo "  -h  show this help text"
            echo "  -d  database name"
            echo "  -u  database user"
            echo "  -p  database user password"
            echo "  -f  directory where to store the backup"
            echo "  -l  log file"
            echo "  -e  .env file"
            exit 0
            ;;
        d)
            DB_NAME=$OPTARG
            ;;
        u)
            DB_USER=$OPTARG
            ;;
        p)
            DB_USER_PASSWD=$OPTARG
            ;;
        f)
            BACKUP_DIR=$OPTARG
            ;;
        l)
            LOG_FILE=$OPTARG
            ;;
        e)
            ENV_FILE=$OPTARG
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
    esac
done

## preflight checks

# check if the backup directory exists
if [ ! -d ${BACKUP_DIR} ]; then
    echo "The backup directory does not exist. Creating it..."
fi
# check if the log file exists
if [ ! -f ${LOG_FILE} ]; then
    echo "The log file does not exist. Creating it..."
fi

# check .env file
if [ ! -f ${ENV_FILE} ]; then
    echo "ERROR: .env file not found"
    exit 1
fi

# check if the backup directory is writable
if [ ! -w ${BACKUP_DIR} ]; then
    echo "ERROR: Backup directory is not writable"
    exit 1
fi
# check if the log file is writable
if [ ! -w ${LOG_FILE} ]; then
    echo "ERROR: Log file is not writable"
    exit 1
fi
# check if the .env file is readable
if [ ! -r ${ENV_FILE} ]; then
    echo "ERROR: .env file is not readable"
    exit 1
fi
# if all checks are ok, continue
echo "All preflight checks passed. Continuing..."
source "${ENV_FILE}"
DB_USER_PASSWD="${DB_ADMIN_PSSWD}"

## backup

# if the database name is provided, backup only this database
if [ -n "${DB_NAME}" ]; then
    echo "${LOG_TIMESTAMP} INFO: single dump of database ${DB_NAME} started"
    echo "${LOG_TIMESTAMP} INFO: single dump of database ${DB_NAME} started" >> ${LOG_FILE}
    mysqldump -u ${DB_USER} -p${DB_USER_PASSWD} ${DB_NAME} > ${BACKUP_DIR}${DB_NAME}_${BACKUP_TIME_STAMP}.sql
    # check if the dump was successful
    if [ $? -eq 0 ]; then
        echo "${LOG_TIMESTAMP} INFO: Database ${DB_NAME} dumped successfully by ${DB_USER} " >> ${LOG_FILE}
    else
        echo "${LOG_TIMESTAMP} ERROR: ${DB_USER} Database ${DB_NAME} dump failed by ${DB_USER} " >> ${LOG_FILE}
        exit 1
    fi
    # compress the dump
    echo "${LOG_TIMESTAMP} INFO: Compressing database ${DB}"
    gzip ${BACKUP_DIR}${DB_NAME}_${BACKUP_TIME_STAMP}.sql
    # check if the compression was successful
    if [ $? -eq 0 ]; then
        echo "${LOG_TIMESTAMP} INFO: Database ${DB_NAME} compressed successfully by ${DB_USER} " >> ${LOG_FILE}
    else
        echo "${LOG_TIMESTAMP} ERROR: ${DB_USER} Database ${DB_NAME} compression failed by ${DB_USER} " >> ${LOG_FILE}
        exit 1
    fi
# if the database name is not provided, backup all databases
else
    # for each database in the list of databases dump it
    # get the list of databases
    echo "${LOG_TIMESTAMP} INFO: Dump of all databases started"
    echo "${LOG_TIMESTAMP} INFO: Dump of all databases started" >> ${LOG_FILE}
    # get the list of databases
    DB_LIST="$(mysql -u ${DB_USER} -p${DB_USER_PASSWD} -e 'show databases' -s --skip-column-names | grep -v -E 'information_schema|performance_schema|mysql|sys')"
    echo "Databases to backup: ${DB_LIST}"
    # dump each database
    for DB in ${DB_LIST}; do
        echo "${LOG_TIMESTAMP} INFO: ${DB_USER} Dumping database ${DB}"
        mysqldump -u ${DB_USER} -p${DB_USER_PASSWD} ${DB} > ${BACKUP_DIR}${DB}_${BACKUP_TIME_STAMP}.sql
        # check if the dump was successful
        if [ $? -eq 0 ]; then
            echo "${LOG_TIMESTAMP} INFO: Database ${DB} dumped successfully by ${DB_USER} " >> ${LOG_FILE}
        else
            echo "${LOG_TIMESTAMP} ERROR: Database ${DB} dump failed by ${DB_USER}" >> ${LOG_FILE}
        fi
        # compress the dump
        echo "${LOG_TIMESTAMP} INFO: Compressing database ${DB}"
        gzip ${BACKUP_DIR}${DB}_${BACKUP_TIME_STAMP}.sql
        # check if the compression was successful
        if [ $? -eq 0 ]; then
            echo "${LOG_TIMESTAMP} INFO: Database ${DB} compressed successfully" >> ${LOG_FILE}
        else
            echo "${LOG_TIMESTAMP} ERROR: Database ${DB} compression failed" >> ${LOG_FILE}
        fi
    done
echo "${LOG_TIMESTAMP} INFO: ${DB_LIST} database(s) dumped successfully"
echo "${LOG_TIMESTAMP} INFO: ${DB_LIST} database(s) dumped successfully" >> ${LOG_FILE}
fi
```
création du fichier de stockage des dumps
```bash
[paja444@db ~]$ sudo mkdir /srv/db_dumps/
```
création du ficher de stockage des logs
```bash
[paja444@db ~]$ sudo mkdir /var/log/db_dumps/
```
création de l'utilisateur dédier aux backup de la base de donnée
```bash
[paja444@db ~]$ sudo useradd db_dumps -d /srv/db_dumps/ -s /usr/bin/nologin
[sudo] password for paja444: 
useradd: Warning: missing or non-executable shell '/usr/bin/nologin'
useradd: warning: the home directory /srv/db_dumps/ already exists.
useradd: Not copying any file from skel directory into it.
```
```bash
[paja444@db ~]$ cat /etc/passwd | grep db_dumps
db_dumps:x:1001:1001::/srv/db_dumps/:/usr/bin/nologin
```
lui donné la propriété des fichiers de sauvegarde et de log

```bash
[paja444@db srv]$ sudo chown db_dumps tp3_db_dump.sh
[paja444@db srv]$ sudo chown db_dumps .env
[paja444@db srv]$ sudo chown db_dumps db_dumps
[paja444@db ~]$ sudo chmod 744 /srv/tp3_db_dump.sh 
[paja444@db ~]$ ls -al /srv
total 8
drwxr-xr-x.  3 root     root   56 Nov 21 17:11 .
dr-xr-xr-x. 18 root     root  246 Nov 21 16:16 ..
drwxr-xr-x.  2 db_dumps root   46 Nov 21 17:27 db_dumps
-rw-r--r--.  1 db_dumps root   25 Nov 21 16:22 .env
-rwxr--r--.  1 db_dumps root 1495 Nov 21 17:31 tp3_db_dump.sh
```
```bash
[paja444@db ~]$ sudo chown db_dumps /var/log/db_dumps.log 
[paja444@db ~]$ ls -al /var/log/db_dumps.log 
-rw-r--r--. 1 db_dumps root 0 Nov 21 17:06 /var/log/db_dumps.log
```
executer avec: 

## usage
```bash
[paja444@db ~]$ sudo -u db_dumps /srv/tp3_db_dump.sh 
All preflight checks passed. Continuing...
[2022-11-21_21:07:55] INFO: Dump of all databases started
Databases to backup: nextcloud
[2022-11-21_21:07:55] INFO: adminDB Dumping database nextcloud
[2022-11-21_21:07:55] INFO: Compressing database nextcloud
[2022-11-21_21:07:55] INFO: nextcloud database(s) dumped successfully
[paja444@db ~]$ 
```
```bash
[paja444@db ~]$ sudo -u db_dumps /srv/tp3_db_dump.sh -h
[sudo] password for paja444: 
Usage: /srv/tp3_db_dump.sh [-h] [-d database_name] [-u database_user] [-p database_user_password] [-f backup_directory] [-l log_file] [-e .env_file]
Options:
  -h  show this help text
  -d  database name
  -u  database user
  -p  database user password
  -f  directory where to store the backup
  -l  log file
  -e  .env file

```
## III. Service et timer

➜ **Créez un *service*** système qui lance le script

- inspirez-vous du *service* créé à la fin du TP1
- la seule différence est que vous devez rajouter `Type=oneshot` dans la section `[Service]` pour indiquer au système que ce service ne tournera pas à l'infini (comme le fait un serveur web par exemple) mais se terminera au bout d'un moment

```bash
[paja444@db ~]$ sudo nano /etc/systemd/system/db-dump.serviced
```
```bash
[Unit]
Description=MariaDB backup service

[Service]
User=db_dumps
ExecStart=/srv/tp3_db_dump.sh
Type=oneshot


[Install]
WantedBy=multi-user.target
```
- vous appelerez le service `db-dump.service`
- assurez-vous qu'il fonctionne en utilisant des commandes `systemctl`

```bash
[paja444@db ~]$ sudo systemctl status db-dump
○ db-dump.service - MariaDB backup service
     Loaded: loaded (/etc/systemd/system/db-dump.service; disabled; vendor preset: disabled)
     Active: inactive (dead)
[paja444@db ~]$ sudo systemctl start db-dump
```
```bash
[paja444@db ~]$ cat /var/log/db_dumps.log 
```
```bash
[2022-11-21_20:42:45] INFO: Dump of all databases started
[2022-11-21_20:42:45] INFO: Database nextcloud dumped successfully by adminDB 
[2022-11-21_20:42:45] INFO: Database nextcloud compressed successfully
[2022-11-21_20:42:45] INFO: nextcloud database(s) dumped successfully
```

➜ **Créez un *timer*** système qui lance le *service* à intervalles réguliers

- le fichier doit être créé dans le même dossier
- le fichier doit porter le même nom
- l'extension doit être `.timer` au lieu de `.service`
- ainsi votre fichier s'appellera `db-dump.timer`
- la syntaxe est la suivante :

```bash
[paja444@db ~]$ sudo nano /etc/systemd/system/db-dump.timer
```
```systemd
[Unit]
Description=Run service db-dump

[Timer]
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=timers.target
```

> [La doc Arch est cool à ce sujet.](https://wiki.archlinux.org/title/systemd/Timers)

- une fois le fichier créé :

```bash
# demander au système de lire le contenu des dossiers de config
# il découvrira notre nouveau timer
$ sudo systemctl daemon-reload

# on peut désormais interagir avec le timer
$ sudo systemctl start db-dump.timer
$ sudo systemctl enable db-dump.timer
$ sudo systemctl status db-dump.timer

# il apparaîtra quand on demande au système de lister tous les timers
$ sudo systemctl list-timers
```
```bash
[paja444@db ~]$ sudo systemctl daemon-reload
[paja444@db ~]$ sudo systemctl start db-dump.timer
[paja444@db ~]$ sudo systemctl enable db-dump.timer
Created symlink /etc/systemd/system/timers.target.wants/db-dump.timer → /etc/systemd/system/db-dump.timer.
[paja444@db ~]$ sudo systemctl status db-dump.timer
● db-dump.timer - Run service db-dump
     Loaded: loaded (/etc/systemd/system/db-dump.timer; enabled; vendor preset: disabled)
     Active: active (waiting) since Mon 2022-11-21 20:50:12 CET; 31s ago
      Until: Mon 2022-11-21 20:50:12 CET; 31s ago
    Trigger: Tue 2022-11-22 04:00:00 CET; 7h left
   Triggers: ● db-dump.service

Nov 21 20:50:12 db.tp2.linux systemd[1]: Started Run service db-dump.
[paja444@db ~]$ sudo systemctl list-timers
NEXT                        LEFT         LAST                        PASSED    UNIT                         ACTIVATES                     
Mon 2022-11-21 21:07:02 CET 15min left   Mon 2022-11-21 20:06:00 CET 45min ago dnf-makecache.timer          dnf-makecache.service
Tue 2022-11-22 00:00:00 CET 3h 8min left Mon 2022-11-21 14:07:30 CET 6h ago    logrotate.timer              logrotate.service
Tue 2022-11-22 04:00:00 CET 7h left      n/a                         n/a       db-dump.timer                db-dump.service
Tue 2022-11-22 14:23:00 CET 17h left     Mon 2022-11-21 14:23:00 CET 6h ago    systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service

4 timers listed.
Pass --all to see loaded but inactive timers, too.
```

➜ **Tester la restauration des données** sinon ça sert à rien :)

- livrez-moi la suite de commande que vous utiliseriez pour restaurer les données dans une version antérieure

```bash
[paja444@db db_dumps]$ sudo gunzip -q nextcloud_20221121204245.sql.gz 
[paja444@db db_dumps]$ ls
nextcloud_20221121204150.sql.gz  nextcloud_20221121204245.sql
```
```bash
[paja444@db db_dumps]$ mysql -u adminDB -p nextcloud < nextcloud_20221121204245.sql
Enter password: 
[paja444@db db_dumps]$ 
```
