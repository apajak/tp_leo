#!/bin/bash
# Script de sauvegarde de la DB MariaDB
# Auteur : Pajak Alexandrer
# Date : 2022-11-21

# Variables
BACKUP_TIME_STAMP="$(date +%Y%m%d%H%M%S)"
LOG_TIMESTAMP="[$(date +%Y-%m-%d_%H:%M:%S)]"
BACKUP_DIR='/srv/db_dumps/'
LOG_FILE='/var/log/db_dumps.log'
ENV_FILE='/srv/.env'
DB_USER='adminDB'

# read options and add default values
# -h : help
# -d : database name
# -u : database user
# -p : database user password
# -f : directory where to store the backup
# -l : log file
# -e : .env file
while getopts "hd:u:p:f:l:e:" opt; do
    case $opt in
        h)
            echo "Usage: $0 [-h] [-d database_name] [-u database_user] [-p database_user_password] [-f backup_directory] [-l log_file] [-e .env_file]"
            echo "Options:"
            echo "  -h  show this help text"
            echo "  -d  database name"
            echo "  -u  database user"
            echo "  -p  database user password"
            echo "  -f  directory where to store the backup"
            echo "  -l  log file"
            echo "  -e  .env file"
            exit 0
            ;;
        d)
            DB_NAME=$OPTARG
            ;;
        u)
            DB_USER=$OPTARG
            ;;
        p)
            DB_USER_PASSWD=$OPTARG
            ;;
        f)
            BACKUP_DIR=$OPTARG
            ;;
        l)
            LOG_FILE=$OPTARG
            ;;
        e)
            ENV_FILE=$OPTARG
            ;;
        \?)
            echo "Invalid option: -$OPTARG" >&2
            exit 1
            ;;
        :)
            echo "Option -$OPTARG requires an argument." >&2
            exit 1
            ;;
    esac
done

## preflight checks

# check if the backup directory exists
if [ ! -d ${BACKUP_DIR} ]; then
    echo "The backup directory does not exist. Creating it..."
fi
# check if the log file exists
if [ ! -f ${LOG_FILE} ]; then
    echo "The log file does not exist. Creating it..."
fi

# check .env file
if [ ! -f ${ENV_FILE} ]; then
    echo "ERROR: .env file not found"
    exit 1
fi

# check if the backup directory is writable
if [ ! -w ${BACKUP_DIR} ]; then
    echo "ERROR: Backup directory is not writable"
    exit 1
fi
# check if the log file is writable
if [ ! -w ${LOG_FILE} ]; then
    echo "ERROR: Log file is not writable"
    exit 1
fi
# check if the .env file is readable
if [ ! -r ${ENV_FILE} ]; then
    echo "ERROR: .env file is not readable"
    exit 1
fi
# if all checks are ok, continue
echo "All preflight checks passed. Continuing..."
source "${ENV_FILE}"
DB_USER_PASSWD="${DB_ADMIN_PSSWD}"

## backup

# if the database name is provided, backup only this database
if [ -n "${DB_NAME}" ]; then
    echo "${LOG_TIMESTAMP} INFO: single dump of database ${DB_NAME} started"
    echo "${LOG_TIMESTAMP} INFO: single dump of database ${DB_NAME} started" >> ${LOG_FILE}
    mysqldump -u ${DB_USER} -p${DB_USER_PASSWD} ${DB_NAME} > ${BACKUP_DIR}${DB_NAME}_${BACKUP_TIME_STAMP}.sql
    # check if the dump was successful
    if [ $? -eq 0 ]; then
        echo "${LOG_TIMESTAMP} INFO: Database ${DB_NAME} dumped successfully by ${DB_USER} " >> ${LOG_FILE}
    else
        echo "${LOG_TIMESTAMP} ERROR: ${DB_USER} Database ${DB_NAME} dump failed by ${DB_USER} " >> ${LOG_FILE}
        exit 1
    fi
    # compress the dump
    echo "${LOG_TIMESTAMP} INFO: Compressing database ${DB}"
    gzip ${BACKUP_DIR}${DB_NAME}_${BACKUP_TIME_STAMP}.sql
    # check if the compression was successful
    if [ $? -eq 0 ]; then
        echo "${LOG_TIMESTAMP} INFO: Database ${DB_NAME} compressed successfully by ${DB_USER} " >> ${LOG_FILE}
    else
        echo "${LOG_TIMESTAMP} ERROR: ${DB_USER} Database ${DB_NAME} compression failed by ${DB_USER} " >> ${LOG_FILE}
        exit 1
    fi 

# if the database name is not provided, backup all databases
else
    # for each database in the list of databases dump it
    # get the list of databases
    echo "${LOG_TIMESTAMP} INFO: Dump of all databases started"
    echo "${LOG_TIMESTAMP} INFO: Dump of all databases started" >> ${LOG_FILE}
    # get the list of databases
    DB_LIST="$(mysql -u ${DB_USER} -p${DB_USER_PASSWD} -e 'show databases' -s --skip-column-names | grep -v -E 'information_schema|performance_schema|mysql|sys')"
    echo "Databases to backup: ${DB_LIST}"
    # dump each database
    for DB in ${DB_LIST}; do
        echo "${LOG_TIMESTAMP} INFO: ${DB_USER} Dumping database ${DB}"
        mysqldump -u ${DB_USER} -p${DB_USER_PASSWD} ${DB} > ${BACKUP_DIR}${DB}_${BACKUP_TIME_STAMP}.sql
        # check if the dump was successful
        if [ $? -eq 0 ]; then
            echo "${LOG_TIMESTAMP} INFO: Database ${DB} dumped successfully by ${DB_USER} " >> ${LOG_FILE}
        else
            echo "${LOG_TIMESTAMP} ERROR: Database ${DB} dump failed by ${DB_USER}" >> ${LOG_FILE}
        fi
        # compress the dump
        echo "${LOG_TIMESTAMP} INFO: Compressing database ${DB}"
        gzip ${BACKUP_DIR}${DB}_${BACKUP_TIME_STAMP}.sql
        # check if the compression was successful
        if [ $? -eq 0 ]; then
            echo "${LOG_TIMESTAMP} INFO: Database ${DB} compressed successfully" >> ${LOG_FILE}
        else
            echo "${LOG_TIMESTAMP} ERROR: Database ${DB} compression failed" >> ${LOG_FILE}
        fi
    done
echo "${LOG_TIMESTAMP} INFO: ${DB_LIST} database(s) dumped successfully"
echo "${LOG_TIMESTAMP} INFO: ${DB_LIST} database(s) dumped successfully" >> ${LOG_FILE}
fi