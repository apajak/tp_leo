#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2022/11/24
# version ='1.0'
# ---------------------------------------------------------------------------
""" Simple TCP server for TP4.linux """
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import socket

# Global variables
# ---------------------------------------------------------------------------
host_ip = "0.0.0.0"
port = 5000

# Functions
# ---------------------------------------------------------------------------
def main():
    """Main function"""
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Bind the socket to the port
    server_address = (host_ip, port)
    print(f"starting up on {host_ip} port {port}")
    sock.bind(server_address)

    # Listen for incoming connections
    sock.listen(1)

    # Wait for a connection
    print("waiting for a connection")
    connection, client_address = sock.accept()
    try:
        print("connection from", client_address)

        # Receive the data in small chunks and retransmit it
        while True:
            data = connection.recv(16)
            print("received {!r}".format(data))
            if data:
                print("sending data back to the client")
                connection.sendall(data)
            else:
                print("no data from", client_address)
                break

    finally:
        # Clean up the connection
        connection.close()


# ---------------------------------------------------------------------------

if __name__ == "__main__":
    main()
