#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# ----------------------------------------------------------------------------
# Created By  : Pajak Alexandre
# Created Date: 2022/11/24
# version ='1.0'
# ---------------------------------------------------------------------------
""" Simple TCP client for TP4.linux """
# ---------------------------------------------------------------------------
# Imports
# ---------------------------------------------------------------------------
import socket

# Global variables
# ---------------------------------------------------------------------------
host_ip = "10.104.1.11"
port = 5000

# Functions
# ---------------------------------------------------------------------------
def main():
    """Main function"""
    # Create a TCP/IP socket
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # Connect the socket to the port where the server is listening
    server_address = (host_ip, port)
    print(f"connecting to {host_ip} port {port}")
    sock.connect(server_address)

    try:
        # wait for user input
        message = input("Enter message to send: ")
        # Send data
        print(f"sending {message}")
        sock.sendall(str.encode(message))

        # Look for the response
        amount_received = 0
        amount_expected = len(message)

        while amount_received < amount_expected:
            data = sock.recv(16)
            amount_received += len(data)
            print(f"received {data}")

    finally:
        print("closing socket")
        sock.close()

# ---------------------------------------------------------------------------
if __name__ == "__main__":
    main()
