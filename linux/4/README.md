# TP4 : Conteneurs

## I. Docker

### 1. Install

```bash
[paja444@docker ~]$ sudo dnf config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```
```bash
[paja444@docker ~]$ sudo dnf install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```
- start service
```bash
[paja444@docker ~]$ sudo systemctl start docker
[paja444@docker ~]$ sudo systemctl enable docker
Created symlink /etc/systemd/system/multi-user.target.wants/docker.service → /usr/lib/systemd/system/docker.service.
[paja444@docker ~]$ sudo systemctl status docker
● docker.service - Docker Application Container Engine
     Loaded: loaded (/usr/lib/systemd/system/docker.service; enabled; vendor preset: disabled)
     Active: active (running) since Thu 2022-11-24 15:22:35 CET; 14s ago
TriggeredBy: ● docker.socket
       Docs: https://docs.docker.com
   Main PID: 12306 (dockerd)
      Tasks: 7
     Memory: 23.9M
        CPU: 212ms
     CGroup: /system.slice/docker.service
             └─12306 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock

Nov 24 15:22:33 docker.tp4.linux dockerd[12306]: time="2022-11-24T15:22:33.839568188+01:00" level=info msg="ClientConn switching balancer to \"pick_first\"" >
Nov 24 15:22:33 docker.tp4.linux dockerd[12306]: time="2022-11-24T15:22:33.841048969+01:00" level=error msg="Failed to built-in GetDriver graph btrfs /var/li>
Nov 24 15:22:33 docker.tp4.linux dockerd[12306]: time="2022-11-24T15:22:33.877573209+01:00" level=info msg="Loading containers: start."
Nov 24 15:22:34 docker.tp4.linux dockerd[12306]: time="2022-11-24T15:22:34.679975659+01:00" level=info msg="Default bridge (docker0) is assigned with an IP a>
Nov 24 15:22:34 docker.tp4.linux dockerd[12306]: time="2022-11-24T15:22:34.812701949+01:00" level=info msg="Firewalld: interface docker0 already part of dock>
Nov 24 15:22:34 docker.tp4.linux dockerd[12306]: time="2022-11-24T15:22:34.989757890+01:00" level=info msg="Loading containers: done."
Nov 24 15:22:35 docker.tp4.linux dockerd[12306]: time="2022-11-24T15:22:35.028387861+01:00" level=info msg="Docker daemon" commit=3056208 graphdriver(s)=over>
Nov 24 15:22:35 docker.tp4.linux dockerd[12306]: time="2022-11-24T15:22:35.028510619+01:00" level=info msg="Daemon has completed initialization"
Nov 24 15:22:35 docker.tp4.linux systemd[1]: Started Docker Application Container Engine.
Nov 24 15:22:35 docker.tp4.linux dockerd[12306]: time="2022-11-24T15:22:35.095934833+01:00" level=info msg="API listen on /run/docker.sock"
```
- add user to docker group
```bash
[paja444@docker ~]$ sudo usermod -aG docker paja444
[paja444@docker ~]$ sudo groups paja444
paja444 : paja444 wheel docker
```
```bash
[paja444@docker ~]$ exit
```
### 2. Vérifier l'install

```bash
[paja444@docker ~]$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Docker Buildx (Docker Inc., v0.9.1-docker)
  compose: Docker Compose (Docker Inc., v2.12.2)
  scan: Docker Scan (Docker Inc., v0.21.0)
```

### 3. Lancement de conteneurs

- create host directory for share to container

```bash
[paja444@docker ~]$ sudo mkdir /srv/www
```
```bash
[paja444@docker ~]$ sudo mkdir /srv/www/tp4
```

- create html file
```bash
[paja444@docker ~]$ sudo nano /srv/www/tp4/index.html
```
```html
<!doctype html>
<html>

<head>
    <title>Welcome tp4.linux !</title>
</head>

<body>
    <p>This is an example paragraph. Anything in the <strong>body</strong> tag will appear on the page, just like this
        <strong>p</strong> tag and its contents.
    </p>
</body>

</html>
```

- create .conf file

```bash
[paja444@docker ~]$ sudo nano /srv/www/tp4/myconf.conf
```
```bash
server {
  # on définit le port où NGINX écoute dans le conteneur
  listen 9999;
  
  # on définit le chemin vers la racine web
  # dans ce dossier doit se trouver un fichier index.html
  root /var/www/tp4; 
}
```

- run nginx in container

```bash
[paja444@docker ~]$ docker run --name web -d -v /srv/www/tp4/index.html:/var/www/tp4/index.html -v /srv/www/tp4/myconf.conf:/etc/nginx/conf.d/myconf.conf --memory="1g" --cpus="1.0" -p 8888:9999 nginx
```

```bash
[paja444@docker ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED              STATUS              PORTS                                               NAMES
a981443f46e7   nginx     "/docker-entrypoint.…"   About a minute ago   Up About a minute   80/tcp, 0.0.0.0:8888->9999/tcp, :::8888->9999/tcp   web
```

```bash
(base) paja@paja-mac ~ % curl 10.104.1.11:8888
<!doctype html>
<html>

<head>
    <title>Welcome tp4.linux !</title>
</head>

<body>
    <p>This is an example paragraph. Anything in the <strong>body</strong> tag will appear on the page, just like this
        <strong>p</strong> tag and its contents.
    </p>
</body>

</html>
```

## II. Images

- in /srv/www/tp4 add custom index.html
- in /srv/www/tp4 add custom apache conf file
```bash
[paja444@docker tp4]$ sudo nano /srv/www/tp4/myconf.conf 
```
```bash
# on définit un port sur lequel écouter
Listen 80

# on charge certains modules Apache strictement nécessaires à son bon fonctionnement
LoadModule mpm_event_module "/usr/lib/apache2/modules/mod_mpm_event.so"
LoadModule dir_module "/usr/lib/apache2/modules/mod_dir.so"
LoadModule authz_core_module "/usr/lib/apache2/modules/mod_authz_core.so"

# on indique le nom du fichier HTML à charger par défaut
DirectoryIndex index.html
# on indique le chemin où se trouve notre site
DocumentRoot "/var/www/html/"

# quelques paramètres pour les logs
ErrorLog "logs/error.log"
LogLevel warn
```
- add Dockerfile
```bash
[paja444@docker tp4]$ sudo nano Dockerfile
```
```Dockerfile
# use ubuntu
FROM ubuntu:16.04
# run apt-get update
RUN apt-get update -y
# install apache2
RUN apt-get install apache2 -y
# add customed index.html
ADD index.html /var/www/html/index.html
# add config file
ADD myconf.conf /etc/apache2/apache2.conf

# run apache2
CMD ["apache2", "-D", "FOREGROUND"]
```

- Build Dockerfile
```bash
[paja444@docker tp4]$ docker build -t apache_image .
```
```bash
[paja444@docker tp4]$ docker build -t apache_image .
Sending build context to Docker daemon  4.608kB
Step 1/9 : FROM ubuntu
 ---> a8780b506fa4
Step 2/9 : RUN apt-get update
[...]
Step 5/9 : RUN apt-get clean
 ---> Running in 0c5adb55de8c
Removing intermediate container 0c5adb55de8c
 ---> 5ba4fcfc2c49
Step 6/9 : RUN mkdir /etc/apache2/logs/
 ---> Running in e286db8352ad
Removing intermediate container e286db8352ad
 ---> d216bce0e51b
Step 7/9 : COPY index.html /var/www/html/index.html
 ---> cb092982ed4a
Step 8/9 : COPY myconf.conf /etc/apache2/apache2.conf
 ---> 63db64774f89
Step 9/9 : CMD ["apache2ctl", "-D", "FOREGROUND"]
 ---> Running in f19478a00852
Removing intermediate container f19478a00852
 ---> db763a16a124
Successfully built db763a16a124
Successfully tagged apache_image:latest
```
- check 
```bash
[paja444@docker tp4]$ docker images
REPOSITORY     TAG       IMAGE ID       CREATED          SIZE
apache_image   latest    db763a16a124   26 seconds ago   225MB
nginx          latest    88736fe82739   10 days ago      142MB
ubuntu         latest    a8780b506fa4   3 weeks ago      77.8MB
```

- run container with apache_image

```bash
[paja444@docker tp4]$ docker run --name apache_docker_1 -d -p 80:80 apache_image
a0964880f4f7d6d2ae6eb1195ad545b0b1fa9e1a37b4bfe12878c71b6d07dc0e
```
- check

```bash
[paja444@docker tp4]$ docker ps
CONTAINER ID   IMAGE          COMMAND                  CREATED         STATUS         PORTS                               NAMES
a0964880f4f7   apache_image   "apache2ctl -D FOREG…"   4 seconds ago   Up 2 seconds   0.0.0.0:80->80/tcp, :::80->80/tcp   apache_docker_1
```

- test

```bash
(base) paja@paja-mac ~ % curl 10.104.1.11:80
<!doctype html>
<html>

<head>
    <title>Welcome tp4.linux !</title>
</head>

<body>
    <p>This is an example paragraph. Anything in the <strong>body</strong> tag will appear on the page, just like this
        <strong>p</strong> tag and its contents.
    </p>
</body>

</html>
```

## 2. Make your own meow

- download tcp pong server

```bash
[paja444@docker]$ sudo dnf install git
```
```bash
[paja444@docker]$ git clone https://gitlab.com/apajak/tp_leo.git
```

- make Dockerfile

```bash
[paja444@docker ~]$ vi tp_leo/linux/4/app/Dockerfile
```
```bash
FROM python:3.7.6
ADD src /
CMD [ "python3", "simpleTCP_server.py" ]
```
- make compose file

```bash
[paja444@docker ~]$ vi tp_leo/linux/4/app/docker-compose.yml 
```
```bash
version: '3'
services:
  udp:
    image: tcp_pong
    container_name: tcp_pong_1
    ports:
      - '5000:5000'
    build:
      context: /home/paja444/tp_leo/linux/4/app/Dockerfile
      dockerfile: Dockerfile
```

- create tcp_pong image

```bash
cd /home/paja444/tp_leo/linux/4/app/
```
```bash
[paja444@docker ~]$ docker build -t tcp_pong .
```
- compose up 
```bash
[paja444@docker app]$ docker compose up 
[+] Running 1/0
 ⠿ Container tcp_pong_1  Created                                                                                                                         0.0s
Attaching to tcp_pong_1
```

- test (run simpleTCP_client.py from another device on network)

```bash
(base) paja@paja-mac src % python3 simpleTCP_client.py
```
```bash
connecting to 10.104.1.11 port 5000
Enter message to send: hello Docker !!!
sending hello Docker !!!
received b'hello Docker !!!'
closing socket
(base) paja@paja-mac src % 
```

```bash
[paja444@docker app]$ docker compose up 
[+] Running 1/0
 ⠿ Container tcp_pong_1  Created                                                                                                                         0.0s
Attaching to tcp_pong_1
tcp_pong_1  | starting up on 0.0.0.0 port 5000
tcp_pong_1  | waiting for a connection
tcp_pong_1  | connection from ('10.104.1.1', 53997)
tcp_pong_1  | received b'hello Docker !!!'
tcp_pong_1  | sending data back to the client
tcp_pong_1  | received b''
tcp_pong_1  | no data from ('10.104.1.1', 53997)
tcp_pong_1 exited with code 0
[paja444@docker app]$ 
```

## dev/prod feature

- dev Docker file

```bash
[paja444@docker app]$ vi Dockerfile_dev
```
```bash
FROM python:3.7.6

WORKDIR /src

CMD [ "python3", "simpleTCP_server.py" ]
```

- dev image 

```bash
[paja444@docker app]$ docker build -f Dockerfile_dev -t tcp_pong_dev .
Sending build context to Docker daemon  11.78kB
Step 1/2 : FROM python:3.7.6
 ---> f66befd33669
Step 2/2 : CMD [ "python3", "simpleTCP_server.py" ]
 ---> Running in 446326495aec
Removing intermediate container 446326495aec
 ---> 30396713b701
Successfully built 30396713b701
Successfully tagged tcp_pong_dev:latest
```

- prod Docker file 

```bash
[paja444@docker app]$ vi Dockerfile_prod
```
```bash
FROM python:3.7.6
ADD src /
CMD [ "python3", "simpleTCP_server.py" ]
```

- prod image

```bash
[paja444@docker app]$ docker build -f Dockerfile_prod -t tcp_pong_prod .
Sending build context to Docker daemon  11.78kB
Step 1/3 : FROM python:3.7.6
 ---> f66befd33669
Step 2/3 : ADD src /
 ---> e4904608c254
Step 3/3 : CMD [ "python3", "simpleTCP_server.py" ]
 ---> Running in ae9ac17dc096
Removing intermediate container ae9ac17dc096
 ---> c0c35282a853
Successfully built c0c35282a853
Successfully tagged tcp_pong_prod:latest
```
- check 

```bash
[paja444@docker app]$ docker images
REPOSITORY      TAG       IMAGE ID       CREATED          SIZE
tcp_pong_prod   latest    c0c35282a853   32 seconds ago   919MB
tcp_pong_dev    latest    30396713b701   7 minutes ago    919MB
apache_image    latest    db763a16a124   3 hours ago      225MB
nginx           latest    88736fe82739   10 days ago      142MB
ubuntu          latest    a8780b506fa4   3 weeks ago      77.8MB
python          3.7.6     f66befd33669   2 years ago      919MB
```

- dev Compose file 

```bash
[paja444@docker app]$ vi docker-compose_dev.yml
```
```bash
version: '3'
services:
  udp:
    image: tcp_pong_dev
    container_name: tcp_pong_dev
    ports:
      - '5000:5000'
    volumes:
      - /home/paja444/tp_leo/linux/4/app/src:/src
    build:
      context: .
      dockerfile: Dockerfile_dev
```

```bash
[paja444@docker app]$ vi docker-compose_prod.yml
```
```bash
version: '3'
services:
  udp:
    image: tcp_pong_prod
    container_name: tcp_pong_prod
    ports:
      - '5000:5000'
    build:
      context: .
      dockerfile: Dockerfile_prod
```

- compose up dev 

```bash
[paja444@docker app]$ docker compose -f  docker-compose_dev.yml up --build
```

- compose up prod

```bash
[paja444@docker app]$ docker compose -f  docker-compose_prod.yml up --build
```
 
