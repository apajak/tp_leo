# TP2 : Gestion de service

Dans ce TP on va s'orienter sur l'**utilisation des systèmes GNU/Linux** comme un outil pour **faire tourner des services**. C'est le principal travail de l'administrateur système : fournir des services.

Ces services, on fait toujours la même chose avec :

- **installation** (opération ponctuelle)
- **configuration** (opération ponctuelle)
- **maintien en condition opérationnelle** (opération continue, tant que le service est actif)
- **renforcement niveau sécurité** (opération ponctuelle et continue : on conf robuste et on se tient à jour)

**Dans cette première partie, on va voir la partie installation et configuration.** Peu importe l'outil visé, de la base de données au serveur cache, en passant par le serveur web, le serveur mail, le serveur DNS, ou le serveur privé de ton meilleur jeu en ligne, c'est toujours pareil : install into conf.

On abordera la sécurité et le maintien en condition opérationelle dans une deuxième partie.

**On va apprendre à maîtriser un peu ces étapes, et pas simplement suivre la doc.**

On va maîtriser le service fourni :

- manipulation du service avec systemd
- quelle IP et quel port il utilise
- quels utilisateurs du système sont mobilisés
- quels processus sont actifs sur la machine pour que le service soit actif
- gestion des fichiers qui concernent le service et des permissions associées
- gestion avancée de la configuration du service

---

Bon le service qu'on va setup c'est NextCloud. **JE SAIS** ça fait redite avec l'an dernier, me tapez pas. ME TAPEZ PAS.  

Mais vous inquiétez pas, on va pousser le truc, on va faire évoluer l'install, l'architecture de la solution. Cette première partie de TP, on réalise une install basique, simple, simple, basique, la version *vanilla* un peu. Ce que vous êtes censés commencer à maîtriser (un peu, faites moi plais).

Refaire une install guidée, ça permet de s'exercer à faire ça proprement dans un cadre, bien comprendre, et ça me fait un pont pour les B1C aussi :)

On va faire évoluer la solution dans la suite de ce TP.

# Sommaire

- [TP2 : Gestion de service](#tp2--gestion-de-service)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. Un premier serveur web](#i-un-premier-serveur-web)
  - [1. Installation](#1-installation)
  - [2. Avancer vers la maîtrise du service](#2-avancer-vers-la-maîtrise-du-service)
- [II. Une stack web plus avancée](#ii-une-stack-web-plus-avancée)
  - [1. Intro blabla](#1-intro-blabla)
  - [2. Setup](#2-setup)
    - [A. Base de données](#a-base-de-données)
    - [B. Serveur Web et NextCloud](#b-serveur-web-et-nextcloud)
    - [C. Finaliser l'installation de NextCloud](#c-finaliser-linstallation-de-nextcloud)

# 0. Prérequis

➜ Machines Rocky Linux

➜ Un unique host-only côté VBox, ça suffira. **L'adresse du réseau host-only sera `10.102.1.0/24`.**

➜ Chaque **création de machines** sera indiquée par **l'emoji 🖥️ suivi du nom de la machine**

➜ Si je veux **un fichier dans le rendu**, il y aura l'**emoji 📁 avec le nom du fichier voulu**. Le fichier devra être livré tel quel dans le dépôt git, ou dans le corps du rendu Markdown si c'est lisible et correctement formaté.

## Checklist

A chaque machine déployée, vous **DEVREZ** vérifier la 📝**checklist**📝 :

- [x] IP locale, statique ou dynamique
- [x] hostname défini
- [x] firewall actif, qui ne laisse passer que le strict nécessaire
- [x] SSH fonctionnel avec un échange de clé
- [x] accès Internet (une route par défaut, une carte NAT c'est très bien)
- [x] résolution de nom
- [x] SELinux désactivé (vérifiez avec `sestatus`, voir [mémo install VM tout en bas](https://gitlab.com/it4lik/b2-reseau-2022/-/blob/main/cours/memo/install_vm.md#4-pr%C3%A9parer-la-vm-au-clonage))

**Les éléments de la 📝checklist📝 sont STRICTEMENT OBLIGATOIRES à réaliser mais ne doivent PAS figurer dans le rendu.**

![Checklist](./pics/checklist_is_here.jpg)

# I. Un premier serveur web

## 1. Installation

🖥️ **VM web.tp2.linux**

| Machine         | IP            | Service     |
|-----------------|---------------|-------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web |

🌞 **Installer le serveur Apache**

- paquet `httpd`
```bash
[paja444@web ~]$ sudo dnf update
```
```bash
[paja444@web ~]dnf install httpd
```

- la conf se trouve dans `/etc/httpd/`
  - le fichier de conf principal est `/etc/httpd/conf/httpd.conf`
  - je vous conseille **vivement** de virer tous les commentaire du fichier, à défaut de les lire, vous y verrez plus clair
    - avec `vim` vous pouvez tout virer avec `:g/^ *#.*/d`

> Ce que j'entends au-dessus par "fichier de conf principal" c'est que c'est **LE SEUL** fichier de conf lu par Apache quand il démarre. C'est souvent comme ça : un service ne lit qu'un unique fichier de conf pour démarrer. Cherchez pas, on va toujours au plus simple. Un seul fichier, c'est simple.  
**En revanche** ce serait le bordel si on mettait toute la conf dans un seul fichier pour pas mal de services.  
Donc, le principe, c'est que ce "fichier de conf principal" définit généralement deux choses. D'une part la conf globale. D'autre part, il inclut d'autres fichiers de confs plus spécifiques.  
On a le meilleur des deux mondes : simplicité (un seul fichier lu au démarrage) et la propreté (éclater la conf dans plusieurs fichiers).

🌞 **Démarrer le service Apache**

- le service s'appelle `httpd` (raccourci pour `httpd.service` en réalité)
  - démarrez le

  ```bash
    [paja444@web ~]$ sudo systemctl start httpd
    [paja444@web ~]$ sudo systemctl status httpd
    ● httpd.service - The Apache HTTP Server
        Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor preset: disabled)
        Active: active (running) since Tue 2022-11-15 14:44:54 CET; 11s ago
        Docs: man:httpd.service(8)
    Main PID: 23195 (httpd)
        Status: "Total requests: 0; Idle/Busy workers 100/0;Requests/sec: 0; Bytes served/sec:   0 B/sec"
        Tasks: 213 (limit: 5907)
        Memory: 23.0M
            CPU: 98ms
        CGroup: /system.slice/httpd.service
                ├─23195 /usr/sbin/httpd -DFOREGROUND
                ├─23196 /usr/sbin/httpd -DFOREGROUND
                ├─23197 /usr/sbin/httpd -DFOREGROUND
                ├─23198 /usr/sbin/httpd -DFOREGROUND
                └─23199 /usr/sbin/httpd -DFOREGROUND

    Nov 15 14:44:54 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
    Nov 15 14:44:54 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
    Nov 15 14:44:54 web.tp2.linux httpd[23195]: Server configured, listening on: port 80
  ```
  - faites en sorte qu'Apache démarre automatique au démarrage de la machine
  
    ```bash
        [paja444@web ~]$ sudo systemctl enable httpd
        Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
    ```
  - ouvrez le port firewall nécessaire
    - utiliser une commande `ss` pour savoir sur quel port tourne actuellement Apache

    ```bash
[paja444@web ~]$ sudo ss -tunlp | grep httpd
tcp   LISTEN 0      511                *:80              *:*    users:(("httpd",pid=23199,fd=4),("httpd",pid=23198,fd=4),("httpd",pid=23197,fd=4),("httpd",pid=23195,fd=4))
    ```
    ```bash
[paja444@web ~]$ sudo sudo firewall-cmd --add-port=80/tcp --permanent
    ```
    - une portion du mémo commandes est dédiée à `ss`

**En cas de problème** (IN CASE OF FIIIIRE) vous pouvez check les logs d'Apache :
```bash
# Demander à systemd les logs relatifs au service httpd
$ sudo journalctl -xe -u httpd

# Consulter le fichier de logs d'erreur d'Apache
$ sudo cat /var/log/httpd/error_log

# Il existe aussi un fichier de log qui enregistre toutes les requêtes effectuées sur votre serveur
$ sudo cat /var/log/httpd/access_log
```

🌞 **TEST**

- vérifier que le service est démarré

```bash
[paja444@web ~]$ sudo systemctl is-active httpd
active
```
- vérifier qu'il est configuré pour démarrer automatiquement

```bash
[paja444@web ~]$ sudo systemctl is-enabled httpd
enabled
```
- vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement

```bash
[paja444@web ~]$ curl -silent localhost:80 -SL | head -n 10
HTTP/1.1 403 Forbidden
Date: Tue, 15 Nov 2022 13:57:11 GMT
Server: Apache/2.4.51 (Rocky Linux)
Last-Modified: Wed, 06 Jul 2022 02:37:36 GMT
ETag: "1dc4-5e319da143c00"
Accept-Ranges: bytes
Content-Length: 7620
Content-Type: text/html; charset=UTF-8

<!doctype html>
```
- vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur we`

```bash
(base) paja@paja-mac ~ % curl -silent 10.102.1.11:80 -SL | head -n 10
HTTP/1.1 403 Forbidden
Date: Tue, 15 Nov 2022 14:12:12 GMT
Server: Apache/2.4.51 (Rocky Linux)
Last-Modified: Wed, 06 Jul 2022 02:37:36 GMT
ETag: "1dc4-5e319da143c00"
Accept-Ranges: bytes
Content-Length: 7620
Content-Type: text/html; charset=UTF-8

<!doctype html>
```


## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**

Petit soucis de batterie mélé d'un oubli de commit font que j'ai refais le rapport de la partie 2. d'où les résultats d'install un peu nul.

- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache

```bash
[paja444@web ~]$ sudo systemctl cat httpd
# /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true
OOMPolicy=continue

[Install]
WantedBy=multi-user.target
```

🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

- mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé

```bash
[paja444@web ~]$ sudo cat /etc/httpd/conf/httpd.conf | grep User
# User/Group: The name (or #number) of the user/group to run httpd as.
User apache
    LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\"" combined
      LogFormat "%h %l %u %t \"%r\" %>s %b \"%{Referer}i\" \"%{User-Agent}i\" %I %O" combinedio
```
- utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf

```bash
[paja444@web ~]$ ps -ef | grep httpd
root         712       1  0 15:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       747     712  0 15:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       749     712  0 15:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       750     712  0 15:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       751     712  0 15:10 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
paja444     1115    1047  0 15:23 pts/0    00:00:00 grep --color=auto httpd
```
- la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
  - vérifiez avec un `ls -al` que tout son contenu est **accessible en lecture** à l'utilisateur mentionné dans le fichier de conf

```bash
[paja444@web ~]$ ls -al /usr/share/testpage/ | grep index.html
-rw-r--r--.  1 root root 7620 Jul  6 04:37 index.html
```
🌞 **Changer l'utilisateur utilisé par Apache**

- créez un nouvel utilisateur
  - pour les options de création, inspirez-vous de l'utilisateur Apache existant
    - le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine
    - servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut

    ```bash
[paja444@web ~]$ sudo useradd webAdmin -d /usr/share/httpd -s /sbin/nologin
[sudo] password for paja444: 
useradd: warning: the home directory /usr/share/httpd already exists.
useradd: Not copying any file from skel directory into it.
[paja444@web ~]$ sudo usermod -aG apache webAdmin
[paja444@web ~]$ sudo cat /etc/passwd | grep webAdmin
webAdmin:x:1001:1001::/usr/share/httpd:/sbin/nologin
    ```
- modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur

```bash
[paja444@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
````
```bash
# User/Group: The name (or #number) of the user/group to run httpd as.
# It is usually good practice to create a dedicated user and group for
# running httpd, as with most system services.
#
User webAdmin
Group apache
```
- redémarrez Apache

```bash
[paja444@web ~]$ sudo systemctl daemon-reload 
[paja444@web ~]$ sudo systemctl restart httpd
[paja444@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-15 15:43:02 CET; 5s ago
       Docs: man:httpd.service(8)
   Main PID: 1211 (httpd)
     Status: "Started, listening on: port 80"
      Tasks: 213 (limit: 5907)
     Memory: 22.7M
        CPU: 156ms
     CGroup: /system.slice/httpd.service
             ├─1211 /usr/sbin/httpd -DFOREGROUND
             ├─1212 /usr/sbin/httpd -DFOREGROUND
             ├─1213 /usr/sbin/httpd -DFOREGROUND
             ├─1214 /usr/sbin/httpd -DFOREGROUND
             └─1215 /usr/sbin/httpd -DFOREGROUND

Nov 15 15:43:02 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 15 15:43:02 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 15 15:43:02 web.tp2.linux httpd[1211]: Server configured, listening on: port 80
```
- utilisez une commande `ps` pour vérifier que le changement a pris effet

```bash
[paja444@web ~]$ ps -ef | grep httpd
root        1211       1  0 15:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webAdmin    1212    1211  0 15:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webAdmin    1213    1211  0 15:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webAdmin    1214    1211  0 15:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
webAdmin    1215    1211  0 15:43 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
paja444     1445    1047  0 15:48 pts/0    00:00:00 grep --color=auto httpd
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

- modifiez la configuration d'Apache pour lui demander d'écouter sur un autre port de votre choix

```bash
[paja444@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
```
```bash
# Change this to Listen on a specific IP address, but note that if
# httpd.service is enabled to run at boot time, the address may not be
# available when the service starts.  See the httpd.service(8) man
# page for more information.
#
#Listen 12.34.56.78:80
Listen 8888
```
- ouvrez ce nouveau port dans le firewall, et fermez l'ancien

```bash
[paja444@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[paja444@web ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
```
- redémarrez Apache

```bash
[paja444@web ~]$ sudo systemctl daemon-reload 
[paja444@web ~]$ sudo systemctl restart httpd
[paja444@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
     Loaded: loaded (/usr/lib/systemd/system/httpd.service; enabled; vendor preset: disabled)
     Active: active (running) since Tue 2022-11-15 15:51:47 CET; 7s ago
       Docs: man:httpd.service(8)
   Main PID: 1488 (httpd)
     Status: "Started, listening on: port 8888"
      Tasks: 213 (limit: 5907)
     Memory: 26.7M
        CPU: 434ms
     CGroup: /system.slice/httpd.service
             ├─1488 /usr/sbin/httpd -DFOREGROUND
             ├─1489 /usr/sbin/httpd -DFOREGROUND
             ├─1490 /usr/sbin/httpd -DFOREGROUND
             ├─1491 /usr/sbin/httpd -DFOREGROUND
             └─1492 /usr/sbin/httpd -DFOREGROUND

Nov 15 15:51:46 web.tp2.linux systemd[1]: Starting The Apache HTTP Server...
Nov 15 15:51:47 web.tp2.linux systemd[1]: Started The Apache HTTP Server.
Nov 15 15:51:47 web.tp2.linux httpd[1488]: Server configured, listening on: port 8888
```
- prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi

```bash
[paja444@web ~]$ sudo ss -tunlp | grep httpd
tcp   LISTEN 0      511                *:8888            *:*    users:(("httpd",pid=1492,fd=4),("httpd",pid=1491,fd=4),("httpd",pid=1490,fd=4),("httpd",pid=1488,fd=4))
```
- vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port

```bash
[paja444@web ~]$ curl -silent localhost:8888 -SL | head -n 10
HTTP/1.1 403 Forbidden
Date: Tue, 15 Nov 2022 14:53:44 GMT
Server: Apache/2.4.51 (Rocky Linux)
Last-Modified: Wed, 06 Jul 2022 02:37:36 GMT
ETag: "1dc4-5e319da143c00"
Accept-Ranges: bytes
Content-Length: 7620
Content-Type: text/html; charset=UTF-8

<!doctype html>
```
- vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port

```bash
(base) paja@paja-mac ~ % scp paja444@10.102.1.11:/etc/httpd/conf/httpd.conf /Users/paja/Downloads
paja444@10.102.1.11's password: 
httpd.conf                                                                                                                  100%   12KB   5.1MB/s   00:00    
(base) paja@paja-mac ~ % 
```
📁 **Fichier `/etc/httpd/conf/httpd.conf`**
[httpd.conf](./files/httpd.conf)

# II. Une stack web plus avancée

⚠⚠⚠ **Réinitialiser votre conf Apache avant de continuer** ⚠⚠⚠  
En particulier :

- reprendre le port par défaut
- reprendre l'utilisateur par défaut

## 1. Intro blabla

**Le serveur web `web.tp2.linux` sera le serveur qui accueillera les clients.** C'est sur son IP que les clients devront aller pour visiter le site web.  

**Le service de base de données `db.tp2.linux` sera uniquement accessible depuis `web.tp2.linux`.** Les clients ne pourront pas y accéder. Le serveur de base de données stocke les infos nécessaires au serveur web, pour le bon fonctionnement du site web.

---

Bon le but de ce TP est juste de s'exercer à faire tourner des services, un serveur + sa base de données, c'est un peu le cas d'école. J'ai pas envie d'aller deep dans la conf de l'un ou de l'autre avec vous pour le moment, on va se contenter d'une conf minimale.

Je vais pas vous demander de coder une application, et cette fois on se contentera pas d'un simple `index.html` tout moche et on va se mettre dans la peau de l'admin qui se retrouve avec une application à faire tourner. **On va faire tourner un [NextCloud](https://nextcloud.com/).**

En plus c'est utile comme truc : c'est un p'tit serveur pour héberger ses fichiers via une WebUI, style Google Drive. Mais on l'héberge nous-mêmes :)

---

Le flow va être le suivant :

➜ **on prépare d'abord la base de données**, avant de setup NextCloud

- comme ça il aura plus qu'à s'y connecter
- ce sera sur une nouvelle machine `db.tp2.linux`
- il faudra installer le service de base de données, puis lancer le service
- on pourra alors créer, au sein du service de base de données, le nécessaire pour NextCloud

➜ **ensuite on met en place NextCloud**

- on réutilise la machine précédente avec Apache déjà installé, ce sera toujours Apache qui accueillera les requêtes des clients
- mais plutôt que de retourner une bête page HTML, NextCloud traitera la requête
- NextCloud, c'est codé en PHP, il faudra donc **installer une version de PHP précise** sur la machine
- on va donc : install PHP, configurer Apache, récupérer un `.zip` de NextCloud, et l'extraire au bon endroit !

![NextCloud install](./pics/nc_install.png)

## 2. Setup

🖥️ **VM db.tp2.linux**

**N'oubliez pas de dérouler la [📝**checklist**📝](#checklist).**

| Machines        | IP            | Service                 |
|-----------------|---------------|-------------------------|
| `web.tp2.linux` | `10.102.1.11` | Serveur Web             |
| `db.tp2.linux`  | `10.102.1.12` | Serveur Base de Données |

### A. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

- déroulez [la doc d'install de Rocky](https://docs.rockylinux.org/guides/database/database_mariadb-server/)
- je veux dans le rendu **toutes** les commandes réalisées
```bash
[paja444@db ~]$ dnf install mariadb-server
```
```bash
[paja444@db ~]$ systemctl enable mariadb
```
```bash
[paja444@db ~]$ systemctl start mariadb
```
```bash
[paja444@db ~]$ mysql_secure_installation
[...]
(appuie sur entrée a chaque question pour choix par défaut) 
```
- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`
  - il sera nécessaire de l'ouvrir dans le firewall
  ```bash
[paja444@db ~]$ sudo ss -tunlp | grep mariadb
[sudo] password for paja444: 
tcp   LISTEN 0      80                 *:3306            *:*    users:(("mariadbd",pid=798,fd=19))
  ```
  ```bash
[paja444@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
[paja444@db ~]$ sudo firewall-cmd --reload
  ```


> La doc vous fait exécuter la commande `mysql_secure_installation` c'est un bon réflexe pour renforcer la base qui a une configuration un peu *chillax* à l'install.

🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :
  - connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root -p`
  - exécutez les commandes SQL suivantes :

```sql
-- Création d'un utilisateur dans la base, avec un mot de passe
-- L'adresse IP correspond à l'adresse IP depuis laquelle viendra les connexions. Cela permet de restreindre les IPs autorisées à se connecter.
-- Dans notre cas, c'est l'IP de web.tp2.linux
-- "pewpewpew" c'est le mot de passe hehe
CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';

-- Création de la base de donnée qui sera utilisée par NextCloud
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

-- On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';

-- Actualisation des privilèges
FLUSH PRIVILEGES;

-- C'est assez générique comme opération, on crée une base, on crée un user, on donne les droits au user sur la base
```

  ```bash
[paja444@db ~]$ sudo mysql -u root -p
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.102.1.11';
MariaDB [(none)]> FLUSH PRIVILEGES;
  ```

> Par défaut, vous avez le droit de vous connecter localement à la base si vous êtes `root`. C'est pour ça que `sudo mysql -u root` fonctionne, sans nous demander de mot de passe. Evidemment, n'importe quelles autres conditions ne permettent pas une connexion aussi facile à la base.

🌞 **Exploration de la base de données**

- afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
  - depuis la machine `web.tp2.linux` vers l'IP de `db.tp2.linux`
  - utilisez la commande `mysql` pour vous connecter à une base de données depuis la ligne de commande
    - par exemple `mysql -u <USER> -h <IP_DATABASE> -p`
    - si vous ne l'avez pas, installez-là
    - vous pouvez déterminer dans quel paquet est disponible la commande `mysql` en saisissant `dnf provides mysql`
- **donc vous devez effectuer une commande `mysql` sur `web.tp2.linux`**
- une fois connecté à la base, utilisez les commandes SQL fournies ci-dessous pour explorer la base

```sql
SHOW DATABASES;
USE <DATABASE_NAME>;
SHOW TABLES;
```
```bash
[paja444@web ~]$ mysql -u nextcloud -h 10.102.1.12 -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 4
Server version: 5.5.5-10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;
Database changed
mysql> SHOW TABLES;
Empty set (0.00 sec)

mysql> USE information_shema;
ERROR 1044 (42000): Access denied for user 'nextcloud'@'10.102.1.11' to database 'information_shema'
mysql> exit
Bye
```

🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**

- vous ne pourrez pas utiliser l'utilisateur `nextcloud` de la base pour effectuer cette opération : il n'a pas les droits
- il faudra donc vous reconnectez localement à la base en utilisant l'utilisateur `root`

```bash
[paja444@db ~]$ sudo mysql -u root -p
[sudo] password for paja444: 
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 3
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.102.1.11' IDENTIFIED BY 'pewpewpew';
ERROR 1396 (HY000): Operation CREATE USER failed for 'nextcloud'@'10.102.1.11'
MariaDB [(none)]> exit
Bye
[paja444@db ~]$ sudo mysql -u root -p
[sudo] password for paja444: 
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 5
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
+--------------------+
4 rows in set (0.004 sec)

MariaDB [(none)]> use mysql;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [mysql]> show tables;
+---------------------------+
| Tables_in_mysql           |
+---------------------------+
| column_stats              |
| columns_priv              |
| db                        |
| event                     |
| func                      |
| general_log               |
| global_priv               |
| gtid_slave_pos            |
| help_category             |
| help_keyword              |
| help_relation             |
| help_topic                |
| index_stats               |
| innodb_index_stats        |
| innodb_table_stats        |
| plugin                    |
| proc                      |
| procs_priv                |
| proxies_priv              |
| roles_mapping             |
| servers                   |
| slow_log                  |
| table_stats               |
| tables_priv               |
| time_zone                 |
| time_zone_leap_second     |
| time_zone_name            |
| time_zone_transition      |
| time_zone_transition_type |
| transaction_registry      |
| user                      |
+---------------------------+
31 rows in set (0.000 sec)

MariaDB [mysql]> DESCRIBE user;
+------------------------+---------------------+------+-----+----------+-------+
| Field                  | Type                | Null | Key | Default  | Extra |
+------------------------+---------------------+------+-----+----------+-------+
| Host                   | char(60)            | NO   |     |          |       |
| User                   | char(80)            | NO   |     |          |       |
| Password               | longtext            | YES  |     | NULL     |       |
| Select_priv            | varchar(1)          | YES  |     | NULL     |       |
| Insert_priv            | varchar(1)          | YES  |     | NULL     |       |
| Update_priv            | varchar(1)          | YES  |     | NULL     |       |
| Delete_priv            | varchar(1)          | YES  |     | NULL     |       |
| Create_priv            | varchar(1)          | YES  |     | NULL     |       |
| Drop_priv              | varchar(1)          | YES  |     | NULL     |       |
| Reload_priv            | varchar(1)          | YES  |     | NULL     |       |
| Shutdown_priv          | varchar(1)          | YES  |     | NULL     |       |
| Process_priv           | varchar(1)          | YES  |     | NULL     |       |
| File_priv              | varchar(1)          | YES  |     | NULL     |       |
| Grant_priv             | varchar(1)          | YES  |     | NULL     |       |
| References_priv        | varchar(1)          | YES  |     | NULL     |       |
| Index_priv             | varchar(1)          | YES  |     | NULL     |       |
| Alter_priv             | varchar(1)          | YES  |     | NULL     |       |
| Show_db_priv           | varchar(1)          | YES  |     | NULL     |       |
| Super_priv             | varchar(1)          | YES  |     | NULL     |       |
| Create_tmp_table_priv  | varchar(1)          | YES  |     | NULL     |       |
| Lock_tables_priv       | varchar(1)          | YES  |     | NULL     |       |
| Execute_priv           | varchar(1)          | YES  |     | NULL     |       |
| Repl_slave_priv        | varchar(1)          | YES  |     | NULL     |       |
| Repl_client_priv       | varchar(1)          | YES  |     | NULL     |       |
| Create_view_priv       | varchar(1)          | YES  |     | NULL     |       |
| Show_view_priv         | varchar(1)          | YES  |     | NULL     |       |
| Create_routine_priv    | varchar(1)          | YES  |     | NULL     |       |
| Alter_routine_priv     | varchar(1)          | YES  |     | NULL     |       |
| Create_user_priv       | varchar(1)          | YES  |     | NULL     |       |
| Event_priv             | varchar(1)          | YES  |     | NULL     |       |
| Trigger_priv           | varchar(1)          | YES  |     | NULL     |       |
| Create_tablespace_priv | varchar(1)          | YES  |     | NULL     |       |
| Delete_history_priv    | varchar(1)          | YES  |     | NULL     |       |
| ssl_type               | varchar(9)          | YES  |     | NULL     |       |
| ssl_cipher             | longtext            | NO   |     |          |       |
| x509_issuer            | longtext            | NO   |     |          |       |
| x509_subject           | longtext            | NO   |     |          |       |
| max_questions          | bigint(20) unsigned | NO   |     | 0        |       |
| max_updates            | bigint(20) unsigned | NO   |     | 0        |       |
| max_connections        | bigint(20) unsigned | NO   |     | 0        |       |
| max_user_connections   | bigint(21)          | NO   |     | 0        |       |
| plugin                 | longtext            | NO   |     |          |       |
| authentication_string  | longtext            | NO   |     |          |       |
| password_expired       | varchar(1)          | NO   |     |          |       |
| is_role                | varchar(1)          | YES  |     | NULL     |       |
| default_role           | longtext            | NO   |     |          |       |
| max_statement_time     | decimal(12,6)       | NO   |     | 0.000000 |       |
+------------------------+---------------------+------+-----+----------+-------+
47 rows in set (0.002 sec)

MariaDB [mysql]> select Host,User,Password from User;
ERROR 1146 (42S02): Table 'mysql.User' doesn't exist
MariaDB [mysql]> select Host,User,Password from user;
+-------------+-------------+-------------------------------------------+
| Host        | User        | Password                                  |
+-------------+-------------+-------------------------------------------+
| localhost   | mariadb.sys |                                           |
| localhost   | root        | invalid                                   |
| localhost   | mysql       | invalid                                   |
| 10.102.1.11 | nextcloud   | *AF136CF35F0D546F69717A7F18C18849666E64D0 |
+-------------+-------------+-------------------------------------------+
4 rows in set (0.001 sec)
MariaDB [mysql]> exit
Bye
```
> Les utilisateurs de la base de données sont différents des utilisateurs du système Rocky Linux qui porte la base. Les utilisateurs de la base définissent des identifiants utilisés pour se connecter à la base afin d'y voir ou d'y modifier des données.

Une fois qu'on s'est assurés qu'on peut se co au service de base de données depuis `web.tp2.linux`, on peut continuer.

### B. Serveur Web et NextCloud

⚠️⚠️⚠️ **N'OUBLIEZ PAS de réinitialiser votre conf Apache avant de continuer. En particulier, remettez le port et le user par défaut.**

🌞 **Install de PHP**

```bash
# On ajoute le dépôt CRB
$ sudo dnf config-manager --set-enabled crb
# On ajoute le dépôt REMI
$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y

# On liste les versions de PHP dispos, au passage on va pouvoir accepter les clés du dépôt REMI
$ dnf module list php

# On active le dépôt REMI pour récupérer une version spécifique de PHP, celle recommandée par la doc de NextCloud
$ sudo dnf module enable php:remi-8.1 -y

# Eeeet enfin, on installe la bonne version de PHP : 8.1
$ sudo dnf install -y php81-php
```
```bash
[paja444@web ~]$ sudo dnf config-manager --set-enabled crb
[sudo] password for paja444: 
[paja444@web ~]$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y
Rocky Linux 9 - BaseOS                                              2.6 kB/s | 3.6 kB     00:01    
Rocky Linux 9 - AppStream                                           2.8 kB/s | 3.6 kB     00:01    
Rocky Linux 9 - CRB                                                 5.5 kB/s | 3.6 kB     00:00    
Rocky Linux 9 - Extras                                              4.7 kB/s | 2.9 kB     00:00    
remi-release-9.rpm                                                  114 kB/s |  25 kB     00:00    
Package remi-release-9.0-6.el9.remi.noarch is already installed.
Package yum-utils-4.0.24-4.el9_0.noarch is already installed.
Dependencies resolved.
Nothing to do.
Complete!
[paja444@web ~]$ dnf module list php
Rocky Linux 9 - BaseOS                                              4.3 kB/s | 3.6 kB     00:00    
Rocky Linux 9 - AppStream                                           2.8 kB/s | 3.6 kB     00:01    
Rocky Linux 9 - CRB                                                 4.6 kB/s | 3.6 kB     00:00    
Rocky Linux 9 - Extras                                              4.6 kB/s | 2.9 kB     00:00    
Remi's Modular repository for Enterprise Linux 9 - x86_64
Name          Stream               Profiles                           Summary                       
php           remi-7.4             common [d], devel, minimal         PHP scripting language        
php           remi-8.0             common [d], devel, minimal         PHP scripting language        
php           remi-8.1 [e]         common [d], devel, minimal         PHP scripting language        
php           remi-8.2             common [d], devel, minimal         PHP scripting language        

Hint: [d]efault, [e]nabled, [x]disabled, [i]nstalled
[paja444@web ~]$ sudo dnf module enable php:remi-8.1 -y
Last metadata expiration check: 0:01:30 ago on Thu 17 Nov 2022 11:00:10 AM CET.
Dependencies resolved.
Nothing to do.
Complete!
[paja444@web ~]$ sudo dnf install -y php81-php
Last metadata expiration check: 0:01:47 ago on Thu 17 Nov 2022 11:00:10 AM CET.
Package php81-php-8.1.12-1.el9.remi.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```
🌞 **Install de tous les modules PHP nécessaires pour NextCloud**

```bash
# eeeeet euuuh boom. Là non plus j'ai pas pondu ça, c'est la doc :
$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
```
```bash
[paja444@web ~]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
Last metadata expiration check: 0:03:56 ago on Thu 17 Nov 2022 11:00:10 AM CET.
Package libxml2-2.9.13-1.el9_0.1.x86_64 is already installed.
Package openssl-1:3.0.1-43.el9_0.x86_64 is already installed.
Package php81-php-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-gd-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-mbstring-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-process-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-xml-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-pecl-zip-1.21.1-1.el9.remi.x86_64 is already installed.
Package php81-php-common-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-pdo-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-mysqlnd-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-intl-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-bcmath-8.1.12-1.el9.remi.x86_64 is already installed.
Package php81-php-gmp-8.1.12-1.el9.remi.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
```

🌞 **Récupérer NextCloud**

- créez le dossier `/var/www/tp2_nextcloud/`
  - ce sera notre *racine web* (ou *webroot*)
  - l'endroit où le site est stocké quoi, on y trouvera un `index.html` et un tas d'autres marde, tout ce qui constitue NextClo :D
  ```bash
[paja444@web ~]$ sudo mkdir /var/www/tp2_nextcloud/
mkdir: cannot create directory ‘/var/www/tp2_nextcloud/’: File exists
  ```
- récupérer le fichier suivant avec une commande `curl` ou `wget` : https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip

```bash
[paja444@web www]$ sudo curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip-o /var/www/tp2_nextcloud -s
```
- extrayez tout son contenu dans le dossier `/var/www/tp2_nextcloud/` en utilisant la commande `unzip`
  - installez la commande `unzip` si nécessaire
  
  ```bash
[paja444@web tp2_nextcloud]$ dnf provides unzip
Last metadata expiration check: 0:18:42 ago on Thu 17 Nov 2022 11:00:38 AM CET.
unzip-6.0-56.el9.x86_64 : A utility for unpacking zip files
Repo        : @System
Matched from:
Provide    : unzip = 6.0-56.el9

unzip-6.0-56.el9.x86_64 : A utility for unpacking zip files
Repo        : baseos
Matched from:
Provide    : unzip = 6.0-56.el9

[paja444@web tp2_nextcloud]$ sudo dnf install unzip
Last metadata expiration check: 0:19:31 ago on Thu 17 Nov 2022 11:00:10 AM CET.
Package unzip-6.0-56.el9.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
  ```
  - vous pouvez extraire puis déplacer ensuite, vous prenez pas la tête

  ```bash
[paja444@web tp2_nextcloud]$ unzip nextcloud-25.0.0rc3.zip
  ```
  - contrôlez que le fichier `/var/www/tp2_nextcloud/index.html` existe pour vérifier que tout est en place

  ```bash
[paja444@web ~]$ sudo chown -R apache /var/www/tp2_nextcloud/
[paja444@web ~]$ chgrp -R apache /var/www/tp2_nextcloud

  ```
- assurez-vous que le dossier `/var/www/tp2_nextcloud/` et tout son contenu appartient à l'utilisateur qui exécute le service Apache

```bash
[paja444@web ~]$ cd /var/www/tp2_nextcloud/
[paja444@web tp2_nextcloud]$ ls -al
total 140
drwxr-xr-x. 15 apache apache  4096 Nov 15 18:06 .
drwxr-xr-x.  5 root   root      54 Nov 15 17:49 ..
drwxr-xr-x. 47 apache apache  4096 Nov 15 17:49 3rdparty
drwxr-xr-x. 50 apache apache  4096 Nov 15 17:49 apps
-rw-r--r--.  1 apache apache 19327 Nov 15 17:49 AUTHORS
drwxr-xr-x.  2 apache apache    85 Nov 15 18:06 config
-rw-r--r--.  1 apache apache  4095 Nov 15 17:49 console.php
-rw-r--r--.  1 apache apache 34520 Nov 15 17:49 COPYING
drwxr-xr-x. 23 apache apache  4096 Nov 15 17:49 core
-rw-r--r--.  1 apache apache  6317 Nov 15 17:49 cron.php
drwxr-xr-x.  2 apache apache    62 Nov 15 18:06 data
drwxr-xr-x.  2 apache apache  8192 Nov 15 17:49 dist
-rw-r--r--.  1 apache apache  3253 Nov 15 17:49 .htaccess
-rw-r--r--.  1 apache apache   156 Nov 15 17:49 index.html
-rw-r--r--.  1 apache apache  3456 Nov 15 17:49 index.php
drwxr-xr-x.  6 apache apache   125 Nov 15 17:49 lib
-rw-r--r--.  1 apache apache   283 Nov 15 17:49 occ
drwxr-xr-x.  2 apache apache    23 Nov 15 17:49 ocm-provider
drwxr-xr-x.  2 apache apache    55 Nov 15 17:49 ocs
drwxr-xr-x.  2 apache apache    23 Nov 15 17:49 ocs-provider
-rw-r--r--.  1 apache apache  3139 Nov 15 17:49 public.php
-rw-r--r--.  1 apache apache  5426 Nov 15 17:49 remote.php
drwxr-xr-x.  4 apache apache   133 Nov 15 17:49 resources
-rw-r--r--.  1 apache apache    26 Nov 15 17:49 robots.txt
-rw-r--r--.  1 apache apache  2452 Nov 15 17:49 status.php
drwxr-xr-x.  3 apache apache    35 Nov 15 17:49 themes
drwxr-xr-x.  2 apache apache    43 Nov 15 17:49 updater
-rw-r--r--.  1 apache apache   101 Nov 15 17:49 .user.ini
-rw-r--r--.  1 apache apache   387 Nov 15 17:49 version.php
```

> A chaque fois que vous faites ce genre de trucs, assurez-vous que c'est bien ok. Par exemple, vérifiez avec un `ls -al` que tout appartient bien à l'utilisateur qui exécute Apache.

🌞 **Adapter la configuration d'Apache**

- regardez la dernière ligne du fichier de conf d'Apache pour constater qu'il existe une ligne qui inclut d'autres fichiers de conf
- créez en conséquence un fichier de configuration qui porte un nom clair et qui contient la configuration suivante :

```bash
[paja444@web conf.d]$ sudo nano /etc/httpd/conf.d/myConf.conf
```
```apache
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp2_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp2.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp2_nextcloud/> 
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

🌞 **Redémarrer le service Apache** pour qu'il prenne en compte le nouveau fichier de conf

```bash
[paja444@web ~]$ sudo systemctl restart httpd
```
### C. Finaliser l'installation de NextCloud

➜ **Sur votre PC**

- modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
  - pour pouvoir joindre l'IP de la VM en utilisant le nom `web.tp2.linux`

  ```bash
(base) paja@paja-mac ~ % cat /etc/hosts | grep 10.102.1.11
10.102.1.11	web.tp2.linux
  ```

- avec un navigateur, visitez NextCloud à l'URL `http://web.tp2.linux`
  - c'est possible grâce à la modification de votre fichier `hosts`
- on va vous demander un utilisateur et un mot de passe pour créer un compte admin
  - ne saisissez rien pour le moment
- cliquez sur "Storage & Database" juste en dessous
  - choisissez "MySQL/MariaDB"
  - saisissez les informations pour que NextCloud puisse se connecter avec votre base
- saisissez l'identifiant et le mot de passe admin que vous voulez, et validez l'installation

🌴 **C'est chez vous ici**, baladez vous un peu sur l'interface de NextCloud, faites le tour du propriétaire :)

🌞 **Exploration de la base de données**

- connectez vous en ligne de commande à la base de données après l'installation terminée
- déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
  - ***bonus points*** si la réponse à cette question est automatiquement donnée par une requête SQL

```bash
[paja444@db ~]$ sudo mysql -u root -p
[sudo] password for paja444: 
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 127
Server version: 10.5.16-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> 
```
  ```sql
MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| nextcloud          |
| performance_schema |
+--------------------+
4 rows in set (0.000 sec)

MariaDB [(none)]> use nextcloud;
Reading table information for completion of table and column names
You can turn off this feature to get a quicker startup with -A

Database changed
MariaDB [nextcloud]> 
MariaDB [nextcloud]> SELECT count(*) AS TOTALNUMBEROFTABLES
    -> FROM INFORMATION_SCHEMA.TABLES
    -> WHERE TABLE_SCHEMA = 'nextcloud';
+---------------------+
| TOTALNUMBEROFTABLES |
+---------------------+
|                  95 |
+---------------------+
1 row in set (0.001 sec)
  ```
