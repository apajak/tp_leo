# Free TP
# Mattermost

## Sommaire:
- [Mattermost Install](#mattermost-install)
- [Mattermost Configuration](#mattermost-configuration)
- [GitLab intgration](#gitlab-intgration)
- [GitHub intgration](#github-intgration)
- [Zoom integration](#zoom-integration)
- [Syslog Server](#syslog-server)

## Equipe:
- Durand Erwan
- Pajak Alexandre

## Mattermost Install

### Download
```bash
paja444@Alpagame:~$ wget https://releases.mattermost.com/7.5.1/mattermost-7.5.1-linux-amd64.tar.gz
```
### Extract
```bash
paja444@Alpagame:~$ tar -xvzf mattermost-7.5.1-linux-amd64.tar.gz
```
```bash
paja444@Alpagame:~$ sudo mv mattermost /opt
```

### make storage directory 

```bash
paja444@Alpagame:~$ sudo mkdir /opt/mattermost/data
```

### add dedicated user

```bash
paja444@Alpagame:~$ sudo useradd --system --user-group mattermost
```

### set him as mattermost directory owner

```bash
paja444@Alpagame:~$ sudo chown -R mattermost:mattermost /opt/mattermost
```

### Give write permissions to the mattermost group

```bash
paja444@Alpagame:~$ sudo chmod -R g+w /opt/mattermost
```

## Install & config MariaDB

```bash
spnx@vps-cd719915:~$ sudo apt install mariadb-server

spnx@vps-cd719915:~$ sudo mysql_secure_installation
n
n
Y
Y
Y
spnx@vps-cd719915:~$ sudo mysql

MariaDB [(none)]> CREATE USER 'mmuser'@'127.0.0.1' IDENTIFIED BY '**********';

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS mattermost CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

MariaDB [(none)]> GRANT ALL PRIVILEGES ON mattermost .* TO 'mmuser'@'127.0.0.1';

MariaDB [(none)]> FLUSH PRIVILEGES;
```

#### test db-user

```bash
spnx@vps-cd719915:~$ mysql -u mmuser -h 127.0.0.1 -p
Enter password:
Your MariaDB connection id is 58
MariaDB [(none)]>
```
#### update les règles du firewall

```bash
spnx@vps-cd719915:~$ sudo ufw allow 3306
```

## Mattermost Configuration

### Set up the database driver

```bash
paja444@Alpagame:~$ sudo vim /opt/mattermost/config/config.json
```
```bash
paja444@Alpagame:~$ sudo cat /opt/mattermost/config/config.json | grep DataSource
        "DataSource": "mmuser:<**********>@tcp(127.0.0.1:3306)/mattermost?charset=utf8mb4,utf8&writeTimeout=30s"utf8\u0026writeTimeout=30s",
        "DataSourceReplicas": [],
        "DataSourceSearchReplicas": [],
```

### make a service

```bash
paja444@Alpagame:~$ sudo touch /lib/systemd/system/mattermost.service
```
```bash
paja444@Alpagame:~$ sudo vi /lib/systemd/system/mattermost.service
```
```bash
paja444@Alpagame:~$ cat /lib/systemd/system/mattermost.service
[Unit]
Description=Mattermost
After=network.target
[Service]
Type=notify
ExecStart=/opt/mattermost/bin/mattermost
TimeoutStartSec=3600
KillMode=mixed
Restart=always
RestartSec=10
WorkingDirectory=/opt/mattermost
User=mattermost
Group=mattermost
LimitNOFILE=49152
[Install]
WantedBy=multi-user.target
```

### Nginx revers proxy & https features

#### services
- stop mattermost.service
```bash
paja444@Alpagame:~$ sudo systemctl stop mattermost.service
```
- copy source directory
```bash
paja444@Alpagame:/opt$ sudo cp -r mattermost/ ./mattermost_1/
paja444@Alpagame:/opt$ sudo cp -r mattermost/ ./mattermost_2/
paja444@Alpagame:/opt$ sudo cp -r mattermost/ ./mattermost_3/
```
- make mattermost user as owner:

```bash
paja444@Alpagame:/opt$ sudo chown -R mattermost:mattermost /opt/mattermost_1
paja444@Alpagame:/opt$ sudo chown -R mattermost:mattermost /opt/mattermost_2
paja444@Alpagame:/opt$ sudo chown -R mattermost:mattermost /opt/mattermost_3
```

- Give write permissions to the mattermost group

```bash
paja444@Alpagame:/opt$ sudo chmod -R g+w /opt/mattermost_1
paja444@Alpagame:/opt$ sudo chmod -R g+w /opt/mattermost_2
paja444@Alpagame:/opt$ sudo chmod -R g+w /opt/mattermost_3
```
- first service:

```bash
paja444@Alpagame:/opt$ sudo vi /lib/systemd/system/mattermost_1.service
```
```bash
[Unit]
Description=Mattermost_1 service
After=network.target
[Service]
Type=notify
ExecStart=/opt/mattermost_1/bin/mattermost
TimeoutStartSec=3600
KillMode=mixed
Restart=always
RestartSec=10
WorkingDirectory=/opt/mattermost_1
User=mattermost
Group=mattermost
LimitNOFILE=49152
[Install]
WantedBy=multi-user.target
```
- second service:


```bash
paja444@Alpagame:/opt$ sudo vi /lib/systemd/system/mattermost_2.service
```
```bash
[Unit]
Description=Mattermost_2 service
After=network.target
[Service]
Type=notify
ExecStart=/opt/mattermost_2/bin/mattermost
TimeoutStartSec=3600
KillMode=mixed
Restart=always
RestartSec=10
WorkingDirectory=/opt/mattermost_2
User=mattermost
Group=mattermost
LimitNOFILE=49152
[Install]
WantedBy=multi-user.target
```

- third service:

```bash
paja444@Alpagame:/opt$ sudo vi /lib/systemd/system/mattermost_3.service
```
```bash
[Unit]
Description=Mattermost_3 service
After=network.target
[Service]
Type=notify
ExecStart=/opt/mattermost_3/bin/mattermost
TimeoutStartSec=3600
KillMode=mixed
Restart=always
RestartSec=10
WorkingDirectory=/opt/mattermost_3
User=mattermost
Group=mattermost
LimitNOFILE=49152
[Install]
WantedBy=multi-user.target
```
```bash
paja444@Alpagame:~$ sudo systemctl daemon-reload 
```
### change port on config.json

```bash
paja444@Alpagame:/opt$ sudo vi /opt/mattermost_2/config/config.json
```
```bash
paja444@Alpagame:/opt$ cat /opt/mattermost_1/config/config.json  | grep ListenAddress
cat: /opt/mattermost_1/config/config.json: Permission denied
paja444@Alpagame:/opt$ sudo !!
sudo cat /opt/mattermost_1/config/config.json  | grep ListenAddress
        "ListenAddress": "127.0.0.1:8065",
```
```bash
paja444@Alpagame:/opt$ sudo vi /opt/mattermost_3/config/config.json
```
```bash
paja444@Alpagame:/opt$ sudo cat /opt/mattermost_3/config/config.json | grep ListenAddress
        "ListenAddress": "127.0.0.1:8067",
```

#### start service

```bash
paja444@Alpagame:~$ sudo systemctl start mattermost_1.service
paja444@Alpagame:~$ sudo systemctl start mattermost_2.service 
paja444@Alpagame:~$ sudo systemctl start mattermost_3.service 
```
```bash
paja444@Alpagame:~$ sudo ss -tunlp | grep mattermost
tcp   LISTEN 0      4096       127.0.0.1:8065       0.0.0.0:*    users:(("mattermost",pid=44237,fd=22))                                         
tcp   LISTEN 0      4096       127.0.0.1:8066       0.0.0.0:*    users:(("mattermost",pid=44542,fd=31))                                         
tcp   LISTEN 0      4096       127.0.0.1:8067       0.0.0.0:*    users:(("mattermost",pid=44621,fd=25))  
```

#### Nginx revers proxy configuration

```bash
paja444@Alpagame:~$ sudo vi /etc/nginx/conf.d/mattermost.conf
```
```bash
# backend services list
upstream backend_mattermost{
  # The server to which a request is sent is determined from the client IP address
  ip_hash;
  server 127.0.0.1:8065;
  server 127.0.0.1:8066;
  server 127.0.0.1:8067;
}
# rate limitting as 10 request per second
limit_req_zone $binary_remote_addr zone=mylimit:10m rate=10r/s;

# http redirection
server {
    listen 80 default_server;
    server_name _;
    return 301 https://alpagam.com$request_uri;
}

# https
server {
  listen 443 ssl;
  ssl_certificate /etc/letsencrypt/live/alpagam.com-0001/fullchain.pem;
  ssl_certificate_key /etc/letsencrypt/live/alpagam.com-0001/privkey.pem;
  server_name www.alpagam.com alpagam.com;
      
      # Mattermost socket
      location ~ /api/v[0-9]+/(users/)?websocket$ {
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            client_max_body_size 50M;
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Frame-Options SAMEORIGIN;
            proxy_buffers 256 16k;
            proxy_buffer_size 16k;
            proxy_read_timeout 600s;
            proxy_http_version 1.1;
            proxy_pass http://backend_mattermost;
      }
      # only in login page apply request limitting
      location /login/ {
      limit_req zone=mylimit;
      proxy_pass http://backend_mattermost;
      }

      location / {
            client_max_body_size 50M;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header Host $http_host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header X-Frame-Options SAMEORIGIN;
            proxy_pass http://backend_mattermost;
      }
}
```
```bash
paja444@Alpagame:~$ sudo systemctl restart nginx.service
```

## GitLab intgration

### GitLab-Plugin

#### Mattermost configuration

- Sign in to your Mattermost instance
- Create a channel `GitLab` (for receive notifications)
- Enable incoming webhooks :

        - In Mattermost, go to System Console > Integrations > Integration Management

                - set `True` for Enable Incoming Webhooks

- Add incomming webhook :

        - In Mattermost, go to Product menu > Integrations > Incoming Webhooks
        - Add Incoming Webhook
        - File the fields:
                - Title: `GitLab`
                - Description: `GitLab Notifications  webhook``
                - Channel : `GitLab`
        - Save

- You will end up with a webhook endpoint that looks like so: `https://your-mattermost-server.com/hooks/xxx-generatedkey-xxx`
- Save it and copy the Webhook URL because we need this later for GitLab.
#### GitLab configuration

- On the top bar, select Main menu > Projects and find your project
- On the left sidebar, select Settings > Integrations
- Select Mattermost notifications
- Select the GitLab events to generate notifications for. For each event you select, input the Mattermost channel to receive the notification
- Save changes


### GitLab-Authentification

#### Add an OpenID Connect application to your GitLab account

- Go to [gitlab.com](https://gitlab.com/-/profile/applications)
- Add a new application

        - In the Name field, enter `Mattermos`
        - In the Redirect URI field :
                ```
                https://alpagam.com/login/gitlab/complete
                https://alpagam.com/signup/gitlab/complete
                ```
        - Select scopes: `read_user`
        - Select Save application.

- Log in to Mattermost, then go to System Console > Authentication > OpenID Connect.
- Select GitLab as the service provider.

        - Enable authentication with GitLab: `true`
        - Application ID: `your app ID gitlab`
        - Application Secret Key: `your Secret Key gitlab`
        - GitLab Site URL: `https://gitlab.com`
        - User API Endpoint: `https://gitlab.com/api/v4/user`
        - Auth Endpoint: `https://gitlab.com/oauth/authorize`
        - Tocken Endpoint: `https://gitlab.com/oauth/token`
        - update config.json on mattermost_1, mattermost_2, mattermost_3 
                ```bash
                paja444@Alpagame:~$ sudo vim /opt/mattermost_1/config/config.json
                ```
                ```bash
                "GitLabSettings": {
                        "Enable": true,
                        "Secret": "bdca231580c14128b9f5584144a51184e992bb2e85ec7c9c183bc16c76a69150",
                        "Id": "393b0396511b8dfd1cf6e710b11a82714bea693622ae68c5bbfe91051d0d6143",
                        "Scope": "read_user",
                        "AuthEndpoint": "https://gitlab.com/oauth/authorize",
                        "TokenEndpoint": "https://gitlab.com/oauth/token",
                        "UserAPIEndpoint": "https://gitlab.com/api/v4/user",
                        "DiscoveryEndpoint": "",
                        "ButtonText": "",
                        "ButtonColor": ""
                },
                ```
        - Restart all Mattermost services
        ```bash
        paja444@Alpagame:~$ sudo systemctl restart mattermost_1.service 
        ```
        ```bash
        paja444@Alpagame:~$ sudo systemctl restart mattermost_1.service 
        ```
        ```bash
        paja444@Alpagame:~$ sudo systemctl restart mattermost_1.service 
        ```

### Using GitLab Plugin

Once configuration is complete, run the /gitlab connect slash command from any channel within Mattermost to connect your Mattermost account with GitLab.

- Subscribe to a repository - Use `/gitlab subscriptions add <user/repo>`
- Get to do items - Use `/gitlab todo`
- Update settings - Use `/gitlab settings`
- And more! - Run `/gitlab help`


## GitHub intgration

- Sign in to your Mattermost instance
- Create a channel `GitHub` (for receive notifications)

### OAuth Application

- In GitHub, go to Settings > Developer settings > OAuth Apps > New OAuth Apps and Set the following values:

        - Application name: `Mattermost GitHub Plugin`
        - Homepage URL: `https://github.com/mattermost/mattermost-plugin-github`
        - Authorization callback URL: `https://alpagam.com/plugins/github/oauth/complet`
        - Submit

- Click Generate a new client secret and provide your GitHub password to continue
- Copy the Client ID and Client Secret in the resulting screen

### Create a Webhook in GitHub

- In Mattermost go to System Console > Plugins > GitHub

        - generate a new value for Webhook Secret. 
        - Copy it, as you will use it in a later step.
        - Save

- in GitHub Go to the Settings page of your GitHub organization you want to send notifications from, then select Webhooks in the sidebar

        - Click Add Webhook
        - Set the following values:

                - Payload URL `https://your-mattermost-url.com/plugins/github/webhook`
                - Content Type: `application/json`
                - Secret:`the webhook secret you copied previously`

        - Select Let me select individual events for "Which events would you like to trigger this webhook?".
        - Select the events you want: Branch or Tag creation, Branch or Tag deletion, Issue comments, Issues, Pull requests, Pull request review, Pull request review comments, Pushes, Stars.
        - Hit Add Webhook to save it.

### Using the Plugin

Once configuration is complete, run the /github connect slash command from any channel within Mattermost to connect your Mattermost account with GitHub.

- Subscribe to a repository - Use `/github subscriptions add <user/repo>`
- Get to do items - Use `/github todo`
- Update settings - Use `/github settings`
- And more! - Run `/github help`

## Zoom integration

### Mattermost configuration
- Go to System Console > Plugins > Zoom to configure the Zoom Plugin.

        - Set Enable Plugin to true
        - Set Enable OAuth to true
        - Use the Client ID and Client Secret generated during Zoom Configuration to fill in the fields Zoom OAuth Client ID and Zoom OAuth Client Secret.
        - Select Regenerate next to the field At Rest Token Encryption Key.
        - Make sure Enable Password based authentication is set to false.
        - Ignore API Key and API Secret fields.
        
### Zoom configuration
- go to https://marketplace.zoom.us
- In the top right select Develop and then Build App.

        - Select OAuth in Choose your app type section.

                - Enter a name for your app.
                - Choose Account-level app as the app type.
                - create

        - Go to the App Credentials tab on the left. Here you'll find your Client ID and Client Secret.

                - These will be needed during Mattermost Setup.
                - Redirect URL : `https://alpagam.com/plugins/zoom/oauth2/complete`

        - Select Scopes and add the following scopes:
                - `eeting:write`
                - `user:read`

## Syslog Server

### Configure Syslog Server

```bash
paja444@Alpagame:~$ apt list rsyslog -a
Listing... Done
rsyslog/bullseye-backports 8.2208.0-1~bpo11+1 amd64
rsyslog/stable,stable-security,now 8.2102.0-2+deb11u1 amd64 [installed]
```

- uncomment the lines below to configure the UDP and TCP protocols for log reception.
```bash
# provides UDP syslog reception
module(load="imudp")
input(type="imudp" port="514")

# provides TCP syslog reception
module(load="imtcp")
input(type="imtcp" port="514")
```
- create a new template that instructs the rsyslog server where to save incoming messages. Add the following after below TCP config.

```bash
sudo nano /etc/rsyslog.conf
```
```bash
# provides UDP syslog reception
module(load="imudp")
input(type="imudp" port="514")

# provides TCP syslog reception
module(load="imtcp")
input(type="imtcp" port="514")

$template Incoming-logs,"/var/log/%HOSTNAME%/%PROGRAMNAME%.log"
*.* ?Incoming-logs
```

- check if is running

```bash
paja444@Alpagame:~$ sudo systemctl restart rsyslog
paja444@Alpagame:~$ sudo ss -tunlp | grep 514
udp   UNCONN 0      0            0.0.0.0:514        0.0.0.0:*    users:(("rsyslogd",pid=82842,fd=6))                                               
udp   UNCONN 0      0               [::]:514           [::]:*    users:(("rsyslogd",pid=82842,fd=7))                                               
tcp   LISTEN 0      25           0.0.0.0:514        0.0.0.0:*    users:(("rsyslogd",pid=82842,fd=8))                                               
tcp   LISTEN 0      25              [::]:514           [::]:*    users:(("rsyslogd",pid=82842,fd=9)) 
```
- update firewall rules

```bash
paja444@Alpagame:~$ sudo ufw allow 514/tcp
WARN: uid is 0 but '/' is owned by 1001
Rule added
Rule added (v6)
paja444@Alpagame:~$ sudo ufw allow 514/udp
WARN: uid is 0 but '/' is owned by 1001
Rule added
Rule added (v6)
paja444@Alpagame:~$ sudo ufw reload
WARN: uid is 0 but '/' is owned by 1001
Firewall reloaded
```

### Rsyslog client configuration

(On Erwan's lost server)

```bash
vi /etc/rsyslog.conf
```
append the below line at the end of the file 
```bash
# #Enable sending system logs over UDP to rsyslog server
*.* @54.37.153.179:514

#Enable sending system logs over TCP to rsyslog server
*.* @@54.37.153.179:514

$ActionQueueFileName queue
$ActionQueueMaxDiskSpace 1g
$ActionQueueSaveOnShutdown on
$ActionQueueType LinkedList
$ActionResumeRetryCount -1
```

restart service

```bash
systemctl restart rsyslog.service
```
